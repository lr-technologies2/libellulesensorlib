# libelluleSensorLib <!-- NOTOC -->
## Description

This project handles the sensors of the Libellul'ergo project to be used as Human-Computer Interface. The goal is to replace the computer mouse with any sensor from a pre-defined list, and in the future, enable anyone to add new sensors.   
This repository contains the embedded code for ESP32 platform (to which the sensors are connected), and also temporary holds Python code during development. The repository should work with both Windows and Linux.


[[_TOC_]]

----

## Folder structure

```
📦 libellulesensorlib   ------> Root folder   
┣ 📂 Libergo_ESP_side   ------> C++ Embedded code for ESP32 platform   
┣ 📂 Libergo_PC_side    ------> Python backend to exchange messages between the PC and ESP32   
┣ 📂 Libergo_Sensor_App ------> Python GUI modules (to be moved to LR_Keyboard repository once operational)   
┣ 📂 doc                ------> Global documentation folder   
┣ 📜 README             ------> This file   
┗ 📜 .gitignore         ------> Specify files and folders that must not be pushed to the remote repository (local configs, etc.)   
```


## Installation

Clone this repository.   
```bash
git clone git@gitlab.com:lr-technologies2/libellulesensorlib.git
cd libellulesensorlib
```

### Install Visual Studio Code and extensions

Set up the IDE to develop, build and deploy the code from Libergo_ESP_side folder to the target board (ESP32).

1. Download, install and launch VS Code On the Extensions tab (left column), find and install the following extensions :
   - C/C++ extension
   - Python extension
   - PlatformIO IDE: an open source ecosystem for IoT development with cross platform build. 
   - Espressif-IDF extension: similar to PlatformIO but specialized for ESP32, this extension installs the correct drivers for ESP32 boards.
2. Go to File -> Open Folder and open the `libellulesensorlib` folder that you cloned. PlatformIO should recognize the project and initialize. If not, open `libellulesensorlib/Libergo_ESP_side`. A new tab should appear on the left column (bee head) and a banner should appear at the bottom of the IDE screen (with buttons such as build, deploy, serial monitor, etc.).
3. Build the project.

### How to build and deploy

Use the *Build* icon in PlatformIO banner at the bottom of IDE. 
When the build is successful, plug the ESP32 board and verify it is visible in the "Devices" tab.  
Finally, use the *Upload* icon from the banner, next to the *Build* one.   

![PlatformIO extension in VSCode](doc/Pio_annotated.png)

### Troubleshooting
**PlatformIO says Python cannot be found**   
Add Python to your PATH variable. If necessary, install Python 3.   
For example, using a terminal:
```bash
sudo apt-get install python3-venv
```
Then, restart Visual Studio Code.


## Development guidelines

This section introduces the role of each branch and defines the conventions to contribute to this repository.

### Branches 

The deployment branch is `main`. Code pushed to this branch should be fully tested and functional. The development branch is `develop`. The `archive/POCS` branch contains POCs previously developed in order to evaluate and select technical solutions. Other branhces are work in progress or waiting to be merged.   

### How to contribute 

Create a new branch by cloning the `develop` branch. 

The branch name should follow the convention `name/type/detail`:
- `name`: name of the developer working on the branch. 
- `type`: choose one from:
  - `feature_{number}`: add a new feature to the code. The number corresponds to the associated Trello card.  
  - `bugfix_{number}`: fix a bug. Add a bug card to the Trello board if it is not already there. 
  - `chore`: cleanup task, such as adding comments or documentation, reorganizing files, etc.
- `detail`: give a short descriptive name to your task.  


## Architecture
Here is an overall desired architecture draft. It is yet to be completed, with the initialization and calibration step, additional features, etc.   

The web server exchanges information with the computer app (that can be the real "for use" app or the initialization/calibration app). The app can be represented as a **GUI** and a **GUI_backend** components.

The computer exchanges information with the ESP32 board through the component called **Sensor_Comm_Server**. The adopted solution is a specific Python program that serves as intermediary between the app backend and the ESP32 device (*via* Bluetooth or Serial). Details about this component are given in [Libergo_PC_side/Readme](Libergo_PC_side/README.md).

On the ESP32 side, two tasks are executed in parallel on the two cores: **Sensor_Comm_Client** is dedicated to communication (*via* Bluetooth or Serial), **Sensor_Manager** is dedicated to the sensors. The details for these 2 components are given in [Libergo_ESP_side/Readme](Libergo_ESP_side/README.md).   

![Full architecture components](doc/Full_Architecture.jpg)



## Tutorials and useful tools

* Beginer discussion on FreeRtos the OS inside of ESP usefull for understanding tasks and the need for semaphores.
	* https://www.esp32.com/viewtopic.php?t=9930
	
* Tutorials on SPIFFS the filesystem in ESP
	* https://randomnerdtutorials.com/esp32-vs-code-platformio-spiffs/
	* https://techtutorialsx.com/2018/08/05/esp32-arduino-spiffs-writing-a-file/
	
* Troubleshooting discussion about drivers
	* https://community.platformio.org/t/esp32-not-detected-as-device-in-upload-port/23676/3

* Remote development in WSL
  * https://code.visualstudio.com/docs/remote/wsl-tutorial
  * https://learn.microsoft.com/fr-fr/windows/wsl/install-manual#step-3---enable-virtual-machine-feature
