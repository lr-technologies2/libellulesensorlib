# Libergo - PC

## Description

This folder contains the code to run on the computer. It will exchange messages with the ESP32 board, and decode them to pass information either to the web server or the computer app.

## Architecture

Here is a detailed view of the **Sensor_Comm_Server** module during normal usage (not initialization and calibration mode).   

After setting up the Bluetooth and initializing the ID to exchange messages with, the server waits for messages that can come from:
- the computer: requests from the app.
- the ESP32 board with sensors: measurement or identification data.

When a request is received from the Python app, a message is built and sent through the Bluetooth connection. An Ack message should be received shortly after.

When a message is received from the ESP32 through the Bluetooth connection, the server decodes it. If its an Ack, the message is correctly sent. 
If it is data to transfer, the message is passed on to the main app.

If a bluetooth message is not successfully sent, a warning must be sent back to the app and shown to the user through the GUI.

If the connection is lost, the program must try to reconnect. Both the Sensor_Comm_Server and the Sensor_Comm_Client must pause sending messages and try to reconnect.

![Sensor_Comm_Server state machine](../doc/SensorCommServer.jpg)

## Development roadmap

- [ ] Sensor_Comm_Server
  - [ ] (WIP) BT-V1: basic implementation using Bluetooth commnication
  - [ ] (WIP) Serial-V1: basic implementation using Serial communication
  - [ ] Communication with GUI backend
- [ ] Web Client:
  - [ ] Communication with web server
- [ ] Usage GUI
  - [ ] Basic GUI
  - [ ] Basic mouse control
  - [ ] Integration to LibelluleKeyboard project
- [ ] Deployment of Sensor_Comm_Server as a Python package to use in both GUI (Usage and Initialization/Calibration)
