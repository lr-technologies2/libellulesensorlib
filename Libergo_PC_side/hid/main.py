import asyncio
from bleak import BleakClient, BleakScanner

class BluetoothHIDClient:
    """
    A class to represent a Bluetooth client for HID devices.

    Attributes:
    name (str): The name of the Bluetooth device to connect to.
    receiver_service_UUID (str): The UUID of the receiver service on the Bluetooth device.
    device: The Bluetooth device object.
    client: The BleakClient object for communication.
    """

    def __init__(self, name: str, receiver_service_UUID: str) -> None:
        """
        Constructs a new BluetoothHIDClient object.

        Parameters:
        name (str): The name of the Bluetooth device to connect to.
        receiver_service_UUID (str): The UUID of the receiver service on the Bluetooth device.
        """
        self.name = name
        self.receiver_service_UUID = receiver_service_UUID
        self.device = None
        self.client = None
        self.paired = False
        self.identifier_mapping = {
            "START_CALIBRATING_X_SENSOR": 0,
            "START_CALIBRATING_Y_SENSOR": 1,
            "START_CALIBRATING_LEFT_CLICK_SENSOR": 2,
            "START_CALIBRATING_RIGHT_CLICK_SENSOR": 3,
            # Add other mappings as needed
        }

    async def connect(self) -> bool:
        """
        Connects to the Bluetooth device.

        Returns:
        bool: True if the connection is successful, False otherwise.
        """
        try:
            print("Scanning for 5 seconds, please wait...")
            self.device = await BleakScanner.find_device_by_name(self.name)
            
            if not self.device:
                print("Device not found.")
                return False

            self.client = BleakClient(self.device)
            await self.client.connect()
            print(f"Connected: {self.client.is_connected}")
            return True

        except Exception as e:
            print(f"Connection failed: {e}")
            return False

    async def pair(self) -> None:
        """
        Initiates the pairing process with the Bluetooth device.
        """
        try:
            self.paired = await self.client.pair(protection_level=2)
            print(f"Paired: {self.paired}")
        except Exception as e:
            print(f"Pairing failed: {e}")

    async def send_data(self, identifier_name : str) -> None:
        """
        Sends data to the Bluetooth device.

        Parameters:
        identifier_name (str): The name of the identifier.
        value (int): The value to send.
        """
        identifier = self.identifier_mapping.get(identifier_name, None)
        if identifier is not None:
            data = bytes(f"[{identifier}]", 'utf-8')
            try:
                if self.client.is_connected:
                    await self.client.write_gatt_char(self.receiver_service_UUID, data, response=False)
                    print("Data sent successfully.")
                else:
                    print("Client is not connected.")
            except Exception as e:
                print(f"Failed to send data: {e}")
        else:
            print("Identifier name not found.")

    async def receive_data(self) -> None:
        """
        Receives data from the Bluetooth device.
        """
        data_received = False  # Variable to indicate if data has been received

        try:
            if self.client.is_connected:
                # Define the callback function to handle notifications
                async def notification_handler(sender, data):
                    nonlocal data_received  # Use the variable outside the current scope
                    data_received = True  # Set to True when data is received
                    print(f"Received data: {data.decode('utf-8')}")
                    await self.client.stop_notify(self.receiver_service_UUID) # Needed to avoid duplicate messages

                # Subscribe to notifications of the receive characteristic
                await self.client.start_notify(self.receiver_service_UUID, notification_handler)
                #print("Waiting for data... Press Ctrl+C to stop.")

                # Keep the loop running until data is received
                while not data_received: # TODO: ADD TIMEOUT
                    await asyncio.sleep(1)
            else:
                print("Client is not connected.")

        except Exception as e:
            print(f"Failed to receive data: {e}")

    async def disconnect(self) -> None:
        """
        Disconnects from the Bluetooth device.
        """
        try:
            if self.client.is_connected:
                await self.client.disconnect()
                print("Disconnected from the device.")
            else:
                print("Already disconnected.")
        except Exception as e:
            print(f"Disconnection failed: {e}")

async def main() -> None:
    receiver_service_UUID = "a60e1f9c-dc21-4be2-858a-e8e2321cce7a"
    name = "LRT_HID"

    client = BluetoothHIDClient(name, receiver_service_UUID)
    if await client.connect():
        await client.send_data("START_CALIBRATING_X_SENSOR")
        await client.receive_data()
        
        await client.send_data("START_CALIBRATING_Y_SENSOR")
        await client.receive_data()

        await client.send_data("START_CALIBRATING_RIGHT_CLICK_SENSOR")
        await client.receive_data()

        await client.send_data("START_CALIBRATING_LEFT_CLICK_SENSOR")
        await client.receive_data()
        
        #await client.pair()

if __name__ == "__main__":
    asyncio.run(main())
