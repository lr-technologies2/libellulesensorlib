'''
@file jerry_mouse.py
@author Adrien Dorise - LR Technologies (adorise@lrtechnologies.fr)
@brief Jerry Mouse device - PC side
@date 2023-11-06
'''

import time
import pyautogui
import json
import sensorlib.backend.communication as comm
import sensorlib.backend.messages_generator as msg
import sensorlib.devices.devices as devices


class JerryMouse(devices.Device):
    """Device of type Jerry Mouse.
    This device is equipped with a joystick for cursor control, and two distance sensors for right and left click

    Args:
        cursor_speed (float, optional): speed at which the mouse cursor is moved. Only used when the connceted device process mouse control. Defaults to 50.0.
        sampling_time (float, optional): Sampling time at which each sensor information is recovered. Too slow create latency between input and control, too fast creates inconsistency in the processing time. Defaults to 0.01.
        communication_type (_type_, optional): Choose the communication medium between PC and ESP. Options are: {USB, BLUETOOTH} Defaults to comm.CommunicationType.USB.
        blt_device_name (str, optional): Name of the device when bluetooth is activated. Must match the device name. Defaults to "LRT".
        serial_port (str, optional): Port used when USB is selected. Must match the USB port used. Defaults to "/dev/ttyUSB0".
    """
    def __init__(self, cursor_speed=50.0, sampling_time=0.01, communication_type=comm.CommunicationType.USB, blt_device_name="LRT", serial_port="/dev/ttyUSB0", *args, **kwargs):
        super().__init__(cursor_speed, sampling_time, communication_type, blt_device_name, serial_port, *args, **kwargs)
        self.is_clicking_left = False
        self.is_clicking_right = False
        
        self.calibration_instructions = {"neutral": "Do not touch any sensor",
                        "joy_right": "Move joytsick right",
                        "joy_down": "Move joystick down",
                        "left_click": "Calibrate distance left click",
                        "right_click": "Calibrate distance right click"}
        
        self.my_name = "JerryMouse"
        self.load_default()


    def load_default(self):
        self.my_params = {
            "neutral_x": 1900,
            "neutral_y": 1900,
            "max_x": 3000,
            "max_y": 3000,
            "threshold_right": 200, 
            "threshold_left": 200
        }
        return 0

    def joystick(self, X, Y):
        """Joystick processing for cursor control

        Args:
            X (int): Joystick X value 
            Y (int): Joystick Y value
        """
        if X < (self.my_params["neutral_x"] - 100):
            x_offset = -self.cursor_speed * ((self.my_params["neutral_x"]-X)/self.my_params["max_x"])
        elif X > (self.my_params["neutral_x"] + 100):
            x_offset = self.cursor_speed * ((X-self.my_params["neutral_x"])/self.my_params["max_x"])
        else:
            x_offset = 0

        if Y < (self.my_params["neutral_y"] - 100):
            y_offset = -self.cursor_speed * ((self.my_params["neutral_y"]-Y)/self.my_params["max_y"])
        elif Y > (self.my_params["neutral_y"] + 100):
            y_offset = self.cursor_speed * ((Y-self.my_params["neutral_y"])/self.my_params["max_y"])
        else:
            y_offset = 0
        pyautogui.moveRel(x_offset, y_offset, duration=0.001, tween=pyautogui.linear)

    def click(self, left_distance, right_distance):
        """Distance sensor processing for right and left click

        Args:
            left_distance (int): Distance sensor value for left click
            right_distance (int): Distance sensor value for right click
        """
        if left_distance <= self.my_params["threshold_left"]: 
            if not self.is_clicking_left:
                pyautogui.leftClick()
                self.is_clicking_left = True
                #print("Left click")
        else:
            self.is_clicking_left = False
        
        if right_distance <= self.my_params["threshold_right"]: 
            if not self.is_clicking_right:
                pyautogui.rightClick()
                self.is_clicking_right = True
                #print("Right click")
        else:
            self.is_clicking_right = False
        
    def parser(self, commands):
        """Parse the incoming command to correctly process the input.
        For Jerry Mouse, cmds are related to either the joystic sensor, or one of the two distance sensors.
        Args:
            commands (string): JSON command sent by the ESP32

        Returns:
            int: error code -> negative if error found
            int[3]: {[X,Y] values from the joystick,
                    distance value from the left click,
                    distance value from the right click 
        """
        X = Y = left_distance = right_distance = 0
        if(len(commands) == 0):
            return -1, [[X,Y], left_distance, right_distance]
        
        for cmd in commands:
            try:
                cmd_json = json.loads(cmd)
                if(cmd_json["message_type"] == 3):
                    #End message flag
                    break
                
                sensor_id = cmd_json["ID"]
                if(sensor_id == 1):
                    X = cmd_json["X_value"]
                    Y = cmd_json["Y_value"]
                elif(sensor_id == 2):
                    left_distance = cmd_json["Distance"]
                elif(sensor_id == 3):
                    right_distance = cmd_json["Distance"]
            except:
                print(f"CMD not in correct JSON format: {cmd}")
                return -1, [[X,Y], left_distance, right_distance]
            
        return 0, [[X,Y], left_distance, right_distance]
    
    
    def process_commands(self, data):
        """Use the data gathered by the sensors to control the computer.
        This method is called in self.control() and must be overridden in child classes

        Args:
            data (_type_): sensors' data
        Returns:
            int: Return 0 if successfull, negative otherwise
        """
        try:
            self.joystick(data[0][0], data[0][1])
            self.click(data[1],data[2])    
        except:
            return -1
        return 0
    
            
    def calibrate_single_sensor(self, token, data):
        """Perform the calibration on each sensor of the Jerry Mouse device
        This method is called in self.calibrate() and must be overridden in child classes
        
        Args:
            token (string): token identifying the instruction used to gather data
            data (int or list[int]): Calibration data for the sensor
        Returns:
            int: return 0 if successfull, negative otherwise
        """
        if token == "neutral": 
            data_x = [item[0][0] for item in data]
            data_y = [item[0][1] for item in data]
            mean_x = sum(data_x)/len(data_x)
            mean_y = sum(data_y)/len(data_y)
            self.my_params["neutral_x"] = round(mean_x)
            self.my_params["neutral_y"] = round(mean_y)
        elif token == "joy_right":
            max_x = max([item[0][0] for item in data])
            self.my_params["max_x"] = max_x
        elif token == "joy_down":
            max_y = max([item[0][1] for item in data])
            self.my_params["max_y"] = max_y
        elif token == "left_click":
            data_left = [item[1] for item in data]
            mean_left = sum(data_left)/len(data_left)
            self.my_params["threshold_left"] = round(mean_left)
        elif token == "right_click":
            data_right = [item[2] for item in data]
            mean_right = sum(data_right)/len(data_right) 
            self.my_params["threshold_right"] = round(mean_right)
        return 0
     
     
if __name__ == "__main__":
    jerry = JerryMouse(50.0, 0.001, comm.CommunicationType.USB)
    if(True):
        error = jerry.connect()
        for token, instr in jerry.calibration_instructions.items():
            print(instr)
            if(jerry.calibrate({token:instr}) < 0):
                break
        jerry.save_config()
    while(True):
        time.sleep(1)
        print("Connection...")
        error = jerry.connect()
        print(error)
        if error >= 0:
            print("Connected")
            jerry.control()
            print("Connection lost")
