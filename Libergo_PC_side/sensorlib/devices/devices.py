'''
@file devices.py
@author Adrien Dorise - LR Technologies (adorise@lrtechnologies.fr)
@brief Main device class - PC side
@date 2023-11-09
'''
import os
import pyautogui
import json
import sensorlib.backend.communication as comm
import sensorlib.backend.messages_generator as msg
import time


class Device():
    """Device class that instantiate the link between the ESP32 and the PC.
    It connect values recover by the ESP32, and the processing in terms of human-machine interactions
    It is thought to be a parent class of all available devices on ESP32 side
    

    Args:
        cursor_speed (float, optional): speed at                                                                                                                                                            which the mouse cursor is moved. Only used when the connceted device process mouse control. Defaults to 50.0.
        sampling_time (float, optional): Sampling time at which each sensor information is recovered. Too slow create latency between input and control, too fast creates inconsistency in the processing time. Defaults to 0.01.
        communication_type (_type_, optional): Choose the communication medium between PC and ESP. Options are: {USB, BLUETOOTH} Defaults to comm.CommunicationType.USB.
        blt_device_name (str, optional): Name of the device when bluetooth is activated. Must match the device name. Defaults to "LRT".
        serial_port (str, optional): Port used when USB is selected. Must match the USB port used. Defaults to "/dev/ttyUSB0".
    """
    def __init__(self, cursor_speed=50.0, sampling_time=0.01, communication_type=comm.CommunicationType.USB, blt_device_name="LRT", serial_port="/dev/ttyUSB0", *args, **kwargs):
        self.sampling_time = sampling_time
        self.communication_type = communication_type
        self.cursor_speed = cursor_speed
        self.blt_device_name = blt_device_name
        self.serial_port = serial_port
        self.comm_connected = False
        self.interrupt = False
        self.calibration_instructions = {}
        self.my_name = "DEVICE"
        pyautogui.PAUSE = sampling_time
        pyautogui.FAILSAFE = False
        
        self.calibration_sampling_time = 0.1
        self.calibration_period_time = 2
        
        self.my_params = {}
        
    def connect(self):
        """Instantiate the communication link between ESP32 and PC
        """
        self.interrupt = False
        self.communication = comm.Communication(communication_type=self.communication_type, blt_device_name=self.blt_device_name, serial_port=self.serial_port)
        if(self.communication.connect(self.communication_type) < 0):
            return -1
        response_type = -1
        while(response_type != msg.START_ESP and not self.interrupt):
            self.communication.send(msg.message_start())
            response = self.communication.read()
            response_type = self.communication.get_message_type(response)
        self.comm_connected = True
        return response_type

    def control(self):
        """Main control loop
        Return:
            int: Return -1 if the loop is stopped.
        """
        start_time = time.time() + self.sampling_time
        
        while self.comm_connected and not self.interrupt:
            if(time.time() - start_time >= self.sampling_time):
                if not self.communication.is_connection_open():
                    self.disconnect()
                    break
                sensor_data = self.get_sensors_data()
                if(len(sensor_data) > 0):
                    err, data = self.parser(sensor_data)
                    self.process_commands(data)
                start_time = time.time()
        return -1
    
    def process_commands(self, data):
        """Use the data gathered by the sensors to control the computer.
        This method is called in self.control() and must be overridden in child classes

        Args:
            data (_type_): sensors' data
        """
        return -1

    def get_sensors_data(self):
        """Send the ready flag to the ESP32 and read the sensors data.
        The message sent by ESP are taken care in a loop that can be stopped if 
        the communication link between PC <-> ESP is broken

        Returns:
            _type_: list(string): Return the data gathered by the sensors in JSON string format. Empty array otherwise
        """
        self.communication.send(msg.message_ready())
        response_type = 0
        response = []
        while(response_type != msg.END_MSG_ESP and self.comm_connected and not self.interrupt):
            resp = self.communication.read()
            if(resp == ""):
                break
            response.append(resp)
            response_type = self.communication.get_message_type(response[-1])
            if(response_type < 0):
                break
        return response
        
    def parser(self, commands):
        """Parse the incoming command to correctly process the input
        This method is called in self.control() and must be overridden in child classes

        Args:u
            commands (string): JSON command sent by the ESP32

        Returns:
            int: return 0 if successfull, negative otherwise
        """
        return -1
    
    def disconnect(self):
        """Send RESTART flag to the ESP32 and close communication protocole
        """
        self.communication.send(msg.message_restart_esp())
        self.communication.close_connection()
        self.comm_connected = False
        self.interrupt = True

    
    def calibrate(self, calibration_instructions = {"neutral": "Do not touch any sensor"}):
        """Runs a scenario to initialize internal attributes.

        Args:
            calibration_instruction : Dictionnary containing the calibrated senor key, and the instructions to give to the user. Default to {"neutral": "Do not touch any sensor"}

        Returns:
            int: return 0 if no problem
        """
        safeguard = 0
        error = 0  

        #print("\nStarting Calibration Phase")
        time.sleep(2)
        for token, instr in calibration_instructions.items():
            data_json = []
            data = []
            time.sleep(2)
            start_time = time.time()
            
            #We gather data to calibrate each sensors 
            while time.time() <= start_time + self.calibration_period_time:
                if(safeguard >= 10000):
                    return -1
                safeguard+=1
                
                tmp_data = self.get_sensors_data()
                if(len(tmp_data) == 0):
                    return -1
                data_json.append(tmp_data)
                time.sleep(self.calibration_sampling_time)
                
            if(len(data_json) == 0):
                return -1
            
            for dat in data_json:
                error, tmp_data = self.parser(dat)
                data.append(tmp_data)
                if(error < 0):
                    return -1
            
            #We process gathered data depending on instruction given
            error = self.calibrate_single_sensor(token, data)
            if(error < 0):
                return -1
        
        #print("Ending Calibration Phase\n")
        return 0

    def calibrate_single_sensor(self, token, data):
        """Perform the calibration on each sensor of the device
        This method is called in self.calibrate() and must be overridden in child classes
        
        Args:
            token (string): token identifying the instruction used to gather data
            data (int or list[int]): Calibration data for the sensor
        Returns:
            int: return 0 if no problem
        """
        return -1
        
    def save_config(self, calib_file_path="lr_calib.json"):
        """Saves calibration data into a json file
        
        Args:
            calib_file_path (string, optional): Path to save the calibration file. Default is "lr_calib_json"
        """
        assert isinstance(self.my_params, dict), \
            f"`self.my_params` argument should be a dictionary, got {type(self.my_params)}"

        try: 
            with open(calib_file_path, "r") as file:
                data = json.load(file)
        except Exception:
            with open(calib_file_path, "w") as file:
                json.dump({}, file)
                data = {}
        
        data[self.my_name] = self.my_params

        with open(calib_file_path, "w") as file:
            json.dump(data, file)
        print(f"--> Calibration data saved as {calib_file_path}.")
    
    def load_config(self, calib_file_path="lr_calib.json"):
        """ Load configuration into the device subclass. If no configuration is found, the defaukt calibration of the device is loaded.
        
        Args:
            calib_file_path (string, optional): Path where the calibration is stored. Default is "lr_calib_json"
        """
        try:        
            with open(calib_file_path, "r") as file:
                data = json.load(file)
            self.my_params = data[self.my_name]
        except Exception:
            self.load_default()
            
    def load_default(self):
        """Load default configuration for the device
        This function is to override in subclasses
        
        Returns:
            -1 if error, 0 otherwise
        """
        return -1
        

           
if __name__ == "__main__":
    #main()
    device = Device(1,comm.CommunicationType.USB)
    device.save_config()
    device.load_config()
