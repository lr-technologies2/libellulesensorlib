'''
@file air_mouse.py
@author Julia Cohen - LR Technologies (jcohen@lrtechnologies.fr)
@brief Air Mouse device - PC side
@date 2023-11-27
'''

import time
import pyautogui
import json
import sensorlib.backend.communication as comm
import sensorlib.backend.messages_generator as msg
import sensorlib.devices.devices as devices


class AirMouse(devices.Device):
    """Device of type Air Mouse
    This device has two distance sensors for cursor control along the vertical 
    and horizontal axes, and two distance sensors for left and right clicks.

    Args:
        cursor_speed (float, optional): speed at which the mouse cursor is moved. Only used when the connected device process mouse control. Defaults to 50.0.
        sampling_time (float, optional): Sampling time at which each sensor information is recovered. Too slow create latency between input and control, too fast creates inconsistency in the processing time. Defaults to 0.01.
        communication_type (_type_, optional): Choose the communication medium between PC and ESP. Options are: {USB, BLUETOOTH} Defaults to comm.CommunicationType.USB.
        blt_device_name (str, optional): Name of the device when bluetooth is activated. Must match the device name. Defaults to "LRT".
        serial_port (str, optional): Port used when USB is selected. Must match the USB port used. Defaults to "/dev/ttyUSB0".
    """
    def __init__(self, cursor_speed=50.0, sampling_time=0.01, communication_type=comm.CommunicationType.USB, blt_device_name="LRT", serial_port="/dev/ttyUSB0", *args, **kwargs):
        super().__init__(cursor_speed, sampling_time, communication_type, blt_device_name, serial_port, *args, **kwargs)
        self.is_clicking_left = False
        self.is_clicking_right = False

        self.calibration_instructions = {"neutral": "Do not touch any sensor",
                        "move_right": "Move right - close to sensor 1",
                        "move_left": "Move left - far away from sensor 1",
                        "move_down": "Move down - close to sensor 2",
                        "move_up": "Move up - far away from sensor 2",
                        "right_click": "Calibrate distance right click - sensor 3",
                        "left_click": "Calibrate distance left click - sensor 4"}
        
        self.my_name = "AirMouse"
        self.load_default()
    
    def load_default(self):
        # TODO update params
        self.my_params = {
            "neutral_x": 150,
            "neutral_y": 150,
            "max_x": 400,
            "max_y": 400,
            "threshold_right": 200, 
            "threshold_left": 200
        }
        return 0
    
    def move(self, X, Y):
        """Distance sensor processing for cursor control

        Args:
            X (int): Distance sensor value for horizontal axis
            Y (int): Distance sensor value for vertical axis
        """
        # vertical axis
        if Y > self.my_params["max_y"]:
            y_offset = 0
        elif Y < (self.my_params["neutral_y"] - 30):
            y_offset = self.cursor_speed * ((self.my_params["neutral_y"]-Y)/self.my_params["max_y"])
        elif Y > (self.my_params["neutral_y"] + 30):
            y_offset = -self.cursor_speed * ((Y-self.my_params["neutral_y"])/self.my_params["max_y"])
        else:
            y_offset = 0
        
        # horizontal axis
        if X > self.my_params["max_x"]:
            x_offset = 0
        elif X < (self.my_params["neutral_x"] - 30):
            x_offset = self.cursor_speed * ((self.my_params["neutral_x"]-X)/self.my_params["max_x"])
        elif X > (self.my_params["neutral_x"] + 30):
            x_offset = -self.cursor_speed * ((X-self.my_params["neutral_x"])/self.my_params["max_x"])
        else:
            x_offset = 0
        pyautogui.moveRel(x_offset, y_offset, duration=0.001, tween=pyautogui.linear)

    def click(self, left_distance, right_distance):
        """Distance sensor processing for right and left click

        Args:
            left_distance (int): Distance sensor value for left click
            right_distance (int): Distance sensor value for right click
        """
        if left_distance <= self.my_params["threshold_left"]: 
            if not self.is_clicking_left:
                pyautogui.leftClick()
                self.is_clicking_left = True
                #print("Left click")
        else:
            self.is_clicking_left = False
        
        if right_distance <= self.my_params["threshold_right"]: 
            if not self.is_clicking_right:
                pyautogui.rightClick()
                self.is_clicking_right = True
                #print("Right click")
        else:
            self.is_clicking_right = False
   
    def parser(self, commands):
        """Parse the incoming command to correctly process the input.
        For AirMouse, cmds are related to one of the four distance sensors.
        Args:
            commands (string): JSON command sent by the ESP32

        Returns:
            int: error code -> negative if error found
            int[3]: {[X,Y] distnace values for the horizontal and vertical axes,
                    distance value from the left click,
                    distance value from the right click 
        """
        X = Y = left_distance = right_distance = 0
        if(len(commands) == 0):
            return -1, [[X,Y], left_distance, right_distance]
        
        for cmd in commands:
            try:
                cmd_json = json.loads(cmd)
                if(cmd_json["message_type"] == 3):
                    #End message flag
                    break
                
                sensor_id = cmd_json["ID"]
                if(sensor_id == 1):
                    X = cmd_json["Distance"]
                elif(sensor_id == 2):
                    Y = cmd_json["Distance"]
                elif(sensor_id == 3):
                    right_distance = cmd_json["Distance"]
                elif(sensor_id == 4):
                    left_distance = cmd_json["Distance"]
            except:
                print(f"CMD not in correct JSON format: {cmd}")
                return -1, [[X,Y], left_distance, right_distance]
            
        return 0, [[X,Y], left_distance, right_distance]
    
    def process_commands(self, data):
        """Use the data gathered by the sensors to control the computer.
        This method is called in self.control() and must be overridden in child classes

        Args:
            data (_type_): sensors' data
        Returns:
            int: Return 0 if successfull, negative otherwise
        """
        try:
            self.move(data[0][0], data[0][1])
            self.click(data[1], data[2])
        except:
            return -1
        return 0

    def calibrate_single_sensor(self, token, data):
        """Perform the calibration on each sensor of the AirMouse device
        This method is called in self.calibrate() and must be overridden in child classes
        
        Args:
            token (string): token identifying the instruction used to gather data
            data (int or list[int]): Calibration data for the sensor
        Returns:
            int: return 0 if successfull, negative otherwise
        """
        if token == "neutral": 
            pass
        elif token == "move_right":
            data_right = [item[0][0] for item in data]
            mean_right = round(sum(data_right)/len(data_right))
            self.my_params["close_x"] = mean_right
        elif token == "move_left":
            data_left = [item[0][0] for item in data]
            mean_left = round(sum(data_left)/len(data_left))
            self.my_params["neutral_x"] = round((self.my_params["close_x"]+mean_left)/2)
            self.my_params["max_x"] = 2*self.my_params["neutral_x"]
        elif token == "move_down":
            data_down = [item[0][1] for item in data]
            mean_down = round(sum(data_down)/len(data_down))
            self.my_params["close_y"] = mean_down
        elif token == "move_up":
            data_up = [item[0][1] for item in data]
            mean_up = round(sum(data_up)/len(data_up))
            self.my_params["neutral_y"] = round((self.my_params["close_y"]+mean_up)/2)
            self.my_params["max_y"] = 2*self.my_params["neutral_y"]
        elif token == "left_click":
            data_left = [item[1] for item in data]
            mean_left = sum(data_left)/len(data_left)
            self.my_params["threshold_left"] = round(mean_left)
        elif token == "right_click":
            data_right = [item[2] for item in data]
            mean_right = sum(data_right)/len(data_right) 
            self.my_params["threshold_right"] = round(mean_right)
        return 0


if __name__ == "__main__":
    # Uncomment next line to use on Linux
    mouse = AirMouse(50.0, 0.001, comm.CommunicationType.USB)
    # Uncomment next line to use on Windows
    #mouse = AirMouse(50.0, 0.001, comm.CommunicationType.USB, serial_port="COM4")
    if(True):
        error = mouse.connect()
        for token, instr in mouse.calibration_instructions.items():
            print(instr)
            if(mouse.calibrate({token:instr}) < 0):
                break
        mouse.save_config()
    mouse.load_config()
    while(True):
        time.sleep(1)
        print("Connection...")
        error = mouse.connect()
        print(error)
        if error >= 0:
            print("Connected")
            mouse.control()
            print("Connection lost")

