'''
@file tools.py
@author Adrien Dorise - LR Technologies (adorise@lrtechnologies.fr)
@brief Tools to use device classes - PC side
@date 2023-12-20
'''

import sensorlib.backend.communication as comm
import sensorlib.devices.dummy_device as DM
import sensorlib.devices.jerry_mouse as JM
import sensorlib.devices.air_mouse as AM


def create_device(device_name, cursor_speed=50.0, sampling_time=0.01, communication_type=comm.CommunicationType.USB, blt_device_name="LRT", serial_port="/dev/ttyUSB0", *args, **kwargs):
    """_summary_

    Args:
        device_name (str, optional): Name of the device to initialise
        cursor_speed (float, optional): speed at                                                                                                                                                            which the mouse cursor is moved. Only used when the connceted device process mouse control. Defaults to 50.0.
        sampling_time (float, optional): Sampling time at which each sensor information is recovered. Too slow create latency between input and control, too fast creates inconsistency in the processing time. Defaults to 0.01.
        communication_type (_type_, optional): Choose the communication medium between PC and ESP. Options are: {USB, BLUETOOTH} Defaults to comm.CommunicationType.USB.
        blt_device_name (str, optional): Name of the device when bluetooth is activated. Must match the device name. Defaults to "LRT".
        serial_port (str, optional): Port used when USB is selected. Must match the USB port used. Defaults to "/dev/ttyUSB0".

    Returns:
        _type_: Device object of the selected subclass.
    """
    if(device_name == "dummyDevice"):
        return DM.DummyDevice()
    elif(device_name == "jerryMouse"):
        return JM.JerryMouse(cursor_speed=cursor_speed, sampling_time=sampling_time, communication_type=communication_type, blt_device_name=blt_device_name, serial_port=serial_port, args=args, kwargs=kwargs)
    elif(device_name == "airMouse"):
        return AM.AirMouse(cursor_speed=cursor_speed, sampling_time=sampling_time, communication_type=communication_type, blt_device_name=blt_device_name, serial_port=serial_port, args=args, kwargs=kwargs)
    
    return DM.DummyDevice()


if __name__ == "__main__":
    import time
    jerry = create_device("JerryMouse",50.0, 0.001, comm.CommunicationType.USB, serial_port="/dev/ttyUSB1")
    if(True):
        error = jerry.connect()
        for token, instr in jerry.calibration_instructions.items():
            print(instr)
            jerry.calibrate({token:instr})
        jerry.save_calib()
    while(True):
        time.sleep(1)
        print("Connection...")
        error = jerry.connect()
        print(error)
        if error >= 0:
            print("Connected")
            jerry.control()
            print("Connection lost")