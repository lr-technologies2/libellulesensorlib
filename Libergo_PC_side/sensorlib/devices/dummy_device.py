'''
@file dully_device.py
@author Adrien Dorise - LR Technologies (adorise@lrtechnologies.fr)
@brief Dummy device - PC side
@date 2023-11-
'''

import sys
import pyautogui
import json
import time
import sensorlib.backend.communication as comm
import sensorlib.backend.messages_generator as msg
import sensorlib.devices.devices as devices

class DummyDevice(devices.Device):
    """Device of type Dummy Device
    This device is a test component to verify integration of sensorlib in LR_keyboard.

    Args:
        cursor_speed (float, optional): speed at which the mouse cursor is moved. Only used when the connceted device process mouse control. Defaults to 50.0.
        sampling_time (float, optional): Sampling time at which each sensor information is recovered. Too slow create latency between input and control, too fast creates inconsistency in the processing time. Defaults to 0.01.
        communication_type (_type_, optional): Choose the communication medium between PC and ESP. Options are: {USB, BLUETOOTH} Defaults to comm.CommunicationType.USB.
        blt_device_name (str, optional): Name of the device when bluetooth is activated. Must match the device name. Defaults to "LRT".
        serial_port (str, optional): Port used when USB is selected. Must match the USB port used. Defaults to "/dev/ttyUSB0".
    """
    def __init__(self, cursor_speed=50.0, sampling_time=0.01, communication_type=comm.CommunicationType.USB, blt_device_name="LRT", serial_port="/dev/ttyUSB0", *args, **kwargs):
        super().__init__(cursor_speed, sampling_time, communication_type, blt_device_name, serial_port, *args, **kwargs)
        self.sampling_time = 2

    def connect(self):
        """Instantiate the communication link between ESP32 and PC.
        Not doing anything for Dummy Device.
        """
        print("Dummy device is connecting to your soul...")
        time.sleep(2)
        print("Whoa, that's not pretty!")
        time.sleep(1)
        print("Anyway, connection established!")
        return 0
    
    def control(self):
        """Main control loop
        """
        print("Dummy device is starting is main loop of nothingness")
        start_time = time.time() + self.sampling_time
        while not self.interrupt:
            if(time.time() - start_time >= self.sampling_time):
                self.parser("Dummy device doing something!")
                start_time = time.time()
        return -1
        
    def parser(self, commands):
        """Parse the incoming command to correctly process the input.
        For Dummy Device, Only print the sent commands
        Args:
            commands (string): JSON command sent by the ESP32

        Returns:
            int: return 0 if no error
        """
        print(commands)
        return 0
    
    def calibrate(self):
        """Start device calibration.
        """
        print("Dummy calibration start: ")
        print("Scenario 1/3: Please look at the floor with a funny face.")
        time.sleep(2)
        print("Scenario 2/3: Please contemplate the roof while thinking at all the bad things you did in your life. Maybe somebody will forgive you while doing so.")
        time.sleep(2)
        print("Scenario 3/3: The cake is ready. But the cake is your best friend! Will you eat the cake?.")
        time.sleep(2)
        print("Whatever you decided, the cake is dead. Congratulations, calibration finished!")
        return 0

    def restart(self):
        """Send RESTART flag to the ESP32
        """
        print("Dummy Device restarting")
        
    def disconnect(self):
        """Send RESTART flag to the ESP32 and close communication protocole
        """
        print("Dummy Device is tchao bye!")
        time.sleep(1)
    
if __name__ == "__main__":
    device = DummyDevice(sampling_time=2)
    #device.connect()
    device.calibrate()
    device.control()