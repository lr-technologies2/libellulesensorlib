#Messages ID have to be the same that in the file messagesTypes.h (ESP32 side)

import json

#MESSAGE FROM PC
START_PC = 0
ACK_PC = 1
READY_PC = 2
CALIBRATION = 3
DEACTIVATE_SENSOR = 4
ACTIVATE_SENSOR  = 5
DEVICE_INFO = 6
NEW_ID = 7
RESTART_ESP = 8
IS_ESP_ALIVE = 9

#MESSAGE FROM ESP
START_ESP = 0
ACK_ESP = 1
READY_ESP = 2
END_MSG_ESP = 3
DATA_SENSOR = 4
OK_MESSAGE = 5
NO_OK_MESSAGE = 6
IS_PC_ALIVE = 7


def message_start():
    return "{\"message_type\" : " + str(START_PC) + "}"

def message_ack():
    return "{\"message_type\" : " + str(ACK_PC) + "}"

def message_ready():
    return "{\"message_type\" : " + str(READY_PC) + "}"

def message_calibration():
    return "{\"message_type\" : " + str(CALIBRATION) + "}"

def message_activate_sensor():
    return "{\"message_type\" : " + str(ACTIVATE_SENSOR) + "}"

def message_deactivate_sensor():
    return "{\"message_type\" : " + str(DEACTIVATE_SENSOR) + "}"

def message_device_info():
    return "{\"message_type\" : " + str(DEVICE_INFO) + "}"

def message_new_id():
    return "{\"message_type\" : " + str(NEW_ID) + "}"

def message_restart_esp():
    return "{\"message_type\" : " + str(RESTART_ESP) + "}"

def message_is_alive_esp():
    return "{\"message_type\" : " + str(IS_ESP_ALIVE) + "}"

