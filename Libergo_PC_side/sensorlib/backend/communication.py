'''
@file _communication.py
@author Adrien Dorise, Julia Cohen - LR Technologies ({adorise,jcohen}@lrtechnologies.fr)
@brief Communication methods between PC and ESP32. Serial and bluetooth available
@date 2023-11-06
'''
'''
Important:  In order to run bluetooth capabilities the pybluez lib is required:
In order to install it correctly:
a)  Microsoft Visual C++ 14.0 or greater is required. 
    Get it with "Microsoft C++ Build Tools": 
        https://visualstudio.microsoft.com/visual-cpp-build-tools/
b)  Obtain directly the lib from the project page:
        pip install git+https://github.com/pybluez/pybluez.git#egg=pybluez
'''

from enum import Enum
import serial
import bluetooth
import json
import sensorlib.backend.messages_generator as msg

class CommunicationType(Enum):
    USB = 1
    BLUETOOTH = 2

class Communication():
    """Communciation class linking PC and ESP32 devices
    Link is initialised when instantiating class
    
    Args:
        communication_type (CommunicationType, optional): Set the communication type used. Options are USB or BLUETOOTH. Defaults to CommunicationType.USB.
        baud_rate (int, optional): Baud rate to communicate with the ESP32. It must be the same on PC and ESP32. Defaults to 115200.
        blt_device_name (str, optional): Name of the device when bluetooth is activated. Must match the device name. Defaults to "LRT".
        serial_port (str, optional): Port used when USB is selected. Must match the USB port used. Defaults to "/dev/ttyUSB0".
    """

    def __init__(self, communication_type=CommunicationType.USB, baud_rate=115200, blt_device_name="LRT", serial_port="/dev/ttyUSB0"):
        self.comm_type = communication_type
        self.max_timeout = 1.0
        self.is_ESP_ready = False
        
        if(communication_type == CommunicationType.USB):
            self.ser = serial.Serial()
            self.ser.port  = serial_port
            self.ser.baudrate = baud_rate

        elif(communication_type == CommunicationType.BLUETOOTH):
            self.device_address = None
            self.device_name = blt_device_name
    
    
    def connect(self, communication_type = CommunicationType.USB):
        """Initialise the communication link between the ESP device and the PC

        Args:
            communication_type (CommunicationType, optional): Set the communication mediaum {USB, Bluetooth}. Defaults to CommunicationType.USB.

        Returns:
            _type_: Return -1 if an error occurred, return 0 otherwise
        """
        if(communication_type == CommunicationType.USB):
            self.ser.setDTR(False)
            self.ser.setRTS(False)
            self.ser.timeout = self.max_timeout
            try:
                self.ser.open()
            except Exception as e:
                #print("No USB device found")
                return -1
        
        elif(communication_type == CommunicationType.BLUETOOTH):
            print("Finding device")  
            self.socket = None          
            self.device_address = None
            
            nearby_devices = bluetooth.discover_devices(lookup_names=True,lookup_class=True,duration=1)
            print(nearby_devices)
            for btaddr, btname, btclass in nearby_devices:
                if self.device_name == btname:
                    self.device_address = btaddr
            
            if self.device_address is None:
                return -1            
            
            print("found device {} bluetooth device with address {} ".format(self.device_name,self.device_address))
            self.socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
            try:
                self.socket.connect((self.device_address, 1))
                self.socket.settimeout(self.max_timeout)
            except bluetooth._bluetooth.error as e:
                print(f"Error connecting to device {self.device_name} at address {self.device_address}")
                print(e)
                self.socket.close()
                return -1
            print(f"Device {self.device_name} at address {self.device_address} connected!")
        return 0

        
    def send(self, message):        
        """Send message to ESP32
        Args:
            message (string): Message to send
        """
        try:
            if(self.comm_type == CommunicationType.USB):
                self.ser.write(bytes(message, 'utf-8'))
            elif(self.comm_type == CommunicationType.BLUETOOTH):
                self.socket.send(bytes(message, 'utf-8'))
            return 0
        except Exception as e:
            return -1
        
    def read(self):
        """Receive a message from the ESP32

        Returns:
            string: Message received. Empty string if error detected
        """
        if(self.comm_type == CommunicationType.USB):
            try:
                return self.ser.readline().decode()
            except Exception as e:
                return ""
        
        elif(self.comm_type == CommunicationType.BLUETOOTH):
            try:
                buffer = self.socket.recv(1).decode('utf-8')
                while(buffer[-1] != '\n'):
                        char = self.socket.recv(1)
                        buffer += char.decode('utf-8')
                return buffer
            except Exception as e:
                return ""
            
    def get_message_type(self, message):
        """Get message type of a JSON formatted string.
        Message types are specific to Libergo embedded project

        Args:
            message (string): message to decrypt

        Returns:
            int: Message type ID. -1 if error
        """
        try:            
            type = json.loads(message)['message_type']
        except:
            return -1
        return type
    
    def close_connection(self):
        """Close the connection between PC and ESP
        Don't forget to call this at end of program
        """
        if(self.comm_type == CommunicationType.USB):
            self.ser.close()
        elif(self.comm_type == CommunicationType.BLUETOOTH):
            if(self.socket is not None):
                self.socket.close()
                self.device_address = None
                
    def is_connection_open(self):
        """Send a confirmation flag to the ESP to check if the connection is still available

        Returns:
            Boolean: true if connection still available, false otherwise
        """
        self.send(msg.message_is_alive_esp())
        return self.get_message_type(self.read()) == msg.ACK_ESP          