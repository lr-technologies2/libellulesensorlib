# Libergo - PC

## Description

This folder contains the code for the communication between the GUI Python App and the sensor_comm_server.
The communication use the MQTT protocol : one broker is needed for several clients (here clients are the Python App and the sensor_comm_server).

To launch the MQTT broker, run the command :
```
mosquitto -v 
```

To install mosquitto on linux:
```
sudo apt install mosquitto
```

To compile the sensor_comm_server :
```
g++ sensor_comm_server.cpp -o sensor_comm_server -lpaho-mqttpp3 -lpaho-mqtt3as
```
then launch with
```
./sensor_comm_server
```

To install and compile libpaho-mqtt: follow instructions [here](https://github.com/eclipse/paho.mqtt.cpp)

