#include <iostream>
#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

#include <mqtt/async_client.h>

const std::string MQTT_SERVER_ADDRESS = "tcp://localhost:1883";
const std::string CLIENT_ID = "cpp_client";

class message_callback : public virtual mqtt::callback
{
    void message_arrived(mqtt::const_message_ptr msg) override
    {
        std::cout << "Message received : " << msg->to_string() << std::endl;
    }
};

int main(int argc, char *argv[])
{
    // Create an MQTT client
    mqtt::async_client client(MQTT_SERVER_ADDRESS, CLIENT_ID);

    // define callback
    message_callback cb;
    client.set_callback(cb);

    // Create connection options
    mqtt::connect_options conn_opts;

    // Connect to the MQTT broker
    mqtt::token_ptr conntok = client.connect(conn_opts);
    conntok->wait();

    //subscribe to the topic "python_to_cpp" to listen messages
    mqtt::token_ptr sub_token = client.subscribe("python_to_cpp", 2);
    sub_token->wait();

    // Publish a message 
    while(1)
    {
        sleep(5);
        mqtt::message_ptr pubmsg = mqtt::make_message("cpp_to_python", "Hello from C++!");
        client.publish(pubmsg);
    }

    client.disconnect();
    return 0;
}
