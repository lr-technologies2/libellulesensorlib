import paho.mqtt.client as mqtt
from message_app import MessageApp
import time
import threading

# Create an MQTT client
client = mqtt.Client()

#GUI interface (kivy)
gui_chat = MessageApp(client)

def send_message():
    while(1):
        time.sleep(5)
        client.publish("python_to_cpp", "Hello from Python!")
        gui_chat.display_message("Hello from Python!")
        print("message sent")

def on_connect(client, userdata, flags, rc):
    print("Connected to MQTT broker")
    client.subscribe("cpp_to_python")  # Subscribe to topic cpp_to_python to receive messages

def on_message(client, userdata, msg):
    print(f"Message received on topic {msg.topic}: {msg.payload.decode()}")
    gui_chat.display_message(msg.payload.decode())

# Set the callback functions
client.on_connect = on_connect
client.on_message = on_message

# Connect to the MQTT broker
client.connect("localhost", 1883, 60)

# Start the MQTT loop to maintain the connection and to receive messages
client.loop_start()

#Thread for sending messages
thread_send_messages = threading.Thread(target=send_message)
thread_send_messages.start()

#Run the kivy GUI message app
gui_chat.run()

# Stop the MQTT loop and disconnect the client
client.loop_stop()
client.disconnect()
