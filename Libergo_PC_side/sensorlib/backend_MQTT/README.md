# Libergo - PC

## Description
This folder contains files for the GUI in order to test the communication with the MQTT server. 
In the terminal run the command :
```shell
python MQTT_client.py
```


To send a "test" message, press the button "send message".
