import datetime
import sys
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.scrollview import ScrollView
from kivy.utils import get_color_from_hex

class MessageApp(App):
         
    def __init__(self, client, **kwargs):
        super().__init__(**kwargs)
        self.client = client 

    def build(self):
        layout = BoxLayout(orientation='vertical', spacing=20, padding=20)
        scroll_view = ScrollView(size_hint=(1, 0.7))
        self.message_label = Label(text='', halign='left', valign='top', size_hint_y=None, text_size=(None, None), color=get_color_from_hex('#FFF000'))
        scroll_view.add_widget(self.message_label)
        layout.add_widget(scroll_view)

        bottom_layout = BoxLayout(orientation='horizontal', spacing=10, size_hint=(1, 0.1))
        self.text_input = TextInput(hint_text='Saisissez votre message', size_hint=(0.7, 1))
        bottom_layout.add_widget(self.text_input)

        button = Button(text='Envoyer message', size_hint=(0.3, 1))
        button.bind(on_press=self.send_message)
        bottom_layout.add_widget(button)

        layout.add_widget(bottom_layout)
        return layout

    def send_message(self, *largs):
        message = self.text_input.text
        if message == '':
            message = "test"
        #send the message with mqtt to the cpp app
        self.client.publish("python_to_cpp", message)
        self.display_message(message)
        self.text_input.text = ''
        

    def display_message(self, message):
        now = datetime.datetime.now()
        formatted_time = now.strftime("%H:%M:%S")
        full_message = f'{formatted_time}: {message}'

        current_text = self.message_label.text
        if current_text:
            self.message_label.text = f'{current_text}\n{full_message}'
        else:
            self.message_label.text = full_message

        #This line is needed to keep the chat column on the left
        self.message_label.text_size = (self.message_label.width, None)
        
        #This line is needed to keep the scroll bar at the bottom after adding a message
        self.message_label.height = self.message_label.texture_size[1]
        self.message_label.parent.parent.scroll_y = 0


    def on_stop(self):
        sys.exit()