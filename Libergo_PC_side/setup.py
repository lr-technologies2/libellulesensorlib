from setuptools import setup, find_packages

setup(name="sensorlib",
      version="0.9.5.3",
      description="LR Technologies libergo application - Sensor related part",
      author="Adrien Dorise, Julia Cohen, Edouard Villain, Boris Lenseigne",
      author_email="{adorise, jcohen, evillain, blenseigne}@lrtechnologies.fr",
      url="https://gitlab.com/lr-technologies2/libellulesensorlib",
      packages=["sensorlib.backend",
                "sensorlib.devices", 
                ],
      install_requires=[
            "Pillow>=10.1.0",
            "PyAutoGUI>=0.9.54",
            "PyBluez @git+https://github.com/pybluez/pybluez.git@4d46ce14d9e888e3b0c65d5d3ba2a703f8e5e861",
            "pyserial>=3.5"
      ], 
      python_requires=">=3.10.1",
     )