# Libergo - ESP32

## Description

This folder contains the Python code to run the initialization and calibration app. This GUI application enables a helper to install and verify the sensors configuration for a user.

## Development roadmap

- [ ] Initialization/Calibration GUI
  - [ ] Serial connection
  - [ ] ID data exchange
  - [ ] Device data exchange
  - [ ] Calibration data exchange
  - [ ] Calibration procedure
- [ ] Integration of Sensor_Comm_Server