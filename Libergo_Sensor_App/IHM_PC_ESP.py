import serial
import json
import tkinter as tk
from tkinter import ttk
import serial.tools.list_ports

class ESPInterface:
    def __init__(self, baudrate=115200):
        self.ser = self.detect_esp_port(baudrate)
        if not self.ser:
            print("ESP device not found.")
            return
        print(f"Connected to {self.ser.port}")
        self.setup_gui()
        self.root.after(1000, self.request_initial_config)

    def detect_esp_port(self, baudrate):
        ports = serial.tools.list_ports.comports()
        for port in ports:
            print(f"Trying port: {port.device}")
            try:
                ser = serial.Serial(port.device, baudrate, timeout=1)
                ser.close()
                return serial.Serial(port.device, baudrate, timeout=1)
            except serial.SerialException as e:
                print(f"Failed to open port {port.device}: {e}")
                continue
        return None

    def setup_gui(self):
        self.root = tk.Tk()
        self.root.title("ESP Calibration Interface")

        self.frame = ttk.Frame(self.root, padding="10")
        self.frame.grid(row=0, column=0, sticky=(tk.W, tk.E, tk.N, tk.S))

        self.type_label = ttk.Label(self.frame, text="Mouse Type:")
        self.type_label.grid(row=0, column=0, sticky=tk.W)

        self.type_var = tk.StringVar()
        self.type_combobox = ttk.Combobox(self.frame, textvariable=self.type_var)
        self.type_combobox['values'] = ('AIR_MOUSE', 'JERRY_MOUSE')
        self.type_combobox.grid(row=0, column=1, sticky=(tk.W, tk.E))

        self.current_config_label = ttk.Label(self.frame, text="Current Configuration")
        self.current_config_label.grid(row=1, column=0, columnspan=2, sticky=tk.W)

        self.read_only_text = tk.Text(self.frame, width=50, height=10, state='disabled')
        self.read_only_text.grid(row=2, column=0, columnspan=2, sticky=(tk.W, tk.E))

        self.new_config_label = ttk.Label(self.frame, text="New Calibration Configuration")
        self.new_config_label.grid(row=3, column=0, columnspan=2, sticky=tk.W)

        self.calibration_text = tk.Text(self.frame, width=50, height=10)
        self.calibration_text.grid(row=4, column=0, columnspan=2, sticky=(tk.W, tk.E))

        self.send_request_button = ttk.Button(self.frame, text="Send New Configuration", command=self.send_request)
        self.send_request_button.grid(row=5, column=0, pady=10)

        self.update_button = ttk.Button(self.frame, text="Update Configuration", command=self.request_initial_config)
        self.update_button.grid(row=5, column=1, pady=10)

        self.root.columnconfigure(0, weight=1)
        self.root.rowconfigure(0, weight=1)
        self.frame.columnconfigure(1, weight=1)

    def request_initial_config(self):
        self.send_message({"message_type": 10})
        self.root.after(1000, self.read_serial)

    def send_message(self, message):
        msg_str = json.dumps(message) + '\n'
        self.ser.write(msg_str.encode('utf-8'))

    def read_serial(self):
        while self.ser.in_waiting > 0:
            line = self.ser.readline().decode('utf-8').strip()
            if line:
                try:
                    message = json.loads(line)
                    self.handle_message(message)
                except json.JSONDecodeError:
                    pass
        self.root.after(100, self.read_serial)

    def handle_message(self, message):
        if not isinstance(message, dict):
            return

        if "message_type" not in message:
            return

        msg_id = message.get("message_type")
        if msg_id == 8:
            config_data = message.get("configuration", "")
            self.update_read_only_fields(config_data)
            self.update_calibration_text(config_data)

    def update_read_only_fields(self, config_data):
        self.read_only_text.configure(state='normal')
        self.read_only_text.delete('1.0', tk.END)
        fields = self.parse_config_data(config_data)
        for key, value in fields.items():
            self.read_only_text.insert(tk.END, f"{key}={value}\n")
        self.read_only_text.configure(state='disabled')
        mouse_type = fields.get("m_name", "")
        self.type_var.set(mouse_type)

    def parse_config_data(self, config_data):
        fields = {}
        lines = config_data.split('\n')
        for line in lines:
            if '=' in line:
                key, value = line.split('=', 1)
                fields[key.strip()] = value.strip()
        return fields

    def update_calibration_text(self, config_data):
        fields = self.parse_config_data(config_data)
        calibration_text = ""
        if fields.get("m_name") == "AIR_MOUSE":
            calibration_text = (
                "m_name=AIR_MOUSE\n"
                "m_sensibility=\n"
                "SENSOR0m_neutral=\n"
                "SENSOR0m_maxDistance=\n"
                "SENSOR1m_neutral=\n"
                "SENSOR1m_maxDistance=\n"
                "SENSOR2m_neutral=\n"
                "SENSOR2m_maxDistance=\n"
                "SENSOR3m_neutral=\n"
                "SENSOR3m_maxDistance=\n"
            )
        elif fields.get("m_name") == "JERRY_MOUSE":
            calibration_text = (
                "m_name=JERRY_MOUSE\n"
                "m_sensibility=\n"
                "JOYSTICK0m_minX=\n"
                "JOYSTICK0m_maxX=\n"
                "JOYSTICK0m_minY=\n"
                "JOYSTICK0m_maxY=\n"
                "JOYSTICK0m_neutralX=\n"
                "JOYSTICK0m_neutralY=\n"
                "SENSOR0m_neutral=\n"
                "SENSOR0m_maxDistance=\n"
                "SENSOR1m_neutral=\n"
                "SENSOR1m_maxDistance=\n"
            )
        self.calibration_text.delete('1.0', tk.END)
        self.calibration_text.insert(tk.END, calibration_text)

    def send_request(self):
        config_data = self.calibration_text.get('1.0', tk.END).strip()
        message = {
            "message_type": 11,
            "configuration": config_data
        }
        self.send_message(message)

    def run(self):
        self.root.mainloop()

if __name__ == "__main__":
    esp_interface = ESPInterface()
    if esp_interface.ser and esp_interface.ser.is_open:
        esp_interface.run()
