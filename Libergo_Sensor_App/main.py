import time
import pyautogui
import json
import sensorlib.backend.communication as comm
import sensorlib.backend.messages_generator as msg
import sensorlib.devices.devices as devices


import sys
import tkinter as tk

click_x = 0
click_y = 0
neutral_x = 0
neutral_y = 0
max_x = 0
max_y = 0
threshold_x = 0
threshold_y = 0

def pointeur(event):
    global click_x, click_y, neutral_x, neutral_y, max_x, max_y, threshold_x, threshold_y
    click_x = event.x
    click_y = event.y
    print("Clic détecté en X =" + str(click_x) +\
                            ", Y =" + str(click_y))

def ok():
    neutral_x = nx.get()
    neutral_y = ny.get()
    max_x = mx.get()
    max_y = my.get()
    threshold_x = tx.get()
    threshold_y = ty.get()
    print("Slide Neutral X : "+str(neutral_x)+" slide Neutral Y : "+str(neutral_y))
    print("Slide Max X : "+str(max_x)+" slide Max Y : "+str(max_y))
    print("Slide Threshold X : "+str(threshold_x)+" slide Threshold Y : "+str(threshold_y))
    sys.exit(0)

root = tk.Tk()
root.overrideredirect(True)
width = root.winfo_screenwidth()
height = root.winfo_screenheight()

print ("width = ", width, " height = ", height)

canvas = tk.Canvas(root, width=width, height=height, highlightthickness=0)

canvas.focus_set()

height = height - 455
centerx = width/2 - 1;
centery = height/2 - 1;
# center
canvas.create_line(centerx-10,centery,centerx+10,centery,width=10,fill="red")
canvas.create_line(centerx,centery-10,centerx,centery+10,width=10,fill="red")
# top left
canvas.create_line(0,0,0,9,width=10,fill="red")
canvas.create_line(0,0,9,0,width=10,fill="red")
# top right
canvas.create_line(width-10,0,width,0,width=10,fill="red")
canvas.create_line(width-1,0,width-1,10,width=10,fill="red")
# bottom left
canvas.create_line(0,height-10,0,height-1,width=10,fill="red")
canvas.create_line(0,height-1,9,height-1,width=10,fill="red")
# bottom right
canvas.create_line(width-10,height-1,width-1,height-1,width=10,fill="red")
canvas.create_line(width-1,height-10,width-1,height-1,width=10,fill="red")

canvas.bind("<Button-1>", pointeur)
#canvas.bind("<Motion>", lambda event: label.configure(text=f"{event.x}, {event.y}"))
#canvas.pack(fill='both')

nx = tk.Scale(root, label="Neutral X", from_=-4095, to=4095, orient=tk.HORIZONTAL, width=5, length=width/2)
nx.set(0)
nx.pack()

ny = tk.Scale(root, label="Neutral Y", from_=-4095, to=4095, orient=tk.HORIZONTAL, width=5, length=width/2)
ny.set(0)
ny.pack()

mx = tk.Scale(root, label="Max X", from_=-4095, to=4095, orient=tk.HORIZONTAL, width=5, length=width/2)
mx.set(0)
mx.pack()

my = tk.Scale(root, label="Max Y", from_=-4095, to=4095, orient=tk.HORIZONTAL, width=5, length=width/2)
my.set(0)
my.pack()

tx = tk.Scale(root, label="Threshold X", from_=-4095, to=4095, orient=tk.HORIZONTAL, width=5, length=width/2)
tx.set(0)
tx.pack()

ty = tk.Scale(root, label="Threshold Y", from_=-4095, to=4095, orient=tk.HORIZONTAL, width=5, length=width/2)
ty.set(0)
ty.pack()

button = tk.Button(root, text="Click to register ans exit calibration", command=ok)
button.pack(anchor=tk.CENTER)

canvas.pack(fill='both')

root.mainloop()
