#!/bin/sh

# create documentation from code files comments
doxygen

# compute documentation coverage
python3 -m coverxygen --xml-dir doc/doxygen/xml --src-dir . --output doc-coverage.info

# visualize summary in a console
lcov --summary doc-coverage.info

# generate files to visualize the results in an html-browsable format
genhtml --no-function-coverage --no-branch-coverage doc-coverage.info -o doc/coverage