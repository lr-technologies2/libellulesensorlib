/**
 * @file VL53L0X.cpp
 * @author Julia Cohen - LR Technologies (jcohen@lrtechnologies.fr)
 * @brief Implementation of the VL53L0X class (subclass of DistanceSensor)
 * @version 0.1
 * @date 2023-06-28
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "devices/sensors/VL53L0X.h"

#include "SerialMacros.h"
#include "Arduino.h"
#include "device_selection.h"
#include "communication/MessagesManager.h"

#define VL53L0X_MAX_DISTANCE 200
/**
 * @brief Construct a new VL53L0XSensor::VL53L0XSensor object
 *
 * @param idInit int representing the object ID
 * @param xpin GPIO pin of the sensor
 * @param addr I2C address of the sensor
 */
VL53L0XSensor::VL53L0XSensor(const int idInit, const int xpin, const int addr) : DistanceSensor(idInit), _xpin(xpin), _addressI2C(addr)
{
    SERIAL_PRINT("Creating sensor VL53L0X with id=");
    SERIAL_PRINT(_id);
    SERIAL_PRINT(" GPIO: ");
    SERIAL_PRINT(_xpin);
    SERIAL_PRINT(" I2C adress 0x");
    SERIAL_PRINTLN2(_addressI2C, HEX);

    _name = "VL53L0XSensor";
    _nullValue = 0.0;
    delay(10);

    pinMode(_xpin, OUTPUT);
    delay(10);
    setLow();
}

/**
 * @brief Initialize a single VL53L0X sensor
 * @details If multiple sensors are used, they have to be initialized
 * jointly (in a specific order).
 *
 */
bool VL53L0XSensor::initSensor()
{
    bool result = false;
    setLow();
    delay(10);
    setHigh();

    delay(10);
    result = initI2C();
    delay(10);
    return result;
}

/**
 * @brief Deactivate the sensor by setting the xshut pin to LOW
 *
 */
void VL53L0XSensor::setLow(void)
{
    digitalWrite(_xpin, LOW);
}

/**
 * @brief Activate the sensor by setting the xshut pin to HIGH
 *
 */
void VL53L0XSensor::setHigh(void)
{
    digitalWrite(_xpin, HIGH);
}

/**
 * @brief Initialize the I2C communication
 *
 */
bool VL53L0XSensor::initI2C()
{
    SERIAL_PRINT("  Init I2C for VL53L0X with ID ");
    SERIAL_PRINTLN(_id);
    if (!begin(_addressI2C, false))
    {
        SERIAL_PRINT("Sensor ");
        SERIAL_PRINT(_name.c_str());
        SERIAL_PRINT("ID: ");
        SERIAL_PRINT(_id);
        SERIAL_PRINT(" GPIO: ");
        SERIAL_PRINT(_xpin);
        SERIAL_PRINT(" is NOK at I2C adress 0x");
        SERIAL_PRINTLN2(_addressI2C, HEX);
        return false;
    }
    else
    {
        SERIAL_PRINT("Sensor ");
        SERIAL_PRINT(_name.c_str());
        SERIAL_PRINT("ID: ");
        SERIAL_PRINT(_id);
        SERIAL_PRINT(" GPIO: ");
        SERIAL_PRINT(_xpin);
        SERIAL_PRINT(" is OK at I2C adress 0x");
        SERIAL_PRINTLN2(_addressI2C, HEX);
        return true;
    }
}

/**
 * @brief Get the latest measurement, adds it to the data buffer and set the _ready flag
 * @param debug Set true to display debug log. Default to false
 *
 * @todo move _ready to Sensor (instead of VL53L0X)?
 */
void VL53L0XSensor::readMeas(const bool debug)
{

    VL53L0X_RangingMeasurementData_t measure;
    rangingTest(&measure, false); // pass in 'true' to get debug data printout!
    this->distance_value = measure.RangeMilliMeter;

    rawDataBuffer.push_back(this->distance_value);

    if (debug)
    {
        SERIAL_PRINTLN("[MEASURE VL53L0X]");
        SERIAL_PRINT("ID: ");
        SERIAL_PRINTLN(_id);
        SERIAL_PRINT("Distance(mm): ");
        SERIAL_PRINTLN(distance_value);
    }

    if (measure.RangeStatus != VL53L0X_STATE_RUNNING) // if measure successful
    {
        _ready = true;
    }
    else
    {
        _ready = false;
    }
}

/**
 * @brief return the sensor information as a formatted string.
 *
 * @return string with VL53L0X format [*sensor_ID* *className* *Dimension*D xpin:*_xpin* addressI2C:*_addressI2C*]
 */
std::string VL53L0XSensor::getConfig()
{
    SERIAL_PRINTLN("\n Get VL53L0XSensor configuration...");
    std::string config = "";
    config.append(std::to_string(_id));
    config.append(" ");
    config.append(_name);
    config.append(" ");
    config.append(std::to_string(_dimension));
    config.append("D");
    config.append(" xpin:");
    config.append(std::to_string(_xpin));
    config.append(" addressI2C:");
    config.append(std::to_string(_addressI2C));
    config.append("\n");
    return config;
}

/**
 * @brief Transforms raw data from int to float between 0. and 1., adds to data buffer
 *
 */
void VL53L0XSensor::formatData()
{
    for (int rawData : rawDataBuffer) // range-based for loop (C++11)
    {
        if (rawData != _nullValue)
        {
            // Each data is converted to float between 0. and 1.
            float data = (float)rawData / THRESHOLD_HIGH_GOOD;
            data = (data < 0.) ? 0. : data;
            data = (data > 1.) ? 1. : data;

            dataBuffer.push_back(data);
        }
    }

    rawDataBuffer.clear();
    _ready = false;
}

/**
 * @brief undo the sensors initialization
 * @details deactivate the sensor by settintg the GPIO to LOW
 */
void VL53L0XSensor::reset()
{
    digitalWrite(_xpin, LOW);
}

/**
 * @brief Save and clear the raw data buffer of the sensor
 * @details saves the current content of the raw data buffer into a new list and then clears the raw data buffer.
 * 
 * @return a list of int containing the current raw data buffer
 */
std::list<int> VL53L0XSensor::getRawDataBuffer()
{
    std::list<int> savedRawDataBuffer = rawDataBuffer;
    rawDataBuffer.clear();
    return savedRawDataBuffer;
}
/**
 * @brief Sends measurement data.
 * @details The measurement data is sent using the sendMessageDataSensor() method through JSON if needed.
 * 
 * @TODO Management of rawDataBuffer
 */
void VL53L0XSensor::sendMeas()
{

#if !USING_BLUETOOTH && !USING_USBHID
    std::string headers[2] = {"ID", "Distance"};
    int values[2] = {_id, this->distance_value};
    sendMessageDataSensor(headers, values, 2);
#endif
    rawDataBuffer.clear();  // Clear buffer (to change)
}

int VL53L0XSensor::DefaultMaxDistance()
{
    return VL53L0X_MAX_DISTANCE;
}