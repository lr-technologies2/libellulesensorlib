/**
 * @file random_sensor.cpp
 * @author Julia Cohen - LR Technologies (jcohen@lrtechnologies.fr)
 * @brief Implementation of the RandomSensor class
 * @version 0.1
 * @date 2023-08-22
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "devices/sensors/random_sensor.h"
#include <Arduino.h>

/**
 * @brief Construct a new Random Sensor:: Random Sensor object
 *
 * @param idInit int representing the object ID
 * @todo add the possibility for a number of dimensions higher than 1
 */
RandomSensor::RandomSensor(const int & idInit) : Sensor(idInit, "RandomSensor")
{
    _nullValue = 0.0;
    _dimension = 1;
}

/**
 * @brief Create a value to use as measure using the ESP32 random number generator
 * @param debug Set true to display debug log. Default to false
 */
void RandomSensor::readMeas(const bool debug)
{
    // Temporary clearing the buffer while data is not really sent.
    if (dataBuffer.size() >= 10)
    {
        dataBuffer.clear();
    }
    uint32_t rand_value = esp_random();
    dataBuffer.push_back((float)rand_value);
    _ready = true;
}
