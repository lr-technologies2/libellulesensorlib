/**
 * @file capacitive_sensor.cpp
 * @author 
 * @brief Implementation of the capacitive sensor class (subclass of Sensor, to subclass)
 * @version 0.1
 * @date 2023-24-10
 *
 * @copyright Copyright (c) 2023
 *
 */


#include "SerialMacros.h"
#include "devices/sensors/capacitive_sensor.h"
#include "device_selection.h"
#include "communication/MessagesManager.h"
#include <string>
#include <stdio.h>
#include <iostream>
#include <list>
#include <map>

#include "Arduino.h"

/**
 * @brief Constructeur classe CapacitiveSensor
 *
 * @param idInit ID du capteur capacitif
 * @param xpin pin du capteur capacitif
 */
CapacitiveSensor::CapacitiveSensor(int idInit, int xpin) : Sensor(idInit, "CapacitiveSensor")
{
    SERIAL_PRINT("Initializing capacitive sensor with id=");
    SERIAL_PRINTLN(_id);

    _nullValue = 0.0;
    _xpin = xpin;
    _dimension = 1;

    pinMode(_xpin, INPUT);
    delay(10);
}

/**
 * @brief Effectue une mesure capacitif
 *
 */
void CapacitiveSensor::readMeas(bool debug)
{
    int measure = touchRead(CAPACITIVE_XPIN1);
    rawDataBuffer.push_back(measure);
    _ready = true;

#if !USING_BLUETOOTH && !USING_USBHID
    std::string headers[2] = {"ID", "Touch"};
    int values[2] =  {_id, measure};
    sendMessageDataSensor(headers, values, 2);
#endif

    if(debug)
    {
        SERIAL_PRINTLN("[MEASURE CapacitiveSensor]");
        SERIAL_PRINT("ID: ");
        SERIAL_PRINTLN(_id);
        SERIAL_PRINT("Measure: ");
        SERIAL_PRINTLN(measure);
    }
}


/**
 * @brief Evaluate the raw data point against a threshold
 * 
 * @warning threshold is set to 20, must be calibrated for different sensors
 */
void CapacitiveSensor::formatData()
{
    const int threshold = 20; //Ne pas mettre un fil trop grand (diminue la précision et la plage de mesure)
    int touch = 0;

    for (int rawData : rawDataBuffer) // range-based for loop (C++11)
    {
        if (rawData != _nullValue)
        {
            touch = (rawData < threshold) ? 1 : 0;
            dataBuffer.push_back(touch);
        }
    }
    
    rawDataBuffer.clear();
    _ready = false;
}

/**
 * @brief Configuration du capteur
 *
 * @return Configuration du capteur capacitif (ID, name, dimension, pin)
 */
std::string CapacitiveSensor::getConfig()
{
    SERIAL_PRINTLN("\n Get CapacitiveSensor configuration...");

    std::string config = "n";
    config.append(std::to_string(_id));
    config.append(" ");
    config.append(_name);
    config.append(" ");
    config.append(std::to_string(_dimension));
    config.append("D");
    config.append(" xpin:");
    config.append(std::to_string(_xpin));
    config.append("\n");
    return config;
}