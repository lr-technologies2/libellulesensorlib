/**
 * @file distance_sensor.cpp
 * @author Julia Cohen - LR Technologies (jcohen@lrtechnologies.fr)
 * @brief Implementation of the DistanceSensor class (subclass of Sensor, to subclass)
 * @version 0.1
 * @date 2023-06-28
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "devices/sensors/distance_sensor.h"

#include <string>
#include <stdio.h>
#include <iostream>
#include <list>
#include <map>

#include "Arduino.h"

/**
 * @brief Construct a new DistanceSensor::DistanceSensor object
 *
 * @param idInit int representing the object ID
 */
DistanceSensor::DistanceSensor(const int & idInit) : Sensor(idInit, "DistanceSensor")
{
    _dimension = 1;
    _nullValue = -1.0;
}
