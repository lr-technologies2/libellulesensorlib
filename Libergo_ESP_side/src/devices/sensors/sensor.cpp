/**
 * @file sensor.cpp
 * @author Adrien Dorise (adorise@lrtechnologies.fr)
 * @brief Declaration of the Sensor base class.
 * @version 0.1
 * @date 2023-07-26
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "devices/sensors/sensor.h"

#include "SerialMacros.h"
#include "Arduino.h"
#include <iostream>


#define DEFAULT_MAX_DISTANCE 6000

/**
 * @brief Construct a new Sensor::Sensor object
 * 
 * @param idInit int representing the object ID
 * @param name string corresponding to the class name
 */
Sensor::Sensor(const int & idInit, const std::string & name) : _id(idInit), _name(name),
_dimension(0),
_nullValue(0.0f),
_ready(false)
{
}


/**
 * @brief Initialize a single sensor. This function is to be overrided in child classes
 * @details If multiple sensors are used, they have to be initialized
 * jointly (in a specific order).
 *
 */
bool Sensor::initSensor()
{
    return true;
}

/**
 * @brief return the sensor ID
 * 
 * @return int sensor ID
 */
int Sensor::getId()
{
    return _id;
}




/**
 * @brief return sensor's name (name of the class)
 * 
 * @return string name
 */
std::string Sensor::getName()
{
    return _name;
}

/**
 * @brief update internal values specifically for this sensor
 * @warning not implemented yet, inherited methods will probably change the argument(s) 
 * and return type
 */
void Sensor::calibrate()
{

}

/**
 * @brief calls the readMeas(), formatData() and send() methods in sequence.
 * @warning not implemented yet
 */
void Sensor::run()
{}

/**
 * @brief undo the sensors initialization
 * @details un-initialize the sensor when possible, disconnects from battery or 
 * communication channels if adequate
 */
void Sensor::reset()
{
    dataBuffer.clear();
}

/**
 * @brief read the sensor measurement and store the value in _rawDataBuffer when value is adequate
 * @param debug Set true to display debug log. Default to false
 */
void Sensor::readMeas(const bool debug)
{
    if(debug)
    {
        SERIAL_PRINTLN("Reading measure from Sensor");
    }
}

/**
 * @brief return the sensor information as a formatted string (format depends on sensor)
 
 *
 * @return string with base format [ \a sensor_ID \a className \a Dimension D ]
 */
std::string Sensor::getConfig()
{
    std::string config = "";
    config.append(std::to_string(_id));
    config.append(" ");
    config.append(_name);
    config.append(" ");
    config.append(std::to_string(_dimension));
    config.append("D\n");
    return config;
}

/**
 * @brief applies post-measurement formatting on data from _rawDataBuffer and place them in _dataBuffer
 *
 */
void Sensor::formatData()
{}

/**
 * @brief converts data from _dataBuffer into a message suitable for sending.
 * @warning not implemented yet, formats to define. Also, see if this is 
 * redundant with send(string header) or if both are necessary
 * 
 */
void Sensor::send()
{
}

/**
 * @brief Sends the data from dataBuffer in a message, with a custom header
 *
 * @param header type string
 * @param message type &string
 *
 * @todo return the message for other component to send
 * @warning Serial print for now, change to return the formatted message.
 */
std::string Sensor::send(const std::string & header)
{
    std::string message;
    SERIAL_PRINTLN("In Sensor.send(header)");

    while (dataBuffer.size() > 0)
    {
        // depile fifo list
        float measure = dataBuffer.front();
        dataBuffer.pop_front();

        SERIAL_PRINT(header.c_str());
        SERIAL_PRINTLN(measure);
        message.append(std::to_string(measure));
    }
    return message;
}

/**
 * @brief prints the sensor name and ID
 *
 */
void Sensor::printInfo()
{
    std::cout << "sensor: " << _name << _id <<std::endl;
}

/**
 * @brief Checks if the dataBuffer is empty
 * 
 * @return true if dataBuffer is empty, nothing to send
 * @return false if dataBuffer contians data to send
 */
bool Sensor::isQueueEmpty()
{
    return (dataBuffer.size()==0);
}

/**
 * @brief Indicates if the rawDataBuffer has data to review and possibly send.
 *
 * @return true if the _ready flag is set to True.
 * @return false if the _ready flag is set ot False.
 */
bool Sensor::getReady()
{
    return _ready;
}

/**
 * @brief Gets the raw data buffer.
 * @details This method retrieves the raw data buffer from the sensor.
 * 
 * @return A list of integers representing the raw data buffer.
 */
std::list<int> Sensor::getRawDataBuffer()
{
    std::list<int> savedRawDataBuffer = dataBuffer;
    dataBuffer.clear();
    return savedRawDataBuffer;
}

/**
 * @brief Sends measurement data.
 * @details This method sends measurement data from the sensor with JSON format if needed.
 * 
 */
void Sensor::sendMeas()
{
}

int Sensor::DefaultMaxDistance()
{
    return DEFAULT_MAX_DISTANCE;
}