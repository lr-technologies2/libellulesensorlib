/**
 * @file joystick_sensor.cpp
 * @author Adrien Dorise - LR Technologies (adorise@lrtechnologies.fr)
 * @brief Implementation of the JoystickSensor class
 * @version 1.0
 * @date 2023-10-31
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "devices/sensors/joystick_sensor.h"

#include "SerialMacros.h"
#include "Arduino.h"
#include "device_selection.h"
#include "communication/MessagesManager.h"


/** 
 * @brief Construct a new JoystickSensor::JoystickSensor object
 *
 * @param idInit int representing the object ID
 * @param xpin GPIO pin of the sensor
 * @param addr I2C address of the sensor
 */
JoystickSensor::JoystickSensor(int idInit, int xpin, int ypin) : Sensor(idInit, "JoystickSensor"), _xpin(xpin), _ypin(ypin)
{
    SERIAL_PRINT("Creating sensor Joystick with id=");
    SERIAL_PRINT(_id);
    SERIAL_PRINT(" GPIO X: ");
    SERIAL_PRINT(_xpin);
    SERIAL_PRINT(" GPIO Y: ");
    SERIAL_PRINTLN(_ypin);

    _nullValue = 0.0;
    delay(10);
}

/**
 * @brief Get the latest measurement, adds it to the data buffer and set the _ready flag
 * @param debug Set true to display debug log. Default to false
 */
void JoystickSensor::readMeas(bool debug)
{
    int value_x = 0;
    int value_y = 0;

    value_x = analogRead(_xpin);
    XrawDataBuffer.push_back(value_x);
    value_y = analogRead(_ypin);
    YrawDataBuffer.push_back(value_y);

#if !USING_BLUETOOTH && !USING_USBHID
    std::string headers[3] = {"ID", "X_value", "Y_value"};
    int values[3] =  {_id, value_x, value_y};
    sendMessageDataSensor(headers, values , 3);
#endif    
    if(debug)
    {
        SERIAL_PRINTLN("[MEASURE JOYSTICK]");
        SERIAL_PRINT("ID: ");
        SERIAL_PRINTLN(_id);
        SERIAL_PRINT("X_value: ");
        SERIAL_PRINTLN(value_x);
        SERIAL_PRINT("Y_value: ");
        SERIAL_PRINTLN(value_y);
    }

    _ready = true;
}


/**
 * @brief Transforms raw data from int to float between 0. and 1., adds to data buffer
 *
 */
void JoystickSensor::formatData()
{
    for (int rawData : XrawDataBuffer) // range-based for loop (C++11)
    {
        if (rawData != _nullValue)
        {
            // Each data is converted to float between 0. and 1.
            float data = (float)rawData / 255;
            data = (data < 0.) ? 0. : data;
            data = (data > 1.) ? 1. : data;

            dataBuffer.push_back(data);
        }
    }

    for (int rawData : YrawDataBuffer) // range-based for loop (C++11)
    {
        if (rawData != _nullValue)
        {
            // Each data is converted to float between 0. and 1.
            float data = float(rawData) / 255.0f;
            data = (data < 0.) ? 0. : data;
            data = (data > 1.) ? 1. : data;

            dataBuffer.push_back(data);
        }
    }
    
    XrawDataBuffer.clear();
    YrawDataBuffer.clear();
    _ready = false;
}

std::list<int> JoystickSensor::getXrawDataBuffer()
{
    std::list<int> savedRawDataBuffer = XrawDataBuffer;
    XrawDataBuffer.clear();
    return savedRawDataBuffer;
}

std::list<int> JoystickSensor::getYrawDataBuffer()
{
    std::list<int> savedRawDataBuffer = YrawDataBuffer;
    YrawDataBuffer.clear();
    return savedRawDataBuffer;
}
