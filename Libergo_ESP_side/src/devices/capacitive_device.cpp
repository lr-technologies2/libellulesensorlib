/**
 * @file capacitive_device.cpp
 * @author 
 * @brief Implementation of the capacitive sensor class
 * @version 0.1
 * @date 2023-10-25
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "devices/capacitive_device.h"
#include "SerialMacros.h"
#include "device_selection.h"

/**
 * @brief Construct a new Capacitive Device:: Capacitive Device object
 * 
 * @param idInit unique ID of this device
 */
CapacitiveDevice::CapacitiveDevice(const int idInit, const CommunicationType commType, std::shared_ptr<ConfigData> confData) : Device(idInit, "CapacitiveDevice", commType, confData)
{
    _numSensors = 1;
    _sensorList.push_back(std::make_shared<CapacitiveSensor>(1, CAPACITIVE_XPIN1 ));

}

