/**
 * @file light_mouse.cpp
 * @author Adrien Dorise - LR Technologies (adorise@lrtechnologies.fr)
 * @brief Implementation of the AirMouse class (subclass of Device)
 * @version 0.1
 * @date 2023-06-28
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "SerialMacros.h"
#include "devices/light_mouse.h"

#include "device_selection.h"

/**
 * @brief Construct a new LightMouse::LightMouse object
 *
 * @param idInit int representing the object ID
 * @param commType Communication type used between PC and ESP. Default to USB
 */
LightMouse::LightMouse(const int idInit, const CommunicationType commType, std::shared_ptr<ConfigData> confData) : Device(idInit, "LightMouse", commType, confData)
{
    SERIAL_PRINTLN("Constructor of LightMouse");

    _numSensors = 4;

    // Sensor 1
    _sensorList.push_back(std::make_shared<VL53L0XSensor>(1, LIGHTMOUSE_XPIN, LIGHTMOUSE_ADDRESS));
    delay(10);

}

/**
 * @brief calls the 4 sensors formatting method to fill their respective _dataBuffer 
 * attributes
 * 
 * @todo move this to the SensorHub
 * @todo find how to transmit isQueueEmpty from the sensors in the list to the hub
 *
 */
void LightMouse::formatData()
{
    SERIAL_PRINT("LightMouse formatData: ");
    bool emptyQueue = true;
    for (auto &s : _sensorList)
    {
        if (s->getReady())
            s->formatData();
            emptyQueue = emptyQueue && s->isQueueEmpty();
            SERIAL_PRINT2(s->isQueueEmpty(), DEC);
    }
    if (emptyQueue == false)
    {
        SERIAL_PRINTLN("  -> Added nullValue to dataBuffer (temporary)");
    }
}