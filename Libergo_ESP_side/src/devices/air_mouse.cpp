/**
 * @file air_mouse.cpp
 * @author Julia Cohen - LR Technologies (jcohen@lrtechnologies.fr)
 * @brief Implementation of the AirMouse class (subclass of Device)
 * @version 1.0
 * @date 2023-11-06
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "devices/air_mouse.h"
#include "SerialMacros.h"
#include "device_selection.h"

/**
 * @brief Construct a new AirMouse::AirMouse object
 *
 * @param idInit int representing the object ID
 * @param commType Communication type used between PC and ESP. Default to USB
 */
AirMouse::AirMouse(const int idInit, const CommunicationType commType, std::shared_ptr<ConfigData> confData) : Device(idInit, "AirMouse", commType, confData)
{
    SERIAL_PRINTLN("Constructor of AirMouse");

    _numSensors = 4;

    // Sensor 1
    _sensorList.push_back(std::make_shared<VL53L0XSensor>(1, AIRMOUSE_XPIN1, AIRMOUSE_ADDRESS1));
    delay(10);

    // Sensor 2
    _sensorList.push_back(std::make_shared<VL53L0XSensor>(2, AIRMOUSE_XPIN2, AIRMOUSE_ADDRESS2));
    delay(10);

    // Sensor 3
    _sensorList.push_back(std::make_shared<VL53L0XSensor>(3, AIRMOUSE_XPIN3, AIRMOUSE_ADDRESS3));
    delay(10);

    // Sensor 4
    _sensorList.push_back(std::make_shared<VL53L0XSensor>(4, AIRMOUSE_XPIN4, AIRMOUSE_ADDRESS4));
    delay(10);
}

/**
 * @brief This method is used to read data from 4 sensors and format the data for further processing.
 * It also handles the case when the sensor data is above a certain threshold.
 *
 */
void AirMouse::hidController()
{
    _sensorList[0]->readMeas();
    int s1_distance_raw = (_sensorList[0]->getRawDataBuffer()).front();

    _sensorList[1]->readMeas();
    int s2_distance_raw = (_sensorList[1]->getRawDataBuffer()).front();

    _sensorList[2]->readMeas();
    int s3_distance_raw = (_sensorList[2]->getRawDataBuffer()).front();

    _sensorList[3]->readMeas();
    int s4_distance_raw = (_sensorList[3]->getRawDataBuffer()).front();


    auto axisX = std::static_pointer_cast<VL53L0XSensor>(_sensorList[0]);
    auto axisY = std::static_pointer_cast<VL53L0XSensor>(_sensorList[1]);

    // Format the raw data from the sensors
    int s1_data = s1_distance_raw;
    int s2_data = s2_distance_raw;
    int s3_data = s3_distance_raw;
    int s4_data = s4_distance_raw;

    // Handle the case when the sensor data is above a certain threshold
    int maxXDistance = m_confData->GetSensors(0).m_maxDistance;
    int maxYDistance = m_confData->GetSensors(1).m_maxDistance;
    int maxRightDistance = m_confData->GetSensors(2).m_maxDistance;
    int maxLeftDistance = m_confData->GetSensors(3).m_maxDistance;
    int axisXNeutral = m_confData->GetSensors(0).m_neutral;
    int axisYNeutral = m_confData->GetSensors(1).m_neutral;

    if (s1_distance_raw > maxXDistance)
    {
        s1_data = 0;
    }
    else
    {
        if(s1_data < maxXDistance/2 - axisXNeutral) {
            int max = maxXDistance/2;
            int min = 0;
            s1_data = -100 + int(float(s1_data)/float(max-min)*100.0f);
        }
        else if(s1_data > maxXDistance/2 + axisXNeutral) {
            int min = maxXDistance/2;
            int max = maxXDistance;
            s1_data = int(float(s1_data - maxXDistance/2)/float(max-min)*100.0f);
        }
        else s1_data = 0;
    }

    if (s2_distance_raw > maxYDistance)
    {
        s2_data = 0;
    }
    else
    {
        if(s2_data < maxYDistance/2 - axisYNeutral) {
            int max = maxXDistance/2;
            int min = 0;
            s2_data = -100 + int(float(s2_data)/float(max-min)*100.0f);
        } 
        else if(s2_data > maxYDistance/2 + axisYNeutral) {
            int min = maxXDistance/2;
            int max = maxXDistance;
            s2_data = int(float(s2_data-maxXDistance/2)/float(max-min)*100.0f);
        }
        else s2_data = 0;
    }

    if (s3_distance_raw < maxRightDistance)
    {
        s3_data = 1;
    }
    else
    {
        s3_data = 0;
    }

    if (s4_distance_raw < maxLeftDistance)
    {
        s4_data = 1;
    }
    else
    {
        s4_data = 0;
    }

    // Update the sensor data
    s1_distance = s1_data;
    s2_distance = s2_data;
    s3_distance = s3_data;
    s4_distance = s4_data;
}

/**
 * @brief This method is used to get the sensor data from the first sensor.
 *
 * @return int: The sensor data from the first sensor.
 */
int AirMouse::getXDistance()
{
    return s1_distance;
}

/**
 * @brief This method is used to get the sensor data from the second sensor.
 *
 * @return int: The sensor data from the second sensor.
 */
int AirMouse::getYDistance()
{
    return s2_distance;
}

/**
 * @brief This method is used to get the sensor data from the third sensor.
 *
 * @return int: The sensor data from the third sensor.
 */
int AirMouse::getClickRightDistance()
{
    return s3_distance;
}

/**
 * @brief This method is used to get the sensor data from the fourth sensor.
 *
 * @return int: The sensor data from the fourth sensor.
 */
int AirMouse::getClickLeftDistance()
{
    return s4_distance;
}
