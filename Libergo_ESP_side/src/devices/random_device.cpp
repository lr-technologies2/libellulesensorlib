/**
 * @file random_device.cpp
 * @author Julia Cohen (jcohen@lrtechnologies.fr)
 * @brief Implementation of the RandomDevice class
 * @version 0.1
 * @date 2023-08-22
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "devices/random_device.h"
#include "SerialMacros.h"

/**
 * @brief Construct a new Random Device:: Random Device object
 * 
 * @param idInit unique ID of this device
 * @param commType Communication type used between PC and ESP. Default to USB
 */
RandomDevice::RandomDevice(const int idInit, const CommunicationType commType, std::shared_ptr<ConfigData> confData) : Device(idInit, "RandomDevice", commType, confData),
randomData(0)
{
    _numSensors = 1;
    _sensorList.push_back(std::make_shared<RandomSensor>(0));

}

void RandomDevice::hidController()
{
    _sensorList[0]->readMeas(false);
    randomData = (_sensorList[0]->getRawDataBuffer()).front();
}