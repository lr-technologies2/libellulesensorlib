/**
 * @file jerry_mouse.cpp
 * @author Adrien Dorise - LR Technologies (adorise@lrtechnologies.fr)
 * @brief Implementation of the JerryMouse class (subclass of Device)
 * @version 1.0
 * @date 2023-11-06
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "devices/jerry_mouse.h"
#include "devices/sensors/joystick_sensor.h"
#include "SerialMacros.h"
#include "device_selection.h"

/**
 * @brief Construct a new JerryMouse::JerryMouse object
 *
 * @param idInit int representing the object ID
 * @param commType Communication type used between PC and ESP. Default to USB
 */
JerryMouse::JerryMouse(const int idInit, const CommunicationType commType, std::shared_ptr<ConfigData> confData) : Device(idInit, "JerryMouse", commType, confData)
{
    SERIAL_PRINTLN("Constructor of JerryMouse");

    _numSensors = 3;

    // Sensor 1 - Joystick
    _sensorList.push_back(std::make_shared<JoystickSensor>(1, JERRYMOUSE_XPIN, JERRYMOUSE_YPIN));
    
    // Sensor 2 - VL53L0X
    _sensorList.push_back(std::make_shared<VL53L0XSensor>(2, JERRYMOUSE_RCPIN, JERRYMOUSE_ADDRESSRC));
    
    //Sensor 3 - VL53L0X
    _sensorList.push_back(std::make_shared<VL53L0XSensor>(3, JERRYMOUSE_LCPIN, JERRYMOUSE_ADDRESSLC));

    delay(10);
}

/**
 * @brief Activate mouse control with ESP32
 * 
 * @return sensorReady flag indicating if there is valid data to send.
*/
bool JerryMouse::controller()
{
    bool sensorReady = false;
    _ready = false;

    //Joystick
    for(int i=0; i<_sensorList.size(); i++)
    {
        _sensorList[i]->readMeas();
        sensorReady = sensorReady || _sensorList[i]->getReady();
        _ready = _ready || _sensorList[i]->getReady();

    }
    
    sleep(SAMPLING_TIME);

    return sensorReady;
}

void JerryMouse::hidController()
{
    _sensorList[0]->readMeas(false);
    auto joystickSensor = std::static_pointer_cast<JoystickSensor>(_sensorList[0]);
    int joystick_rawX = (joystickSensor->getXrawDataBuffer()).front();
    int joystick_rawY = (joystickSensor->getYrawDataBuffer()).front();

    _sensorList[1]->readMeas(false);
    int s1_distance_raw = (_sensorList[1]->getRawDataBuffer()).front();

    _sensorList[2]->readMeas(false);
    int s2_distance_raw = (_sensorList[2]->getRawDataBuffer()).front();

    // Format the raw data from the sensors
    int jx_data = -1;
    int jy_data = -1;
    int s1_data = -1;
    int s2_data = -1;

    int max1Distance = m_confData->GetSensors(0).m_maxDistance;
    int max2Distance = m_confData->GetSensors(1).m_maxDistance;
    auto joystick = m_confData->GetJoystick(0);

    float dataX = float(joystick_rawX)-float(joystick.m_maxX-joystick.m_minX)/2.0f;
    float dataY = float(joystick_rawY)-float(joystick.m_maxY-joystick.m_minY)/2.0f;
    if(std::abs(dataX) < joystick.m_neutralX)
    {
        jx_data = 0;
    }
    else
    {
        jx_data = int (dataX/float(joystick.m_maxX-joystick.m_minX)*200.0f);//value (-100=>100)
    }

    if(std::abs(dataY) < joystick.m_neutralY)
    {
        jy_data = 0;
    }
    else
    {
        jy_data = int (dataY/float(joystick.m_maxY-joystick.m_minY)*200.0f);//value (-100=>100)
    }

    if (s1_distance_raw < max1Distance)
    {
        s1_data = 1;
    }
    else
    {
        s1_data = 0;
    }

    if (s2_distance_raw < max2Distance)
    {
        s2_data = 1;
    }
    else
    {
        s2_data = 0;
    }

    // Update the sensor data
    s1_distance = s1_data;
    s2_distance = s2_data;
    jx_distance = jx_data;
    jy_distance = jy_data; 
}

int JerryMouse::getXDistance()
{
    return jx_distance;
}
int JerryMouse::getYDistance()
{
    return jy_distance;
}
int JerryMouse::getClickRightDistance()
{   
    return s1_distance;
}
int JerryMouse::getClickLeftDistance()
{
    return s2_distance;
}
