/**
 * @file device.cpp
 * @author Julia Cohen, Adrien Dorise ({jcohen, adorise}@lrtechnologies.fr)
 * @brief Implementation of the Device base class
 * @version 1.0
 * @date 2023-11-06
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "SerialMacros.h"
#include "devices/device.h"

#include <stdio.h>
#include <iostream>
#include "Arduino.h"
#include "device_selection.h"

#include "devices/sensors/VL53L0X.h"
#include "devices/air_mouse.h"

/**
 * @brief Construct a new Device::Device object
 * 
 * @param idInit int representing the object ID
 * @param name string corresponding to the class name
 * @param commType Communication type used between PC and ESP. Default to USB
 */
Device::Device(const int idInit, const std::string name, const CommunicationType commType, std::shared_ptr<ConfigData> confData) : _id(idInit), _name(name), _commType(commType), m_confData(confData)
{
}

/**
 * @brief Destroy the Device::Device object
 * 
 */
Device::~Device()
{
    _sensorList.clear();
}

/** initSensors
 * @brief Initialize the device by creating and initializing the sensors. 
 * This funciton must be overrided by inherited classes.
 * @todo complete Device::initSensors() with every sensor
 */
bool Device::initSensors(void)
{
    bool result = true;
    std::string msg = _name + ": Initializing the sensors Manage list of sensors...";
    SERIAL_PRINTLN(msg.c_str());
    for (auto &s : _sensorList)
    {
        result = result && s->initSensor();
    }
    if(result) {
        SERIAL_PRINTLN("Initialization succeeded");
    } else {
        SERIAL_PRINTLN("Initialization failed!");
    }
    return result;    
}

void Device::hidController()
{

}

/** calibrate
 * @brief Calibrate every sensor
 */
void Device::calibrate(void)
{
    for (std::vector<std::shared_ptr<Sensor>>::iterator it = _sensorList.begin(); it != _sensorList.end(); it++)
    {
        (*it)->calibrate();
    }
}

/** scan
 * @brief Make a measurement for every sensor and update the _ready flag.
 * @param debug Set true to display debug log. Default to false
 * 
 * @return sensorReady flag indicating if there is valid data to send.
 */
bool Device::scan(bool debug)
{
    if(debug)
    {
        SERIAL_PRINTLN("Device scan(): reading sensors from _sensor_list.");
    }

    bool sensorReady = false;
    _ready = false;
    for (auto &sensor : _sensorList)
    {
        /* Debug print  
        SERIAL_PRINT("In sensors list loop. Name: ");
        SERIAL_PRINT(sensor->getName().c_str());
        SERIAL_PRINT(" Id: ");
        SERIAL_PRINTLN(sensor->getId()); 
        */
        
        sensor->readMeas(debug);
        sensorReady = sensorReady || sensor->getReady();
        _ready = _ready || sensor->getReady();

        /*Debug print
        SERIAL_PRINT("Sensor Manager _ready=");
        SERIAL_PRINT(sensor->getReady(), DEC);
        SERIAL_PRINT(" -- ");
        */
        
    }
    
    /*Debug print
    SERIAL_PRINT(_sensorList[0]->getReady(), DEC);
    SERIAL_PRINT(" -- ");
    SERIAL_PRINTLN(sensorReady, DEC);
    */

    return sensorReady;
}

/** evaluate
 * @brief Evaluate if there is sensor data to send
 *
 * @return true if there is data to send
 * @return false if sensors have no ready data
 */
bool Device::evaluate(void)
{
    SERIAL_PRINTLN("Device evaluate(): is there any measurement value in _sensor_list?");

    for (auto &sensor : _sensorList)
    {
        // SERIAL_PRINT("In sensors list loop. Name: ");
        // SERIAL_PRINT(sensor->getName().c_str());
        // SERIAL_PRINT(" Id: ");
        // SERIAL_PRINTLN(sensor->getId());
        sensor->formatData();
        if (!sensor->isQueueEmpty())
        {
            SERIAL_PRINTLN("QUEUE NOT EMPTY");
            return true;
        }
    }
    SERIAL_PRINTLN("QUEUE EMPTY");
    return false;
}

/** get_ready
 * @brief Verify the availability of the device
 * 
 * @return true if the device is ready
 * @return false if the device is not ready (currently doing an other action / sleep mode...)
 */
bool Device::get_ready()
{
    return _ready;
}


/** reset_sensors
 * @brief resets the device by deactivating the list of sensors
 *
 */
void Device::reset_sensors(void)
{
    for (auto &s : _sensorList)
    {
        s->reset();
    }
}


/** getConfig
 * @brief get the sensors configuration
 *
 * @return string
 */
std::string Device::getConfig(void)
{
    std::string config = "";
    for (auto s : _sensorList)
    {
        config.append(s->getConfig());
    }
    return config;
}

// TODO Device::updateConfig()
/** updateConfig
 * @brief Decodes the new configuration received from a message and applies it 
 * to the managed sensors.
 * @warning not implemented yet.
 *
 * @param new_config type string the formatted ocnfiguraiton to apply
 */
void Device::updateConfig(std::string new_config)
{
    SERIAL_PRINTLN("Device updateConfig() not implemented yet.");
}

// TODO Device::send()
/** send
 * @brief Send a message containing measurements form the sensors.
 * @warning not implemented yet
 * 
 * @return string message to send
 *
 */
std::string Device::send()
{
    std::string message, header;
    header = "sensors manager temp header:";
    // SERIAL_PRINTLN("Device send() not implemented yet.");
    SERIAL_PRINTLN("Temporary implementation of Device send().");
    for (auto s:_sensorList)
    {
        // message.append(s->send(header));
        message.append(s->getName()); // temporary 
        // TODO s->send(header) is defined in Sensor, must be redefined in SensorHub to loop over every dataBuffer of the sensors list
    }
    return message;
}

/** printInfo
 * @brief Print the number of sensors, their names and IDs.
 *
 */
void Device::printInfo(void)
{
    SERIAL_PRINT("\nGot ");
    SERIAL_PRINT(_numSensors);
    SERIAL_PRINTLN(" sensors in list:");

    for (auto s:_sensorList)
    {
        SERIAL_PRINT("  - ID ");
        SERIAL_PRINT(s->getId());
        SERIAL_PRINT(" : ");
        SERIAL_PRINTLN(s->getName().c_str());
    }
}
