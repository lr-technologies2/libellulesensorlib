#include "ConfigData.h"

#include "SerialMacros.h"
#include <fstream>
#include<sstream>
#include <map>
#include <Arduino.h>

std::string JoystickConfig::save(std::size_t i)
{
    std::string result;
    result += m_name+std::to_string(i) + "m_minX= " + std::to_string(m_minX) + "\n";
    result += m_name+std::to_string(i) + "m_maxX= " + std::to_string(m_maxX) + "\n";
    result += m_name+std::to_string(i) + "m_minY= " + std::to_string(m_minY) + "\n";
    result += m_name+std::to_string(i) + "m_maxY= " + std::to_string(m_maxY) + "\n";
    result += m_name+std::to_string(i) + "m_neutralX= " + std::to_string(m_neutralX) + "\n";
    result += m_name+std::to_string(i) + "m_neutralY= " + std::to_string(m_neutralY) + "\n";
    return result;
}


void JoystickConfig::load(size_t i, const std::map<std::string, int> & data)
{
    auto loadfunc = [i, data, this](const std::string & dataName, int & dataLoad) {
        auto search  = data.find(std::string(m_name+std::to_string(i) + dataName));
        if(search != data.end()) {
            dataLoad = search->second;
        }
    };
    loadfunc("m_minX", m_minX);
    loadfunc("m_maxX", m_maxX);
    loadfunc("m_minY", m_minY);
    loadfunc("m_maxY", m_maxY);
    loadfunc("m_neutralX", m_neutralX);
    loadfunc("m_neutralY", m_neutralY);
}

std::string SensorConfig::save(std::size_t i)
{   
    std::string result;
    result += m_name+std::to_string(i) + "m_neutral= " + std::to_string(m_neutral) + "\n";
    result += m_name+std::to_string(i) + "m_maxDistance= " + std::to_string(m_maxDistance) + "\n";
    return result;
}


void SensorConfig::load(size_t i, const std::map<std::string, int> & data)
{
    auto loadfunc = [i, data, this](const std::string & dataName, int & dataLoad) {
        auto search  = data.find(std::string(m_name+std::to_string(i) + dataName));
        if(search != data.end()) {
            dataLoad = search->second;
        }
    };
    loadfunc("m_neutral", m_neutral);
    loadfunc("m_maxDistance", m_maxDistance);
}

std::vector<std::string> SeparateString(std::string & i_chaine, char s) 
{ 
    std::istringstream iss( i_chaine ); 
    std::string mot; 
    std::vector<std::string> ret;
    while ( std::getline( iss, mot, s ) ) 
    { 
        ret.push_back(mot);
    } 
    return ret;
}


ConfigData::ConfigData(const available_device dType): m_file("/ConfigData.conf"), m_sensibility(10),
m_name(getDeviceType(dType))
{
    Default(dType);
    if (ConfigFiles.getErrorStatus())
    {
      SERIAL_PRINTLN("ConfigFiles has error status.");
    }
    else
    {
      std::string FileContent;
      SERIAL_PRINT("ID File Content: ");
      ConfigFiles.initID();
      SERIAL_PRINTLN(ConfigFiles.readID().c_str());
      SERIAL_PRINTLN("Sensor File Content:\n");
      FileContent = ConfigFiles.readSensor();
      SERIAL_PRINTLN(FileContent.c_str());
      SERIAL_PRINT("Sensor File Content size:");
      SERIAL_PRINTLN(FileContent.size());
      if(FileContent.size()<=1) {
        FileContent = ConfigFiles.readSensor();
        SERIAL_PRINTLN(FileContent.c_str());
      }
      if(!Load(FileContent)){
        SERIAL_PRINTLN("load error");
        Save();
        } else {
            SERIAL_PRINTLN("Configuration file load with success");
        }
    }
}

ConfigData::~ConfigData()
{

}

void ConfigData::Default(const available_device dType)
{
    if (dType == available_device::JERRY_MOUSE) {
        m_joysticks.push_back(JoystickConfig());
        m_distanceSensors.push_back(SensorConfig());
        m_distanceSensors.push_back(SensorConfig());
    } else if(dType == available_device::AIR_MOUSE) {
        m_distanceSensors.push_back(SensorConfig());
        m_distanceSensors.push_back(SensorConfig());
        m_distanceSensors.push_back(SensorConfig());
        m_distanceSensors.push_back(SensorConfig());
    } else {
        // empty
    }
}
const JoystickConfig ConfigData::GetJoystick(size_t id)
{
    return m_joysticks.at(id);
}
const SensorConfig ConfigData::GetSensors(size_t id)
{
    return m_distanceSensors.at(id);
}

const int ConfigData::GetSensiblity()
{
    return m_sensibility;
}

std::string ConfigData::Save()
{
    std::string data;
    data += "m_name=" + m_name + "\n";
    data += "m_sensibility=" + std::to_string(m_sensibility) + "\n";
    for (std::size_t i = 0; i < m_joysticks.size(); i++) {
        data += m_joysticks[i].save(i);
    }
    for (std::size_t i = 0; i < m_distanceSensors.size(); i++) {
        data += m_distanceSensors[i].save(i);
    }
    
    ConfigFiles.writeSensor(data);
    return data;
}

bool ConfigData::Load(std::string data)
{
    std::vector<std::string> text = SeparateString(data , '\n');
    std::map<std::string, int> num_data;
    std::string name;
    
    for(size_t i = 0; i < text.size(); i++){
        if(m_name != text[i]) {
            std::vector<std::string> line = SeparateString(text[i], '=');
            if (line.size()  == 2 && line[0]!= "m_name") {
                try {
                    int val = std::stoi( line[1]);
                    num_data[line[0]] = val;
                }
                catch (std::invalid_argument const& ex) {
                    std::string msg(" confdata " + line[0] + " std::invalid_argument::what(): " + ex.what());
                    SERIAL_PRINTLN(msg.c_str());
                    return false;
                }
                catch (std::out_of_range const& ex) {
                    std::string msg(" confdata " + line[0] + " std::out_of_range::what(): " + ex.what());
                    SERIAL_PRINTLN(msg.c_str());
                    return false;
                }
            }
            if (line[0]== "m_name"){
                name = line[1];
            }
        }
    }
    if(name != m_name) {
        std::string msg("unknow device name = " + name + " expected= " + m_name);
        SERIAL_PRINTLN(msg.c_str());
        return false;
    }
    auto search  = num_data.find("m_sensibility");
    if(search != num_data.end()) {
        m_sensibility = search->second;
    }
    for (std::size_t i = 0; i < m_joysticks.size(); i++) {
        m_joysticks[i].load(i, num_data);
    }
    for (std::size_t i = 0; i < m_distanceSensors.size(); i++) {
        m_distanceSensors[i].load(i, num_data);
    }
    return true;
}