# ESP32-NimBLE-Mouse Library

This library provides BLE mouse functionality for ESP32 devices using the NimBLE library. It is derived from the [ESP32-BLE-Mouse](https://github.com/T-vK/ESP32-BLE-Mouse) library, which was originally based on an issue from the official ESP32-snippets repository.

## Source

This library is a fork of the original [ESP32-NimBLE-Mouse](https://github.com/wakwak-koba/ESP32-NimBLE-Mouse) repository, which was itself derived from [ESP32-BLE-Mouse](https://github.com/T-vK/ESP32-BLE-Mouse) which was derived from an issue from the official  
ESP32-snippets repo which is under APACHE 2.0 License.  

## License

The ESP32-NimBLE-Mouse library is distributed under the Apache License 2.0, the same license as the original ESP32-snippets repository.

## Contents

- `BleMouse.h` and `BleMouse.cpp`: These files contain the BLE mouse implementation for ESP32 devices using the NimBLE library.
- `BleConnectionStatus.h` and `BleConnectionStatus.cpp`: These files manage the BLE connection status for the BLE mouse functionality.

## Changes

- Added a `moveMouse` method to control mouse movements.
- Modified the `taskServer` method to add a service for receiving data from a PC for calibration purposes.