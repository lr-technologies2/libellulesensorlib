#include "BleConnectionStatus.h"

BleConnectionStatus::BleConnectionStatus(void):
connected(false),
inputMouse(nullptr),
outputMouse(nullptr)
{
}

BleConnectionStatus::BleConnectionStatus(const BleConnectionStatus& ble)
{
  this->connected=ble.connected;
  this->inputMouse=ble.inputMouse;
  this->outputMouse=ble.outputMouse;
}

void BleConnectionStatus::onConnect(NimBLEServer *pServer)
{
  this->connected = true;
}

void BleConnectionStatus::onDisconnect(NimBLEServer *pServer)
{
  this->connected = false;
}
