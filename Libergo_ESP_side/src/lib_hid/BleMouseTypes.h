/**
 * @file BleMouseTypes.h
 * @author Jayson Dangremont - LR Technologies (dangremontjayson.pro@gmail.com)
 * @brief Defines usefull types for the bluetooth mouse mouvements
 * @version 1.0
 * @date 2024-02-15
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef ESP32_BLE_MOUSE_TYPES_H
#define ESP32_BLE_MOUSE_TYPES_H

/** @def RIGHT_MOVEMENT
 * @brief Right mouse movement
 */
#define RIGHT_MOVEMENT  {1, 0}

/** @def LEFT_MOVEMENT
 * @brief Left mouse movement
 */
#define LEFT_MOVEMENT   {-1, 0}

/** @def UP_MOVEMENT
 * @brief Up mouse movement
 */
#define UP_MOVEMENT     {0, -1}

/** @def LEFT_MOVEMENT
 * @brief Down mouse movement
 */
#define DOWN_MOVEMENT   {0, 1}

/** @def MOUSE_POSSIBLE_ACTIONS
 * @brief Enum of possible actions for the mouse 
 */
enum MOUSE_POSSIBLE_ACTIONS
{
  MOVE_RIGHT = 0,
  MOVE_LEFT,
  MOVE_UP,
  MOVE_DOWN
};

/** @def MOUSE_MOVEMENTS_MAP
 * @brief Struct to define axis for mouse movements
 */
typedef struct
{
  int dX;
  int dY;
} MOUSE_MOVEMENTS_MAP;

/** @def MOVEMENTS_SIZE
 * @brief Number of possible mouse movements
 */
#define MOVEMENTS_SIZE (4)

/** @def MOUSE_MOVEMENTS_TABLE
 * @brief Table for mouse movements
 */
const MOUSE_MOVEMENTS_MAP MOUSE_MOVEMENTS_TABLE[MOVEMENTS_SIZE] = {
    RIGHT_MOVEMENT,
    LEFT_MOVEMENT,
    UP_MOVEMENT,
    DOWN_MOVEMENT, 
};

/** @def _hidReportDescriptor
 * @brief Descriptor for hid report
 */
static const uint8_t _hidReportDescriptor[] = {
    USAGE_PAGE(1), 0x01,      //    USAGE_PAGE (Generic Desktop)
    USAGE(1), 0x02,           //    USAGE (Mouse)
    COLLECTION(1), 0x01,      //    COLLECTION (Application)
    USAGE(1), 0x01,           //    USAGE (Pointer)
    COLLECTION(1), 0x00,      //    COLLECTION (Physical)
    // ------------------------------------------------- Buttons (Left, Right, Middle, Back, Forward)
    USAGE_PAGE(1), 0x09,      //    USAGE_PAGE (Button)
    USAGE_MINIMUM(1), 0x01,   //    USAGE_MINIMUM (Button 1)
    USAGE_MAXIMUM(1), 0x05,   //    USAGE_MAXIMUM (Button 5)
    LOGICAL_MINIMUM(1), 0x00, //    LOGICAL_MINIMUM (0)
    LOGICAL_MAXIMUM(1), 0x01, //    LOGICAL_MAXIMUM (1)
    REPORT_SIZE(1), 0x01,     //    REPORT_SIZE (1)
    REPORT_COUNT(1), 0x05,    //    REPORT_COUNT (5)
    HIDINPUT(1), 0x02,        //    INPUT (Data, Variable, Absolute) ;5 button bits
    // ------------------------------------------------- Padding
    REPORT_SIZE(1), 0x03,     //    REPORT_SIZE (3)
    REPORT_COUNT(1), 0x01,    //    REPORT_COUNT (1)
    HIDINPUT(1), 0x03,        //    INPUT (Constant, Variable, Absolute) ;3 bit padding
    // ------------------------------------------------- X/Y position, Wheel
    USAGE_PAGE(1), 0x01,      //    USAGE_PAGE (Generic Desktop)
    USAGE(1), 0x30,           //    USAGE (X)
    USAGE(1), 0x31,           //    USAGE (Y)
    USAGE(1), 0x38,           //    USAGE (Wheel)
    LOGICAL_MINIMUM(1), 0x81, //    LOGICAL_MINIMUM (-127)
    LOGICAL_MAXIMUM(1), 0x7f, //    LOGICAL_MAXIMUM (127)
    REPORT_SIZE(1), 0x08,     //    REPORT_SIZE (8)
    REPORT_COUNT(1), 0x03,    //    REPORT_COUNT (3)
    HIDINPUT(1), 0x06,        //    INPUT (Data, Variable, Relative) ;3 bytes (X,Y,Wheel)
    // ------------------------------------------------- Horizontal wheel
    USAGE_PAGE(1), 0x0c,      //    USAGE PAGE (Consumer Devices)
    USAGE(2), 0x38, 0x02,     //    USAGE (AC Pan)
    LOGICAL_MINIMUM(1), 0x81, //    LOGICAL_MINIMUM (-127)
    LOGICAL_MAXIMUM(1), 0x7f, //    LOGICAL_MAXIMUM (127)
    REPORT_SIZE(1), 0x08,     //    REPORT_SIZE (8)
    REPORT_COUNT(1), 0x01,    //    REPORT_COUNT (1)
    HIDINPUT(1), 0x06,        //    INPUT (Data, Var, Rel)
    END_COLLECTION(0),        //    END_COLLECTION
    END_COLLECTION(0)         //    END_COLLECTION
};

#endif // ESP32_BLE_MOUSE_TYPES_H