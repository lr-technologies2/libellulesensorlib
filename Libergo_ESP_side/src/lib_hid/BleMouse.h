#ifndef ESP32_BLE_MOUSE_H
#define ESP32_BLE_MOUSE_H
#include "sdkconfig.h"
#if defined(CONFIG_BT_ENABLED)

#include "BleConnectionStatus.h"
#include "NimBLEHIDDevice.h"
#include "NimBLECharacteristic.h"

#define MOUSE_LEFT 1
#define MOUSE_RIGHT 2
#define MOUSE_MIDDLE 4
#define MOUSE_BACK 8
#define MOUSE_FORWARD 16
#define MOUSE_ALL (MOUSE_LEFT | MOUSE_RIGHT | MOUSE_MIDDLE) #For compatibility with the Mouse library

class BleMouse
{
private:
  uint8_t _buttons;
  BleConnectionStatus *connectionStatus;
  NimBLEHIDDevice *hid;
  NimBLECharacteristic *inputMouse;
  void buttons(uint8_t b);
  void rawAction(uint8_t msg[], char msgSize);
  static void taskServer(void *pvParameter);
  bool startCalibratingXSensor = false;
  bool startCalibratingYSensor = false;
  bool startCalibratingLeftClickSensor = false;
  bool startCalibratingRightClickSensor = false;

public:
  explicit BleMouse(const std::string &deviceName = "ESP32-Mouse", const std::string &deviceManufacturer = "Espressif", const uint8_t & batteryLevel = 100);
  BleMouse(const BleMouse & ble);
  BleMouse & operator =(const BleMouse & ble);
  void begin(void);
  void end(void);
  void click(uint8_t b = MOUSE_LEFT);
  void move(signed char x, signed char y, signed char wheel = 0, signed char hWheel = 0);
  void press(uint8_t b = MOUSE_LEFT);     // press LEFT by default
  void release(uint8_t b = MOUSE_LEFT);   // release LEFT by default
  bool isPressed(uint8_t b = MOUSE_LEFT); // check LEFT by default
  bool isConnected(void);
  void setBatteryLevel(uint8_t level);
  uint8_t batteryLevel;
  std::string deviceManufacturer;
  std::string deviceName;
  void moveMouse(uint8_t direction, uint8_t pixelX, uint8_t pixelY);
  bool getStartCalibrationYSensor();
  bool getStartCalibrationXSensor();
  bool getStartCalibrationLeftClickSensor();
  bool getStartCalibrationRightClickSensor();
  void sendDataToPC(uint8_t data);

protected:
  virtual void onStarted(NimBLEServer *pServer){};
};

#endif // CONFIG_BT_ENABLED
#endif // ESP32_BLE_MOUSE_H
