#ifndef ESP32_BLE_CONNECTION_STATUS_H
#define ESP32_BLE_CONNECTION_STATUS_H
#include "sdkconfig.h"
#if defined(CONFIG_BT_ENABLED)

#include <NimBLEServer.h>
#include "NimBLECharacteristic.h"

class BleConnectionStatus : public NimBLEServerCallbacks
{
public:
  BleConnectionStatus(void);
  BleConnectionStatus(const BleConnectionStatus&  ble);

  bool connected;
  void onConnect(NimBLEServer *pServer)override; 
  void onDisconnect(NimBLEServer *pServer)override;
  NimBLECharacteristic *inputMouse;
  NimBLECharacteristic* outputMouse;
};

#endif // CONFIG_BT_ENABLED
#endif // ESP32_BLE_CONNECTION_STATUS_H
