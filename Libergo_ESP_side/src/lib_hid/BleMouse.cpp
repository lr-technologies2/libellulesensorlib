#include <NimBLEDevice.h>
#include <NimBLEUtils.h>
#include <NimBLEServer.h>
#include "NimBLEHIDDevice.h"
#include "HIDTypes.h"
#include "HIDKeyboardTypes.h"
#include <driver/adc.h>
#include "sdkconfig.h"

#include "BleConnectionStatus.h"
#include "BleMouse.h"
#include "BleMouseTypes.h"
#include "SerialMacros.h"

/* Need those variables to send them from CallBack class to BleMouse */
bool tempStartCalibratingXSensor = false;
bool tempStartCalibratingYSensor = false;
bool tempStartCalibratingLeftClickSensor = false;
bool tempStartCalibratingRightClickSensor = false;

NimBLEService *pReadWriteService;
NimBLECharacteristic *pReadWriteCharacteristic;

/**
 * @brief Constructor for BleMouse class
 */
BleMouse::BleMouse(const std::string & deviceName, const std::string & deviceManufacturer, const uint8_t & batteryLevel) : _buttons(0),
hid(0),
inputMouse(nullptr),
deviceName(deviceName),
deviceManufacturer(deviceManufacturer),
batteryLevel(batteryLevel),
connectionStatus(new BleConnectionStatus())
{
}

  BleMouse::BleMouse(const BleMouse & ble)
  {
    _buttons=ble._buttons;
    connectionStatus = new BleConnectionStatus(*ble.connectionStatus);
    hid=ble.hid;
    inputMouse=ble.inputMouse;
    startCalibratingXSensor=ble.startCalibratingXSensor;
    startCalibratingYSensor=ble.startCalibratingYSensor;
    startCalibratingLeftClickSensor=ble.startCalibratingLeftClickSensor;
    startCalibratingRightClickSensor=ble.startCalibratingRightClickSensor;
    batteryLevel = ble.batteryLevel;
    deviceManufacturer = ble.deviceManufacturer;
    deviceName = ble.deviceName;
  }

  BleMouse & BleMouse::operator =(const BleMouse & ble)
  { 
    if(&ble != this) {
      _buttons=ble._buttons;
      BleConnectionStatus * _connectionStatus = new BleConnectionStatus(*ble.connectionStatus);
      delete connectionStatus;
      connectionStatus = _connectionStatus;
      hid=ble.hid;
      inputMouse=ble.inputMouse;
      startCalibratingXSensor=ble.startCalibratingXSensor;
      startCalibratingYSensor=ble.startCalibratingYSensor;
      startCalibratingLeftClickSensor=ble.startCalibratingLeftClickSensor;
      startCalibratingRightClickSensor=ble.startCalibratingRightClickSensor;
      batteryLevel = ble.batteryLevel;
      deviceManufacturer = ble.deviceManufacturer;
      deviceName = ble.deviceName;
    }
    return *this;
  }

/**
 * @brief Begin method for BleMouse
 */
void BleMouse::begin(void)
{
  xTaskCreate(this->taskServer, "server", 20000, reinterpret_cast<void *>(this), 5, nullptr);
}

/**
 * @brief End method for BleMouse
 */
void BleMouse::end(void)
{
}

/**
 * @brief Click method for BleMouse
 *
 * @param b Button value
 */
void BleMouse::click(uint8_t b)
{
  _buttons = b;
  move(0, 0, 0, 0);
  _buttons = 0;
  move(0, 0, 0, 0);
  delay(500); // Avoid unwanted second click
}

/**
 * @brief Move method for BleMouse
 *
 * @param x X position
 * @param y Y position
 * @param wheel Wheel value
 * @param hWheel Horizontal wheel value
 */
void BleMouse::move(signed char x, signed char y, signed char wheel, signed char hWheel)
{
  if (this->isConnected())
  {
    uint8_t m[5];
    m[0] = _buttons;
    m[1] = x;
    m[2] = y;
    m[3] = wheel;
    m[4] = hWheel;
    this->inputMouse->setValue(m, 5);
    this->inputMouse->notify();
  }
}

/**
 * @brief Move the mouse in a specific direction
 *
 * @param direction Direction to move
 * @param pixelX Pixels to move in X direction
 * @param pixelY Pixels to move in Y direction
 */
void BleMouse::moveMouse(uint8_t direction, uint8_t pixelX, uint8_t pixelY)
{
  int directionX = MOUSE_MOVEMENTS_TABLE[direction].dX * pixelX;
  int directionY = MOUSE_MOVEMENTS_TABLE[direction].dY * pixelY;

  this->move(directionX, directionY);
}

/**
 * @brief Set the buttons for the mouse
 *
 * @param b Button value
 */
void BleMouse::buttons(uint8_t b)
{
  if (b != _buttons)
  {
    _buttons = b;
    move(0, 0, 0, 0);
  }
}

/**
 * @brief Press a specific button
 *
 * @param b Button value
 */
void BleMouse::press(uint8_t b)
{
  buttons(_buttons | b);
}

/**
 * @brief Release a specific button
 *
 * @param b Button value
 */
void BleMouse::release(uint8_t b)
{
  buttons(_buttons & ~b);
}

/**
 * @brief Check if a specific button is pressed
 *
 * @param b Button value
 * @return true if button is pressed, false otherwise
 */
bool BleMouse::isPressed(uint8_t b)
{
  if ((b & _buttons) > 0)
    return true;
  return false;
}

/**
 * @brief Check if the mouse is connected
 *
 * @return true if connected, false otherwise
 */
bool BleMouse::isConnected(void)
{
  return this->connectionStatus->connected;
}

/**
 * @brief Set the battery level for the mouse
 *
 * @param level Battery level
 */
void BleMouse::setBatteryLevel(uint8_t level)
{
  this->batteryLevel = level;
  if (hid != 0)
    this->hid->setBatteryLevel(this->batteryLevel);
}

/**
 * @brief Custom callback class to handle data reception.
 * @details This class inherits from NimBLECharacteristicCallbacks and provides a callback mechanism
 *          for handling the reception of data on a characteristic.
 *
 */
class MyCustomCallbacks : public NimBLECharacteristicCallbacks
{
  /**
   * @brief Callback function triggered when data is written to the characteristic.
   * @details This function is invoked whenever data is written to the characteristic
   *          associated with this callback.
   *
   * @param pCharacteristic Pointer to the NimBLECharacteristic object representing
   *                        the characteristic where data was written.
   *
   */
  void onWrite(NimBLECharacteristic *pCharacteristic) override
  {
    // Extract ID and VALUE
    std::string value = pCharacteristic->getValue();
    int id;
    sscanf(value.c_str(), "[%d]", &id);

    // Process ID
    switch (id)
    {
    case 0:
      tempStartCalibratingXSensor = true;
      SERIAL_PRINTLN("ACK Start Calibrating Left Sensor");
      break;

    case 1:
      tempStartCalibratingYSensor = true;
      SERIAL_PRINTLN("ACK Start Calibrating Right Sensor");
      break;

    case 2:
      tempStartCalibratingLeftClickSensor = true;
      SERIAL_PRINTLN("ACK Start Calibrating Up Sensor");
      break;

    case 3:
      tempStartCalibratingRightClickSensor = true;
      SERIAL_PRINTLN("ACK Start Calibrating Down Sensor");
      break;

    default:
      SERIAL_PRINTLN("ID non reconnu");
      break;
    }
  }
};

/**
 * @brief Task server method for handling BLE operations
 *
 * @param pvParameter Pointer to parameter
 */
void BleMouse::taskServer(void *pvParameter)
{
  BleMouse *bleMouseInstance = reinterpret_cast<BleMouse *>(pvParameter); // static_cast<BleMouse *>(pvParameter);
  NimBLEDevice::init(bleMouseInstance->deviceName);

  NimBLEServer *pServer = NimBLEDevice::createServer();
  pServer->setCallbacks(bleMouseInstance->connectionStatus);

  bleMouseInstance->hid = new NimBLEHIDDevice(pServer);
  bleMouseInstance->inputMouse = bleMouseInstance->hid->inputReport(0); // <-- input REPORTID from report map
  bleMouseInstance->connectionStatus->inputMouse = bleMouseInstance->inputMouse;

  bleMouseInstance->hid->manufacturer()->setValue(bleMouseInstance->deviceManufacturer);

  bleMouseInstance->hid->pnp(0x02, 0xe502, 0xa111, 0x0210);
  bleMouseInstance->hid->hidInfo(0x00, 0x02);

  BLESecurity *pSecurity = new NimBLESecurity();

  pSecurity->setAuthenticationMode(ESP_LE_AUTH_BOND);

  bleMouseInstance->hid->reportMap(const_cast<uint8_t*>(_hidReportDescriptor), sizeof(_hidReportDescriptor));
  bleMouseInstance->hid->startServices();

  bleMouseInstance->onStarted(pServer);

  NimBLEAdvertising *pAdvertising = pServer->getAdvertising();
  pAdvertising->setAppearance(HID_MOUSE);
  pAdvertising->addServiceUUID(bleMouseInstance->hid->hidService()->getUUID());

  /* Create a service to receive data from PC */
  NimBLEUUID receivingDataServiceUUID("401c6000-297a-4909-a9c8-22b8e99bd34c");
  NimBLEUUID receivingDataCharacteristicUUID("a60e1f9c-dc21-4be2-858a-e8e2321cce7a");
  pReadWriteService = pServer->createService(receivingDataServiceUUID);
  pReadWriteCharacteristic = pReadWriteService->createCharacteristic(
      receivingDataCharacteristicUUID,
      NIMBLE_PROPERTY::WRITE | NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::NOTIFY);

  MyCustomCallbacks *pCustomCallbacks = new MyCustomCallbacks();
  pReadWriteCharacteristic->setCallbacks(pCustomCallbacks);
  pReadWriteService->start();
  pAdvertising->addServiceUUID(receivingDataServiceUUID);

  pAdvertising->start();
  bleMouseInstance->hid->setBatteryLevel(bleMouseInstance->batteryLevel);

  vTaskDelay(portMAX_DELAY);
}

/**
 * @brief Send data to PC
 *
 * @param data data to send
 */
void BleMouse::sendDataToPC(uint8_t data)
{
  pReadWriteCharacteristic->setValue(data);
  pReadWriteCharacteristic->notify();
}

/**
 * @brief Get the calibration maximum left distance.
 *
 * @return The maximum left distance calibration value.
 */
bool BleMouse::getStartCalibrationXSensor()
{
  this->startCalibratingXSensor = tempStartCalibratingXSensor;
  return this->startCalibratingXSensor;
}

/**
 * @brief Get the calibration maximum right distance.
 *
 * @return The maximum right distance calibration value.
 */
bool BleMouse::getStartCalibrationYSensor()
{
  this->startCalibratingYSensor = tempStartCalibratingYSensor;
  return this->startCalibratingYSensor;
}

/**
 * @brief Get the calibration maximum up distance.
 *
 * @return The maximum up distance calibration value.
 */
bool BleMouse::getStartCalibrationLeftClickSensor()
{
  this->startCalibratingLeftClickSensor = tempStartCalibratingLeftClickSensor;
  return this->startCalibratingLeftClickSensor;
}

/**
 * @brief Get the calibration maximum down distance.
 *
 * @return The maximum down distance calibration value.
 */
bool BleMouse::getStartCalibrationRightClickSensor()
{
  this->startCalibratingRightClickSensor = tempStartCalibratingRightClickSensor;
  return this->startCalibratingRightClickSensor;
}