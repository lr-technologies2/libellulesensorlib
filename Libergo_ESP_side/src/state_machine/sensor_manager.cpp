/**
 * @file sensor_manager.cpp
 * @author Julien Solbach @ LR Technologies
 * @brief Implements the SensorManager state machine, its states and events
 * @version 0.1
 * @date 2023-09-05
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "general_header.hpp"
#include "state_machine/sensor_manager.hpp"
#include "devices/device.h"
#include "SerialMacros.h"

#define ALONE_TIMEOUT_TICKS pdMS_TO_TICKS(10000) /*!< Timer for no message from PC */
#define SLEEP_TIMEOUT_TICKS pdMS_TO_TICKS(30000) /*!< Timer for sleep duration */

/**
 * @brief Callback function call by the sleep timer.
 * (A static function is necessary for a timer callback in FreeRTOS)
 * @param handle pointer to the stm
 */
static void sleepTimeout(TimerHandle_t handle) {
    auto stm = static_cast<SensorManager*>(pvTimerGetTimerID(handle));

    if (stm == nullptr) return;

    stm->notify(SensorManager_ev::SLEEP_TIMEOUT);
}

/**
 * @brief Callback function called by the alone timer.
 *
 * @param handle pointer to the stm
 */
static void aloneTimeout(TimerHandle_t handle) {
    auto stm = static_cast<SensorManager*>(pvTimerGetTimerID(handle));

    if (stm == nullptr) return;

    stm->notify(SensorManager_ev::ALONE_TIMEOUT);
}

/** @class Sleep
 * @brief Implementation of Sleep state for SensorManager state machine
 * 
 */
class Sleep : public IState<SensorManager_ev, SensorManager_ctx> {
    public :
        /** Construct a new Sleep object (state for SensorManager STM) */
        Sleep() : IState<SensorManager_ev, SensorManager_ctx>("Sleep", "start sleepTimer", "stop sleepTimer") {}

        /** method to execute when entering the state */
        virtual void onEntry(const SensorManager_ctx& ctx) override {
            SERIAL_PRINTLN(" ------ SLEEP ------ ");
            if (ctx.sleepTimer == NULL) return;

            SERIAL_PRINTLN("  RESET SLEEP TIMER  ");
            xTimerStart(ctx.sleepTimer, 0);
        }

        /** method to execute in the state */
        virtual void running(const SensorManager_ctx& ctx) override {
            if (ctx.msgQueue == NULL) return;

            if (uxQueueMessagesWaiting(ctx.msgQueue) == 0) return;

            ctx.stm->notify(SensorManager_ev::MSG_RECEIVED);
        }

        /** method to execute when leaving the state */
        virtual void onExit(const SensorManager_ctx& ctx) override {
            xTimerStop(ctx.sleepTimer, 0);
        }
};

/** @class Parse
 * @brief Implementation of Parse state for SensorManager state machine
 *
 */
class Parse : public IState<SensorManager_ev, SensorManager_ctx> {
    public :
        /** Construct a new Parse object (state for SensorManager STM) */
        Parse() : IState<SensorManager_ev, SensorManager_ctx>("Parse") {}

        /** method to execute when entering the state */
        virtual void onEntry(const SensorManager_ctx& ctx) override {
            SERIAL_PRINTLN(" ------ PARSE ------ ");
            if (ctx.aloneTimer == NULL) return;

            SERIAL_PRINTLN("  RESET ALONE TIMER  ");
            xTimerStart(ctx.aloneTimer, 0);
        }

        /** method to execute in the state */
        virtual void running(const SensorManager_ctx& ctx) override {
            struct AMessage * pMsg;
            if (xQueueReceive(ctx.msgQueue, &pMsg, (TickType_t) 1) == pdFAIL) return;

            //TODO PARSING
            SERIAL_PRINTLN("Message parsed !");
            xTimerReset(ctx.aloneTimer, ALONE_TIMEOUT_TICKS);

            //TODO update ID
            if (pMsg->id >= 0x7) {
                ctx.stm->notify(SensorManager_ev::START_MEASURE);
            }
        }

        /** method to execute when leaving the state */
        virtual void onExit(const SensorManager_ctx& ctx) override {
            if (ctx.aloneTimer == NULL) return;

            xTimerStop(ctx.aloneTimer, 0);
        }
};

/** @class Measure
 * @brief Implementation of Measure state for SensorManager state machine
 *
 */
class Measure : public IState<SensorManager_ev, SensorManager_ctx> {
    public :
        /** Construct a new Measure object (state for SensorManager STM) */
        Measure() : IState<SensorManager_ev, SensorManager_ctx>("Measure") {}

        /** method to execute when entering the state */
        virtual void onEntry(const SensorManager_ctx& ctx) override {
            SERIAL_PRINTLN(" ----- MEASURE ----- ");
        }

        /** method to execute in the state */
        virtual void running(const SensorManager_ctx& ctx) override {
            if (ctx.appDevice->scan() && ctx.appDevice->evaluate()) {
                std::string data = ctx.appDevice->send();

                // TODO, sending to the communication STM
                SERIAL_PRINTLN(data.c_str());
            }

            // Checking if a message entered the queue while being in measure mode.
            if (ctx.msgQueue == NULL) return;

            if (uxQueueMessagesWaiting(ctx.msgQueue) == 0) return;

            ctx.stm->notify(SensorManager_ev::MSG_RECEIVED);
        }

        /** method to execute when leaving the state */
        virtual void onExit(const SensorManager_ctx& ctx) override {
        }
};

/** @class Calibration
 * @brief Implementation of Calibration state for SensorManager state machine
 *
 */
class Calibration : public IState<SensorManager_ev, SensorManager_ctx> {
    public :
        /** Construct a new Calibration object (state for SensorManager STM) */
        Calibration() : IState<SensorManager_ev, SensorManager_ctx>("Calibration") {}

        /** method to execute when entering the state */
        virtual void onEntry(const SensorManager_ctx& ctx) override {SERIAL_PRINTLN(" --- CALIBRATION --- ");
        }

        /** method to execute in the state */
        virtual void running(const SensorManager_ctx& ctx) override {}

        /** method to execute when leaving the state */
        virtual void onExit(const SensorManager_ctx& ctx) override {}
};

/** @class ErrorManagement
 * @brief Implementation of ErrorManagement state for SensorManager state machine
 *
 */
class ErrorManagement : public IState<SensorManager_ev, SensorManager_ctx> {
    public :
        /** Construct a new ErrorManagement object (state for SensorManager STM) */
        ErrorManagement() : IState<SensorManager_ev, SensorManager_ctx>("ErrorManagement") {}

        /** method to execute when entering the state */
        virtual void onEntry(const SensorManager_ctx& ctx) override {SERIAL_PRINTLN(" -- ERROR_MANAGEMENT -- ");
        }

        /** method to execute in the state */
        virtual void running(const SensorManager_ctx& ctx) override {}

        /** method to execute when leaving the state */
        virtual void onExit(const SensorManager_ctx& ctx) override {}
};

/** @class PowerOff
 * @brief Implementation of PowerOff state for SensorManager state machine
 *
 */
class PowerOff : public IState<SensorManager_ev, SensorManager_ctx> {
    public :
        /** Construct a new PowerOff object (state for SensorManager STM) */
        PowerOff() : IState<SensorManager_ev, SensorManager_ctx>("PowerOff") {}

        /** method to execute when entering the state */
        virtual void onEntry(const SensorManager_ctx& ctx) override { SERIAL_PRINTLN(" ---- POWER_OFF ---- ");
        }

        /** method to execute in the state */
        virtual void running(const SensorManager_ctx& ctx) override {}

        /** method to execute when leaving the state */
        virtual void onExit(const SensorManager_ctx &ctx) override {}
};

/**
 * @brief Construct a new Sensor Manager:: Sensor Manager object
 * 
 */
SensorManager::SensorManager() {
    // Setting up STM internals
    std::shared_ptr<IState<SensorManager_ev, SensorManager_ctx>> sleep = std::make_shared<Sleep>();
    std::shared_ptr<IState<SensorManager_ev, SensorManager_ctx>> parse = std::make_shared<Parse>();
    std::shared_ptr<IState<SensorManager_ev, SensorManager_ctx>> measure = std::make_shared<Measure>();
    std::shared_ptr<IState<SensorManager_ev, SensorManager_ctx>> calibration = std::make_shared<Calibration>();
    std::shared_ptr<IState<SensorManager_ev, SensorManager_ctx>> errorManagement = std::make_shared<ErrorManagement>();
    std::shared_ptr<IState<SensorManager_ev, SensorManager_ctx>> powerOff = std::make_shared<PowerOff>();

    sleep->addTransition(std::make_shared<Transition<SensorManager_ev, SensorManager_ctx>>(parse, SensorManager_ev::MSG_RECEIVED, "MESSAGE RECEIVED"));
    sleep->addTransition(std::make_shared<Transition<SensorManager_ev, SensorManager_ctx>>(powerOff, SensorManager_ev::SLEEP_TIMEOUT, "SLEEP_TIMEOUT"));

    parse->addTransition(std::make_shared<Transition<SensorManager_ev, SensorManager_ctx>>(sleep, SensorManager_ev::ALONE_TIMEOUT, "ALONE_TIMEOUT"));
    parse->addTransition(std::make_shared<Transition<SensorManager_ev, SensorManager_ctx>>(measure, SensorManager_ev::START_MEASURE, "START_MEASURE"));
    parse->addTransition(std::make_shared<Transition<SensorManager_ev, SensorManager_ctx>>(calibration, SensorManager_ev::UPDATE_CALIBRATION, "UPDATE_CALIBRATION"));
    parse->addTransition(std::make_shared<Transition<SensorManager_ev, SensorManager_ctx>>(errorManagement, SensorManager_ev::QUEUE_ERROR, "QUEUE_ERROR"));

    measure->addTransition(std::make_shared<Transition<SensorManager_ev, SensorManager_ctx>>(parse, SensorManager_ev::MSG_RECEIVED, "MSG_RECEIVED"));

    calibration->addTransition(std::make_shared<Transition<SensorManager_ev, SensorManager_ctx>>(sleep, SensorManager_ev::SENSORS_UPDATED, "SENSORS_UPDATED"));

    errorManagement->addTransition(std::make_shared<Transition<SensorManager_ev, SensorManager_ctx>>(parse, SensorManager_ev::ERROR_HANDLED, "ERROR_HANDLED"));
    errorManagement->addTransition(std::make_shared<Transition<SensorManager_ev, SensorManager_ctx>>(powerOff, SensorManager_ev::FATAL_ERROR, "FATAL_ERROR"));

    addState(sleep, true);
    addState(parse);
    addState(measure);
    addState(calibration);
    addState(errorManagement);
    addState(powerOff);

    // Setting up Context
    mContext.sleepTimer = xTimerCreate("SleepTimer", SLEEP_TIMEOUT_TICKS, pdFALSE, static_cast<void*>(this), sleepTimeout); // Static functions are mandatory for freeRTOS callbacks.
    mContext.aloneTimer = xTimerCreate("AloneTimer", ALONE_TIMEOUT_TICKS , pdFALSE, static_cast<void*>(this), aloneTimeout); // Same
    mContext.msgQueue = xQueueCreate(50, sizeof(struct AMessage*));
    mContext.stm = this;
}

/**
 * @brief Post a message to the queue stored in context
 *
 */
void SensorManager::postMessage(AMessage* msg) {
    if (mContext.msgQueue == NULL) return;

    xQueueSend(mContext.msgQueue, (void *) &msg, (TickType_t) 0);
}
