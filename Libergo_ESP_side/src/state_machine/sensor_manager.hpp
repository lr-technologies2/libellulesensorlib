/**
 * @file sensor_manager.hpp
 * @author Julien Solbach @ LR Technologies
 * @brief Define the Sensor Manager state machine
 * @version 0.1
 * @date 2023-09-05
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef SENSOR_MANAGER_H
#define SENSOR_MANAGER_H

#include <memory>

#include <Arduino.h>

#include "state_machine/StateMachine.hpp"

class Device;

/**
 * @brief Place holder for the Messages used to communicate with the PC.
 * @todo Change to the real msg struct
 */
struct AMessage {
    unsigned int id; /*!< Message ID */
    unsigned char data[20]; /*!< Message content */
};

/** 
 * @brief Events used by the SensorManager State Machine.
 *
 */
enum class SensorManager_ev
{
    MSG_RECEIVED,  /*!< Message received event */
    START_MEASURE, /*!< Start measure event */

    UPDATE_CALIBRATION, /*!< Update calibration event */
    SENSORS_UPDATED,    /*!< Sensors are updated event */

    SLEEP_TIMEOUT, /*!< Sleep timeout event */
    ALONE_TIMEOUT, /*!< Alone timeout event */

    QUEUE_ERROR,   /*!< Queue error event */
    ERROR_HANDLED, /*!< Error handled event */
    FATAL_ERROR,   /*!< Fatal error event */
};

/**
 * @brief struct used to provide the STM context to its states.
 * 
 */
struct SensorManager_ctx {
    IStateMachine<SensorManager_ev, SensorManager_ctx>* stm;  /*!< The state machine itself */
    QueueHandle_t msgQueue;                                   /*!< The message queue to communicate with the other STM */
    TimerHandle_t sleepTimer;                                 /*!< A timer for sleeping timeout */
    TimerHandle_t aloneTimer;                                 /*!< A timer for alone time timeout */
    std::shared_ptr<Device> appDevice;                                        /*!< The current device */
};

/** @class SensorManager
 * @brief Implementation of the SensorManager state machine
 *
 */
class SensorManager : public IStateMachine<SensorManager_ev, SensorManager_ctx> {
    public :
        SensorManager();

        void postMessage(AMessage* msg);

        /**
         * @brief Set the App Device object
         * 
         * @param dev pointer to the current device
         */
        void setAppDevice(std::shared_ptr<Device> dev) { mContext.appDevice = dev;}
};

#endif