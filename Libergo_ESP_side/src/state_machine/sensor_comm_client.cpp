/**
 * @file sensor_comm_client.cpp
 * @author Julien Solbach @ LR Technologies
 * @brief Implements the SensorCommClient state machine, its states and events
 * @version 0.1
 * @date 2023-09-05
 *
 * @copyright Copyright (c) 2023
 *
 */
#include <memory>

#include "SerialMacros.h"
#include "state_machine/sensor_comm_client.hpp"
//#include "BluetoothSerial.h"

#define PAIRING_TIMEOUT_TICKS pdMS_TO_TICKS(10000) /*!< Timer for pairing with bluetooth*/

/* Bluetooth Serial port object */

/**
 * @brief Callback function call by the pairing timer.
 *
 * @param handle pointer to the stm
 */
static void pairingTimeout(TimerHandle_t handle) {
    SERIAL_PRINTLN("PairingTimer done!");
    auto stm = static_cast<SensorCommClient*>(pvTimerGetTimerID(handle));

    if (stm == nullptr) return;

    stm->notify(SensorCommClient_ev::PAIRING_TIMEOUT);
}


// Process the incoming data
/**
 * @brief Process received message
 * 
 * @warning Not Implemented Yet
 * 
 * @param ctx current context
 */
void processData(const SensorCommClient_ctx& ctx){
    // TODO implement processData

} 

/** @class Pairing
 * @brief Implementation of Pairing state for SensorCommClient state machine
 *
 */
class Pairing : public IState<SensorCommClient_ev, SensorCommClient_ctx> {
    public :
        /** Construct a new Pairing object (state for SensorCommClient STM) */
        Pairing() : IState<SensorCommClient_ev, SensorCommClient_ctx>("Pairing", "start pairingTimer", "stop pairingTimer") {}

        virtual void onEntry(const SensorCommClient_ctx& ctx) override {
            SERIAL_PRINTLN(" ----- PAIRING ----- ");
            // Check if the pairing timer exists in the context
            if (ctx.pairingTimer != NULL) {
                // Start the pairing timer
                xTimerStart(ctx.pairingTimer, 0);
            }else {
                SERIAL_PRINTLN("Pairing timer is not available in the context.");
            }
        }

        virtual void running(const SensorCommClient_ctx& ctx) override {
            SERIAL_PRINTLN(" ----- RUNNING (PAIRING) ----- ");
            // Check if pairing is successful or any other condition
        }

        virtual void onExit(const SensorCommClient_ctx& ctx) override {
            SERIAL_PRINTLN("Exit Pairing state.");
            // Stop the pairing timer if it's running
            if (ctx.pairingTimer != NULL) {
                xTimerStop(ctx.pairingTimer, 0);
            }
        }
};

/** @class Listening
 * @brief Implementation of Listening state for SensorCommClient state machine
 *
 */
class Listening : public IState<SensorCommClient_ev, SensorCommClient_ctx>
{
    public:
        /** Construct a new Listening object (state for SensorCommClient STM) */
        Listening() : IState<SensorCommClient_ev, SensorCommClient_ctx>("Listening") {}

        virtual void onEntry(const SensorCommClient_ctx &ctx) override { 
            SERIAL_PRINTLN(" ---- Listening ---- ");
        }
        virtual void running(const SensorCommClient_ctx &ctx) override {
            SERIAL_PRINTLN("Running Listening state.");
            // Check for incoming data (e.g., from Bluetooth or other communication channels)
        }
        virtual void onExit(const SensorCommClient_ctx &ctx) override {
            SERIAL_PRINTLN("Exit Listening state.");
        }
};

/** @class Parse
 * @brief Implementation of Parse state for SensorCommClient state machine
 *
 */
class Parse : public IState<SensorCommClient_ev, SensorCommClient_ctx> {
    public :
        /** Construct a new Parse object (state for SensorCommClient STM) */
        Parse() : IState<SensorCommClient_ev, SensorCommClient_ctx>("Parse") {}

        virtual void onEntry(const SensorCommClient_ctx& ctx) override {SERIAL_PRINTLN(" ------ PARSE ------ ");
        }
        virtual void running(const SensorCommClient_ctx& ctx) override {}
        virtual void onExit(const SensorCommClient_ctx& ctx) override {}
};

/** @class Store
 * @brief Implementation of Store state for SensorCommClient state machine
 *
 */
class Store : public IState<SensorCommClient_ev, SensorCommClient_ctx> {
    public :
        /** Construct a new Store object (state for SensorCommClient STM) */
        Store() : IState<SensorCommClient_ev, SensorCommClient_ctx>("Store") {}

        virtual void onEntry(const SensorCommClient_ctx& ctx) override {SERIAL_PRINTLN(" ------ STORE ------ ");
        }
        virtual void running(const SensorCommClient_ctx& ctx) override {}
        virtual void onExit(const SensorCommClient_ctx& ctx) override {}
};

/** @class OtherRequest
 * @brief Implementation of OtherRequest state for SensorCommClient state machine
 *
 */
class OtherRequest : public IState<SensorCommClient_ev, SensorCommClient_ctx> {
    public :
        /** Construct a new OtherRequest object (state for SensorCommClient STM) */
        OtherRequest() : IState<SensorCommClient_ev, SensorCommClient_ctx>("OtherRequest") {}

        virtual void onEntry(const SensorCommClient_ctx& ctx) override {SERIAL_PRINTLN(" -- OTHER REQUEST -- ");
        }
        virtual void running(const SensorCommClient_ctx& ctx) override {}
        virtual void onExit(const SensorCommClient_ctx& ctx) override {}
};

/** @class Error
 * @brief Implementation of Error state for SensorCommClient state machine
 *
 */
class Error : public IState<SensorCommClient_ev, SensorCommClient_ctx> {
    public :
        /** Construct a new Error object (state for SensorCommClient STM) */
        Error() : IState<SensorCommClient_ev, SensorCommClient_ctx>("Error", "Store Current Error Code error in section shared with other core") {}

        virtual void onEntry(const SensorCommClient_ctx& ctx) override {SERIAL_PRINTLN(" ------ ERROR ------ ");
        }
        virtual void running(const SensorCommClient_ctx& ctx) override {}
        virtual void onExit(const SensorCommClient_ctx& ctx) override {}
};

/**
 * @brief Construct a new Sensor Comm Client:: Sensor Comm Client object
 * 
 */
SensorCommClient::SensorCommClient() {
    std::shared_ptr<IState<SensorCommClient_ev, SensorCommClient_ctx>> pairing = std::make_shared<Pairing>();
    std::shared_ptr<IState<SensorCommClient_ev, SensorCommClient_ctx>> listening = std::make_shared<Listening>();
    std::shared_ptr<IState<SensorCommClient_ev, SensorCommClient_ctx>> parse = std::make_shared<Parse>();
    std::shared_ptr<IState<SensorCommClient_ev, SensorCommClient_ctx>> store = std::make_shared<Store>();
    std::shared_ptr<IState<SensorCommClient_ev, SensorCommClient_ctx>> error = std::make_shared<Error>();
    std::shared_ptr<IState<SensorCommClient_ev, SensorCommClient_ctx>> otherReq = std::make_shared<OtherRequest>();

    pairing->addTransition(std::make_shared<Transition<SensorCommClient_ev, SensorCommClient_ctx>>(listening, SensorCommClient_ev::PAIRED, "PAIRED"));
    pairing->addTransition(std::make_shared<Transition<SensorCommClient_ev, SensorCommClient_ctx>>(error, SensorCommClient_ev::PAIRING_TIMEOUT, "PAIRING_TIMEOUT"));

    listening->addTransition(std::make_shared<Transition<SensorCommClient_ev, SensorCommClient_ctx>>(pairing, SensorCommClient_ev::CONNECTION_LOST, "CONNECTION_LOST"));
    listening->addTransition(std::make_shared<Transition<SensorCommClient_ev, SensorCommClient_ctx>>(parse, SensorCommClient_ev::MSG_RECEIVED, "MSG_RECEIVED"));

    parse->addTransition(std::make_shared<Transition<SensorCommClient_ev, SensorCommClient_ctx>>(store, SensorCommClient_ev::MSG_OK, "MSG_OK"));
    parse->addTransition(std::make_shared<Transition<SensorCommClient_ev, SensorCommClient_ctx>>(otherReq, SensorCommClient_ev::MSG_IS_REQ, "MSG_IS_REQ"));
    parse->addTransition(std::make_shared<Transition<SensorCommClient_ev, SensorCommClient_ctx>>(listening, SensorCommClient_ev::MSG_NOK, "MSG_NOK"));

    store->addTransition(std::make_shared<Transition<SensorCommClient_ev, SensorCommClient_ctx>>(listening, SensorCommClient_ev::MSG_STORED, "MSG_STORED"));
    store->addTransition(std::make_shared<Transition<SensorCommClient_ev, SensorCommClient_ctx>>(error, SensorCommClient_ev::QUEUE_ERROR, "QUEUE_ERROR"));

    otherReq->addTransition(std::make_shared<Transition<SensorCommClient_ev, SensorCommClient_ctx>>(listening, SensorCommClient_ev::REQUEST_HANDLED, "REQUEST_HANDLED"));

    error->addTransition(std::make_shared<Transition<SensorCommClient_ev, SensorCommClient_ctx>>(listening, SensorCommClient_ev::ERROR_HANDLED, "REQUEST_HANDLED"));

    addState(pairing,true);
    addState(listening);
    addState(parse);
    addState(store);
    addState(error);
    addState(otherReq);

    mContext.msgQueue = xQueueCreate(50, sizeof(struct AMessage*));
    mContext.pairingTimer = xTimerCreate(
        "SleepTimer",               /* Text name, not used by RTOS */
        PAIRING_TIMEOUT_TICKS,      /* Timer period in ticks, must be greater than 0 */
        pdFALSE,                    /* Timer does not autoreload when expires */
        static_cast<void*>(this),   /* Argument to pass to the callback */
        pairingTimeout              /* Callback called when timer expires */
        );
    mContext.stm = this;
}