/**
 * @file sensor_comm_client.hpp
 * @author Julien Solbach @ LR Technologies
 * @brief Define the Sensor Comm Client state machine
 * @version 0.1
 * @date 2023-09-05
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef SENSOR_COMM_CLIENT_H
#define SENSOR_COMM_CLIENT_H

#include <Arduino.h>

#include "state_machine/StateMachine.hpp"
#include "state_machine/sensor_manager.hpp"

class BluetoothSerial;

/**
 * @brief Events used by the SensorManager State Machine.
 *
 */
enum class SensorCommClient_ev
{
    CONNECTION_LOST,    /*!< Lost connection to PC event */
    PAIRED,             /*!< Paired to PC event */
    PAIRING_TIMEOUT,    /*!< Timeout during pairing event */
    MSG_RECEIVED,       /*!< Message received event */
    MSG_NOK,            /*!< Message is not for this board or malformed or an Ack event */
    MSG_OK,             /*!< Message is for this board and not a special request event */
    MSG_IS_REQ,         /*!< Message is a different request evnet */
    REQUEST_HANDLED,    /*!< Request is handled event */
    MSG_STORED,         /*!< Message is stored in the queue event */
    MESSAGE_QUEUE_FULL, /*!< Queue is full error event */
    ERROR_HANDLED,      /*!< Error is handled event */
    QUEUE_ERROR,        /*!< Error from the queue */
    // FATAL_ERROR,        /*!<  */
};

/**
 * @brief struct used to provide the STM context to its states.
 *
 */
struct SensorCommClient_ctx {
    IStateMachine<SensorManager_ev, SensorManager_ctx> *sensorMgr;  /*!< The Sensor Manager STM */
    IStateMachine<SensorCommClient_ev, SensorCommClient_ctx> *stm;  /*!< The Sensor Comm Client STM */
    QueueHandle_t msgQueue;                                         /*!< The message queue */
    TimerHandle_t pairingTimer;                                     /*!< A timer for pairing with PC */
    unsigned int errorCode;                                         /*!< The current error code */
};

/** @class SensorCommClient
 * @brief Implementation of the SensorCommClient state machine
 *
 */
class SensorCommClient : public IStateMachine<SensorCommClient_ev, SensorCommClient_ctx> {
    public :
        SensorCommClient();

        /**
         * @brief Set the Sensor Manager object
         * 
         * @param mgr pointer to the SensorManager STM
         */
        void setSensorManager(SensorManager * mgr) {mContext.sensorMgr = mgr;}
};

#endif