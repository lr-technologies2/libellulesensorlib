/**
 * @file debug.cpp
 * @author Julia Cohen - LR Technologies (jcohen@lrtechnologies.fr)
 * @brief file to handle testing code (temporary?)
 * @version 0.1
 * @date 2023-06-30
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "SerialMacros.h"
#include "Arduino.h"
#include "Wire.h"

/** testI2C
 * @brief Test I2D adresses to know if there is a device
 *
 */
void testI2C()
{
    byte error, address;
    for (address = 1; address < 127; address++)
    {

        Wire.beginTransmission(address);
        error = Wire.endTransmission();

        if (error == 0)
        {
            SERIAL_PRINT("I2C device found at address 0x");
            if (address < 16) {
                SERIAL_PRINT("0");
            }
            SERIAL_PRINT2(address, HEX);
            SERIAL_PRINTLN("  !");
        }
        else if (error == 4)
        {
            SERIAL_PRINT("Unknown error at address 0x");
            if (address < 16) {
                SERIAL_PRINT("0");
            }
            SERIAL_PRINTLN2(address, HEX);
        }
    }
}
