/**
 * @file MessagesManager.cpp
 * @author Lucas Le Loiret, Adrien Dorise @ LR Technologies
 * @brief Implementation of read/write methods for messages
 * @version 0.2
 * @date 2023-11-06
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#include "SerialMacros.h"
#include "communication/MessagesManager.h"
#include "communication/MessagesTypes.h"
#include "communication/MessagesGenerator.h"


/* Json structure to store and decode received messages */
JsonDocument doc_recv;

/** @brief Description : Start connection with PC
   @param void
   @return void */

void initConnection(CommunicationType comm_type){
    SERIAL_PRINTLN("[START]");
    if(comm_type == SERIAL_USB) {
         SERIAL_PRINTLN("[SERIAL USB]");
    }else {
         SERIAL_PRINTLN("[COM TYPE ERROR]");
    }
}


bool is_connection_available(CommunicationType comm_type)
{
    switch(comm_type)
    {
        case USB_HID:
        case SERIAL_USB:
        case BLUETOOTH:
            return Serial;
        default:
            return false;
    }

}


/** @brief Description : Wait for message from PC (json format)
   @param void
   @return void */
String readMessage(CommunicationType comm_type){
    switch(comm_type)
    {
        case CommunicationType::USB_HID:
            if(Serial.available())
            {
                return Serial.readStringUntil('}')+'}'; //As the PC"s message is a json format, the last character has to be "}"
            }
        case CommunicationType::SERIAL_USB:
            if(Serial.available())
            {
                return Serial.readStringUntil('}')+'}'; //As the PC"s message is a json format, the last character has to be "}"
            }
        case CommunicationType::BLUETOOTH:
            if (Serial.available()) 
            {
                SERIAL_PRINTLN("readMessage BLUETOOTH");
                return Serial.readStringUntil('}')+'}'; //As the PC"s message is a json format, the last character has to be "}"
            }
    }
    return "";
}

/** @brief Description : Get the message's type received and call the command needed
   @param String message
   @return void */
int getMessageType(String message){
    //"{\"message_type\":10}"
    DeserializationError error = deserializeJson(doc_recv, message);
    
    if (error) {
       
      SERIAL_PRINT(F("Erreur lors de la désérialisation JSON: "));
      SERIAL_PRINT(error.c_str());
      SERIAL_PRINT(": ");
      SERIAL_PRINTLN(message.c_str());
      /**/
      return -1;
    } 
    
    else {
        int type = doc_recv["message_type"].as<int>();
        switch (type)
        {
        case START_PC:
            readStartPC();
            break;
        case ACK_PC:
            readAck();
            break;
        case READY_PC:
            readReady();
            break;
        case CALIBRATION:
            readCalibration();
            break;
        case DEACTIVATE_SENSOR:
            readDeactivateSensor();
            break;
        case ACTIVATE_SENSOR:
            readActivateSensor();
        case DEVICE_CONFIG:
            readConfiguration();
            break;
        case DEVICE_INFO:
            readDeviceInfo();
            break;
        case NEW_ID:
            readNewID();
            break;
        case IS_ESP_ALIVE:
            readIsAliveESP();
            break;
        case REQUEST_CONF:
            readRequestConf();
            break;
        default:
            type = -1;
            break;
        }
        return type;
    }
}

std::string getConfiguration(){
    std::string configuration = doc_recv["configuration"].as<std::string>();
    return configuration;
}

/*************Read function. !! TO DO : write action to do depending on the message's type */

/**
 * @brief Action to perform when the received message has type START_PC
 * @todo to implement
 * @warning not implemented yet
 */
void readStartPC(){
}

/**
 * @brief Action to perform when the received message has type ACK_PC
 * @todo to implement
 * @warning not implemented yet
 */
void readAck(){
    SERIAL_PRINTLN("Ack received");
}

/**
 * @brief Action to perform when the received message has type READY_PC
 * @todo to implement
 * @warning not implemented yet
 */
void readReady(){
    //SERIAL_PRINTLN("Ready received");
}

/**
 * @brief Action to perform when the received message has type CALIBRATION
 * @todo to implement
 * @warning not implemented yet
 */
void readCalibration(){
    SERIAL_PRINTLN("Calibration message received");
}

/**
 * @brief Action to perform when the received message has type DEACTIVATE_SENSOR
 * @todo to implement
 * @warning not implemented yet
 */
void readDeactivateSensor(){
    SERIAL_PRINTLN("Deac received");
}

/**
 * @brief Action to perform when the received message has type ACTIVATE_SENSOR
 * @todo to implement
 * @warning not implemented yet
 */
void readActivateSensor(){
    SERIAL_PRINTLN("Act received");
}

void readConfiguration(){
    SERIAL_PRINTLN("Configuration received");
}

void readRequestConf(){
    SERIAL_PRINTLN("Request actual configuration received");
}

/**
 * @brief Action to perform when the received message has type DEVICE_INFO
 * @todo to implement
 * @warning not implemented yet
 */
void readDeviceInfo(){
    SERIAL_PRINTLN("DevInfo received");
}

/**
 * @brief Action to perform when the received message has type NEW_ID
 * @todo to implement
 * @warning not implemented yet
 */
void readNewID(){
    SERIAL_PRINTLN("ID received");
}

/**
 * @brief Action to perform when the received message has type IS_ESP_ALIVE
 * @todo to implement
 * @warning not implemented yet
 */
void readIsAliveESP(){
    //SERIAL_PRINTLN("IS ESP alive received");
}


/*************** Send messages function ************/

/**
 * @brief Send a json-structured message through a Serial Bluetooth connection
 * The other sending methods all call this one
 * 
 * @param message JsonDocument message to send
 */
void sendMessage(JsonDocument message, CommunicationType comm_type){
    switch(comm_type)
    {
        case CommunicationType::USB_HID:
            //nothing because data are send by usb hid
            break;
        case CommunicationType::SERIAL_USB:
            serializeJson(message, Serial);
            SERIAL_PRINTLN("");
            break;
        case CommunicationType::BLUETOOTH:
            //nothing because data are send by bluetooth
            break;
    }
}

/**
 * @brief Send a message to PC with type START_ESP
 * @todo to implement
 * @warning not implemented yet
 */
void sendMessageStart(){
    JsonDocument json_start = messageStart();
    sendMessage(json_start);
}

/**
 * @brief Send a message to PC with type ACK_ESP
 * @todo to implement
 * @warning not implemented yet
 */
void sendMessageAck(){
    JsonDocument json_ack = messageAck();
    sendMessage(json_ack);
}

/**
 * @brief Send a message to PC with type READY_ESP
 * @todo to implement
 * @warning not implemented yet
 */
void sendMessageReady(){
    JsonDocument json_ready = messageReady();
    sendMessage(json_ready);
}

/**
 * @brief Send a message to PC with type END_MSG_ESP
 * @todo to implement
 * @warning not implemented yet
 */
void sendMessageEnd(){
    JsonDocument json_end = messageEnd();
    sendMessage(json_end);
}

/**
 * @brief Send a message to PC with type DATA_SENSOR
 * @todo to implement
 * @warning not implemented yet
 */
void sendMessageDataSensor(const std::string headers[], const int data[], const int size){
    JsonDocument json_data_sensor = messageDataSensor(headers, data, size);
    sendMessage(json_data_sensor);
}

void sendMessageConfiguration(const std::string & data){
    JsonDocument json_data_config = messageConfiguration(data);
    sendMessage(json_data_config);
}

/**
 * @brief Send a message to PC with type OK_MESSAGE
 * @todo to implement
 * @warning not implemented yet
 */
void sendMessageOK(){
    JsonDocument json_ok = messageOK();
    sendMessage(json_ok);
}

/**
 * @brief Send a message to PC with type NO_OK_MESSAGE
 * @todo to implement
 * @warning not implemented yet
 */
void sendMessageNOK(){
    JsonDocument json_nok = messageNOK();
    sendMessage(json_nok);
}

/**
 * @brief Send a message to PC with type IS_PC_ALIVE
 * @todo to implement
 * @warning not implemented yet
 */
void sendMessageIsAlivePC(){
    JsonDocument json_nok = messageIsAlivePC();
    sendMessage(json_nok);
}
