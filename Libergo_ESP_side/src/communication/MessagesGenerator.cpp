/**
 * @file MessagesGenerator.cpp
 * @author Lucas Le Loiret @ LR Technologies
 * @brief Implementation of methods to create messages to send to PC
 * @version 0.1
 * @date 2023-09-18
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "communication/MessagesGenerator.h"
#include "communication/MessagesTypes.h"

/**
 * @brief Creates a message with type START_ESP
 * @warning Content of the message to complete (and this method's arguments)
 *
 * @return JsonDocument
 */
JsonDocument messageStart(){
    JsonDocument doc;
    doc["message_type"] = START_ESP;
    return doc;
};

/**
 * @brief Creates a message with type ACK_ESP
 * @warning Content of the message to complete (and this method's arguments)
 *
 * @return JsonDocument
 */
JsonDocument messageAck(){
    JsonDocument doc;
    doc["message_type"] = ACK_ESP;
    return doc;
};

/**
 * @brief Creates a message with type READY_ESP
 * @warning Content of the message to complete (and this method's arguments)
 *
 * @return JsonDocument
 */
JsonDocument messageReady(){
    JsonDocument doc;
    doc["message_type"] = READY_ESP;
    return doc;
};

/**
 * @brief Creates a message with type END_MSG_ESP
 * @warning Content of the message to complete (and this method's arguments)
 *
 * @return JsonDocument
 */
JsonDocument messageEnd(){
    JsonDocument doc;
    doc["message_type"] = END_MSG_ESP;
    return doc;
};


/**
 * @brief Creates a message with type DATA_SENSOR
 * @warning Content of the message to complete (and this method's arguments)
 *
 * @return JsonDocument
 */
JsonDocument messageDataSensor(const std::string headers[], const int data[], const int size){
    JsonDocument doc;
    doc["message_type"] = DATA_SENSOR;
    for(int i=0; i<size; i++)
    {
        doc[headers[i]] = data[i];
    }
    return doc;
}

JsonDocument messageConfiguration(const std::string & data){
    JsonDocument doc;
    doc["message_type"] = CONFIG_ESP;
    doc["configuration"] = data;
    return doc; 
}

/**
 * @brief Creates a message with type OK_MESSAGE
 * @warning Content of the message to complete (and this method's arguments)
 *
 * @return JsonDocument
 */
JsonDocument messageOK(){
    JsonDocument doc;
    doc["message_type"] = OK_MESSAGE;
    return doc;
};

/**
 * @brief Creates a message with type NO_OK_MESSAGE
 * @warning Content of the message to complete (and this method's arguments)
 *
 * @return JsonDocument
 */
JsonDocument messageNOK(){
    JsonDocument doc;
    doc["message_type"] = NO_OK_MESSAGE;
    return doc;
};

/**
 * @brief Creates a message with type IS_PC_ALIVE
 * @warning Content of the message to complete (and this method's arguments)
 *
 * @return JsonDocument
 */
JsonDocument messageIsAlivePC(){
    JsonDocument doc;
    doc["message_type"] = IS_PC_ALIVE;
    return doc;
};