/**
 * \file main.cpp
 * \brief Start of the program.
 *
 * Start the program and its different tasks.
 * First, the ESP32 is initialized (setup() method).
 * During this initialization, the tasks running on each of the 2 cores are started:
 * SensorCommClientTaskCode and SensorManagerTaskCode.
 */

#ifdef USING_USBHID
  #ifndef ARDUINO_USB_MODE
  #error This ESP32 SoC has no Native USB interface
  #elif ARDUINO_USB_MODE == 1
  #warning This sketch should be used when USB is in OTG mode
  #endif
#endif

#include <memory>

#include "SerialMacros.h"
#include "peripherical/bluetooth.h"
#include "peripherical/usbhid.h"
#include "peripherical/serial.h"
#include "device_selection.h"
#include "devices/device.h"
#include "devices/air_mouse.h"
#include "devices/light_mouse.h"
#include "devices/jerry_mouse.h"
#include "devices/random_device.h"
#include "devices/capacitive_device.h"
#include "ConfigData.h"

/* Device */
std::shared_ptr<Device> app_device; /**< Object handling the Device information */
std::shared_ptr<peripherical::Peripherical> m_peripheric;

std::shared_ptr<ConfigData> m_configData;

/** setup
 * \brief Start of the program and the Arduino task.
 *
 * Initializes: Serial connection, files system, bluetooth, I2C, the connected sensors.
 * Then, configurates and starts the tasks running on each core.
 */
void setup()
{ 
  SERIAL_SPEED_BEGIN(115200);
  m_configData = std::make_shared<ConfigData>(deviceType);

#ifdef USING_BLUETOOTH
  m_peripheric = std::make_shared<peripherical::Bluetooth>(m_configData);
  SERIAL_PRINTLN("Starting BLE work!");
#endif
#ifdef USING_USBHID
  m_peripheric = std::make_shared<peripherical::UsbHid>(m_configData);
  SERIAL_PRINTLN("Starting USB work!");
#endif
#if !USING_BLUETOOTH && !USING_USBHID
  m_peripheric = std::make_shared<peripherical::SerialUSB>();
  SERIAL_PRINTLN("Starting Serial work!");
#endif

#if CONFIG_IDF_TARGET_ESP32S3
  if(Wire.setPins(GPIO_NUM_2, GPIO_NUM_1)){
    SERIAL_PRINTLN("\nI2C set Pins ok");
  } else {
    SERIAL_PRINTLN("\nI2C set Pins failed");
  }
#endif

  /* Init Device */
  switch (deviceType)
  {
  case available_device::JERRY_MOUSE:
    app_device = std::make_shared<JerryMouse>(0, m_peripheric->getComType(), m_configData);
    break;
  case available_device::LIGHT_MOUSE:
    app_device = std::make_shared<LightMouse>(0, m_peripheric->getComType(), m_configData);
    break;
  case available_device::AIR_MOUSE:
    app_device = std::make_shared<AirMouse>(0, m_peripheric->getComType(), m_configData);
    break;
  case available_device::RANDOM_DEVICE:
    app_device = std::make_shared<RandomDevice>(0, m_peripheric->getComType(), m_configData);
    break;
  case available_device::CAPACITIVE_SENSOR:
    app_device = std::make_shared<CapacitiveDevice>(0, m_peripheric->getComType(), m_configData);
    break;
  default:
    SERIAL_PRINTLN("\n\nWARNING: Unknown device! Set a device in <device_selection.h>\n\nThis program is now terminated.");
    return;
  }

  m_peripheric->begin(app_device);
  if(app_device->initSensors()) {
    app_device->printInfo();
  } else {
    SERIAL_PRINTLN("failed to init device"); 
  }
  m_peripheric->start();

}

/**
 * @brief Main method called in a loop after setup.
 *
 */
void loop()
{
  m_peripheric->loop(app_device);
}
