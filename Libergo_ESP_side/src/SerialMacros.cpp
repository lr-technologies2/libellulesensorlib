#include"SerialMacros.h"

SerialMacro::SerialMacro(): m_begin(false), m_speed(115200)
{
#if !USING_USBHID
    m_begin = true;
#endif
}

SerialMacro::~SerialMacro()
{

}

bool SerialMacro::getBegin()
{
    return m_begin;
}

void SerialMacro::setBegin()
{
    m_begin = true;
#if USING_USBHID
  Serial.begin(m_speed);
#endif    
}


void SerialMacro::setSpeed(int speed)
{
    m_speed = speed;
#if !USING_USBHID
  Serial.begin(m_speed);
#endif  
}