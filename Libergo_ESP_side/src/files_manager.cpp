/**
 * @file files_manager.cpp
 * @author Julia Cohen - LR Technologies (jcohen@lrtechnologies.fr)
 * @brief File defining the FilesManager class methods to handle the configuration files stored in the ESP32
 * @version 0.1
 * @date 2023-06-30
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "SerialMacros.h"
#include "files_manager.h"

#include <stdio.h>
#include "SPIFFS.h"
//#include "FS.h" // Arduino file system wrapper

#include "errors.h"
#include <Arduino.h>

/**
 * @brief Construct a new FilesManager::FilesManager object
 *
 */
FilesManager::FilesManager()
{
    // initialization
    if (!SPIFFS.begin(true))
    {
        SERIAL_PRINTLN("An Error has occurred while mounting SPIFFS");
        _errorStatus = SPIFFS_INIT_ERROR;
    }
    else
    {
        _errorStatus = NO_ERROR;
    }
}

/**
 * @brief Initializes the file containing the board ID.
 * 
 * @todo remove the Serial prints from here, use a return error value instead.
 */
void FilesManager::initID(void)
{
    if (_errorStatus == SPIFFS_INIT_ERROR)
    {
        SERIAL_PRINTLN("SPIFFS has a status error. Cannot open file for reading ID");
    }
    else
    {
        bool exists = SPIFFS.exists(ID_FILE_NAME);
        if(exists)
        {
            SERIAL_PRINT("ID file already exists.");
        }
        else
        {
            SERIAL_PRINTLN("Creating file with MAC address ID...");
            uint8_t mac_addr[6];
            if (esp_read_mac(mac_addr, ESP_MAC_BT) == ESP_OK)
            {
                File file = SPIFFS.open(ID_FILE_NAME, FILE_WRITE);
                // file.print(mac_addr[0], HEX);
                // file.print(":");
                // file.print(mac_addr[1], HEX);
                // file.print(":");
                // file.print(mac_addr[2], HEX);
                // file.print(":");
                // file.print(mac_addr[3], HEX);
                // file.print(":");
                // file.print(mac_addr[4], HEX);
                // file.print(":");
                // file.print(mac_addr[5], HEX);
                file.printf("%02X:%02X:%02X:%02X:%02X:%02X", 
                                mac_addr[0], 
                                mac_addr[1], 
                                mac_addr[2], 
                                mac_addr[3], 
                                mac_addr[4], 
                                mac_addr[5]);

                file.close();
            }
            else
            {
                SERIAL_PRINTLN("WARNING: failed to read MAC address. Using default ID.");
                File file = SPIFFS.open(ID_FILE_NAME, FILE_WRITE);
                file.print("Libergo");

                file.close();
            }
        }
    }
}

/** 
 * @brief Read the board ID from local file
 *
 * @return string ID from file
 * @note if an error occurs, _errorStatus is updated and an empty string is returned
 */
std::string FilesManager::readID(void)
{
    String id = "";

    if (_errorStatus == SPIFFS_INIT_ERROR)
    {
        SERIAL_PRINTLN("SPIFFS has a status error. Cannot open file for reading ID");
    }
    else
    {
        File open_file = SPIFFS.open(ID_FILE_NAME, FILE_READ);
        if (!open_file)
        {
            SERIAL_PRINTLN("Failed to open ID file for reading");
            _errorStatus = SPIFFS_OPEN_ERROR;
        }
        if(open_file.available())
        {
            id = open_file.readString();
        }

        open_file.close();
    }

    return std::string(id.c_str(), id.length());
}

/** 
 * @brief Write a new board ID in the correct file
 *
 * @param new_id string corresponding to the new ID to write
 * @note if an error occurs, _errorStatus is updated
 * @todo clear le fichier, puis écrire
 */
void FilesManager::writeID(const std::string & new_id)
{
    File open_file;

    if (_errorStatus == SPIFFS_INIT_ERROR)
    {
        SERIAL_PRINTLN("SPIFFS has a status error. Cannot open file for writing ID");
        return;
    }
    else
    {
        open_file = SPIFFS.open(ID_FILE_NAME, FILE_WRITE);
        if (!open_file)
        {
            SERIAL_PRINTLN("Failed to open ID file for writing");
            _errorStatus = SPIFFS_OPEN_ERROR;
            return;
        }
        if (open_file.print(new_id.c_str()))
        {
            SERIAL_PRINT("New ID written: ");
            SERIAL_PRINTLN(new_id.c_str());
        }
        else
        {
            SERIAL_PRINTLN("Failed to write ID file");
            _errorStatus = SPIFFS_WRITE_ERROR; 
        }

        open_file.close();
    }
}

/** 
 * @brief Read the board Sensors data from local file
 *
 * @return string sensor information from file
 * @note if an error occurs, _errorStatus is updated and an empty string is returned
 */
std::string FilesManager::readSensor(void)
{
    File open_file;
    String sensor_data;

    if (_errorStatus == SPIFFS_INIT_ERROR)
    {
        SERIAL_PRINTLN("SPIFFS has a status error. Cannot open file for reading Sensors data");
    }
    else
    {
        open_file = SPIFFS.open(DEVICE_FILE_NAME, FILE_READ);
        if (!open_file)
        {
            SERIAL_PRINTLN("Failed to open Sensors file for reading");
            _errorStatus = SPIFFS_OPEN_ERROR;
        }
        if(open_file.available())
        {
            sensor_data = open_file.readString();
        }

        open_file.close();
    }
    return std::string(sensor_data.c_str(), sensor_data.length());
}

/** 
 * @brief Write new sensor information in the correct file
 *
 * @param new_sensor_data string corresponding to the new data to write
 * @note if an error occurs, _errorStatus is updated
 */
void FilesManager::writeSensor(const std::string & new_sensor_data)
{
    File open_file;

    if (_errorStatus == SPIFFS_INIT_ERROR)
    {
        SERIAL_PRINTLN("SPIFFS has a status error. Cannot open file for writing Sensor data");
        return;
    }

    open_file = SPIFFS.open(DEVICE_FILE_NAME, FILE_WRITE);
    if (!open_file)
    {
        SERIAL_PRINTLN("Failed to open ID file for writing");
        _errorStatus = SPIFFS_OPEN_ERROR;
        return;
    }

    if (open_file.print(new_sensor_data.c_str()))
    {
        SERIAL_PRINTLN("New Sensor data written:");
        SERIAL_PRINTLN(new_sensor_data.c_str());
    }
    else
    {
        SERIAL_PRINTLN("Failed to write Sensor file");
        _errorStatus = SPIFFS_WRITE_ERROR;
    }

    open_file.close();
}

/** 
 * @brief Get the error status to check if SPIFFS is correctly initialized
 *
 * @return unsigned int error status
 */
unsigned int FilesManager::getErrorStatus()
{
    return _errorStatus;
}