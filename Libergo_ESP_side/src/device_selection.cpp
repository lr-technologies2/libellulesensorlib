#include "device_selection.h"

const std::string getDeviceType(const available_device dType)
{
  switch (dType)
  {
  case JERRY_MOUSE:
    return "JERRY_MOUSE";
  case LIGHT_MOUSE:
    return "LIGHT_MOUSE";
  case AIR_MOUSE:
    return "AIR_MOUSE";
  case JOYSTICK:
    return "JOYSTICK";
  case CAPACITIVE_SENSOR:
    return "CAPACITIVE_SENSOR";
  case RANDOM_DEVICE:
    return "RANDOM_DEVICE";
  }
  return "UNKNOW";
}