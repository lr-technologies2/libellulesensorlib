
#include "peripherical/serial.h"
#include "SerialMacros.h"

/** Message frequency*/
#define SEND_FREQ_MS 2000

/** Flag to generate the UML diargma of the state machine in the Serial console */
#define GENERATE_UML true

namespace peripherical
{
    SerialUSB::SerialUSB(): Peripherical(CommunicationType::SERIAL_USB)
    {}
    SerialUSB::~SerialUSB()
    {}

/**
 * @brief Temporary function for the SEND state of the SensorManager state machine.
 *
 * Prints the message on the Serial console, then returns a success value randomly.
 *
 * @param msg string message to send containing sensors values
 * @return true if sending the message was successful
 * @return false if sending the message was not successful
 */
bool SerialUSB::tempSend(std::string msg)
{
  SERIAL_PRINT("Temporary send() function");
  SERIAL_PRINTLN(msg.c_str());
  uint32_t rand_value = esp_random();
  bool ret_value = ((float)rand_value / (float)UINT32_MAX) > 0.5;

  SERIAL_PRINT("Return : ");
  SERIAL_PRINTLN(ret_value);
  return ret_value;
}

/** SensorCommClientTaskCode
 * @brief Code of SensorCommClient component executed on one ESP32 core
 *
 * @param pvParameters parameter passed from setup() when the task is created by xTaskCreatePinnedToCore
 */
//void SerialUSB::SensorCommClientTaskCode(void *pvParameters)
//{
//  if (GENERATE_UML)
//    SERIAL_PRINTLN(stmCommClient.generateUML().c_str());
//  stmCommClient.start();
//  while (1)
//  {
//    stmCommClient.run();
//    vTaskDelay(500);
//  }
//}

/** SensorManagerTaskCode
 * @brief Code of SensorManager component executed on one ESP32 core
 *
 * @param pvParameters parameter passed from setup() when the task is created by xTaskCreatePinnedToCore
 */
//void SerialUSB::SensorManagerTaskCode(void *pvParameters)
//{
//  if (GENERATE_UML)
//    SERIAL_PRINTLN(stmSensor.generateUML().c_str());
//  stmSensor.start();
//  while (1)
//  {
//    stmSensor.run();
//    vTaskDelay(500);
//  }
//}

//void SerialUSB::stateMachine(std::shared_ptr<Device> app_device, CommunicationType comm_type)
//{
//
//  ConfigFiles.writeSensor(app_device->getConfig());
//  SERIAL_PRINTLN("--> New content of Libergo_Sensor.txt:");
//  SERIAL_PRINTLN(ConfigFiles.readSensor().c_str());
//
//  stmSensor.setAppDevice(app_device);
//  stmCommClient.setSensorManager(&stmSensor);
//
//  /* Configuration of the tasks */
//  // create a binary semaphore for task synchronization
//  // create a task that will be executed in the Task1code() function, with priority 1 and executed on core 0
//  xTaskCreatePinnedToCore(
//      [this](){this->SensorCommClientTaskCode(nullptr);}, /* Task function. */
//      "SensorCommClientTask",   /* name of task. */
//      10000,                    /* Stack size of task */
//      NULL,                     /* parameter of the task */
//      1,                        /* priority of the task */
//      &SensorCommClientTask,    /* Task handle to keep track of created task */
//      0);                       /* pin task to core 0 */
//  delay(500);
//
//  // create a task that will be executed in the Task2code() function, with priority 1 and executed on core 1
//  xTaskCreatePinnedToCore(
//      [this](){this->SensorManagerTaskCode(nullptr);}, /* Task function. */
//      "SensorManagerTask",   /* name of task. */
//      10000,                 /* Stack size of task */
//      NULL,                  /* parameter of the task */
//      1,                     /* priority of the task */
//      &SensorManagerTask,    /* Task handle to keep track of created task */
//      1);                    /* pin task to core 1 */
//}

    void SerialUSB::begin(const std::shared_ptr<Device> app_device)
    {
        SERIAL_BEGIN()
        delay(50);
        ESP_LOGI(TAG, "[APP] Startup..");
        ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
        ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

        /*Init communication*/
        initConnection(m_comm_type);
        delay(50);

        SERIAL_PRINTLN(F("Starting Setup..."));
        SERIAL_PRINT("setup() running on core ");
        SERIAL_PRINTLN(xPortGetCoreID());
        delay(50);

        pinMode(0, OUTPUT);

        /* I2C initialization*/
        SERIAL_PRINT("Initializing I2C... ");
        Wire.begin();
        Wire.setClock(400000);
        SERIAL_PRINTLN("Done!");
    }

    void SerialUSB::loop(const std::shared_ptr<Device> app_device)
    {
        if (!is_connection_available(m_comm_type))
        {
          ESP.restart();
        }

        // We wait for the ready_PC flag so that we ca start measuring and sending asynchronus data
        String message;
        message = readMessage();
        if (message.length() >= 1) // Not empty message
        {
          switch (getMessageType(message))
          {
          case READY_PC:
            app_device->scan(false);
            sendMessageEnd();
            break;
          case RESTART_ESP:
            ESP.restart();
            break;
          case IS_ESP_ALIVE:
            sendMessageAck();
            break;
          }
        }
    }

    void SerialUSB::start()
    {
      // stateMachine();

      delay(50);
      // Serial.begin(115200);

      // Wait for PC Start flag
      bool waiting_for_pc = true;
      while (waiting_for_pc)
      {
        SERIAL_PRINTLN("Waiting for PC connection");
        switch (getMessageType(readMessage()))
        {
        case START_PC:
          waiting_for_pc = false;
          break;
        case RESTART_ESP:
          ESP.restart();
          break;
        case IS_ESP_ALIVE:
          sendMessageAck();
          break;
        }
        delay(10);
      }
      sendMessageStart();
      delay(10);        
    }
}