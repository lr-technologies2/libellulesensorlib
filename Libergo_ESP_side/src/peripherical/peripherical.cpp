#include "peripherical/peripherical.h"
#include "communication/MessagesTypes.h"

namespace peripherical
{
    Peripherical::Peripherical(const CommunicationType comm_type): m_comm_type(comm_type){
    }

    Peripherical::~Peripherical(){}

    void Peripherical::serialEventLoop(const std::shared_ptr<Device> app_device, std::shared_ptr<ConfigData> configData)
    {
        // We wait for the ready_PC flag so that we ca start measuring and sending asynchronus data
        String message;
        message = readMessage(m_comm_type);
        if (message.length() >= 1) // Not empty message
        {
          int messageType = getMessageType(message);
          switch (messageType)
          {
          case READY_PC:
            app_device->scan(false);
            sendMessageEnd();
            break;
          case RESTART_ESP:
            ESP.restart();
            break;
          case IS_ESP_ALIVE:
            sendMessageAck();
            break;
          case REQUEST_CONF:
            {
              std::string data = configData->Save();
              sendMessageConfiguration(data.c_str());
            }
            break;
          case DEVICE_CONFIG:
            std::string configuration = getConfiguration();
            configData->Load(configuration);
            configData->Save();
            break;
          }
        }
    }

    CommunicationType Peripherical::getComType()
    {
        return m_comm_type;
    }
}