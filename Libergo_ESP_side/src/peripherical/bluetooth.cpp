#include "SerialMacros.h"
#include "peripherical/bluetooth.h"

#define DEVICE_NAME "LRT_BLUETOOTH"
#define MANUFACTURER "LR Technologies"

namespace peripherical
{
  Bluetooth::Bluetooth(std::shared_ptr<ConfigData> configData) : Peripherical(CommunicationType::BLUETOOTH), m_configData(configData)
  {
  }

  Bluetooth::~Bluetooth()
  {
  }

  void Bluetooth::begin(const std::shared_ptr<Device> app_device)
  {
    SERIAL_BEGIN();
    /* HID Peripheral name */
    std::string device = "LRT_" + getDeviceType(deviceType);
    SERIAL_PRINTLN(device.c_str());
    m_bleMouse = BleMouse(device, MANUFACTURER, 100);
    m_bleMouse.begin();
  }

  void Bluetooth::loop(const std::shared_ptr<Device> app_device)
  {
    serialEventLoop(app_device, m_configData);
    if (m_bleMouse.isConnected())
    {
      app_device->hidController();
      int sensiblity = m_configData->GetSensiblity();
      float displacementX = float(sensiblity) *float(app_device->getXDistance()) / 100.0f;
      float displacementY = float(sensiblity) *float(app_device->getYDistance()) / 100.0f;
      if (displacementX < 0.0f)
      {
        m_bleMouse.moveMouse(MOVE_RIGHT, std::abs(int(displacementX)), std::abs(int(displacementX)));
        SERIAL_PRINTLN("Sensor MOVE_RIGHT");
      }
      else if (displacementX > 0.0f)
      {
        m_bleMouse.moveMouse(MOVE_LEFT, int(displacementX), int(displacementX));
        SERIAL_PRINTLN("Sensor MOVE_LEFT");
      }

      if (displacementY > 0.0f)
      {
        m_bleMouse.moveMouse(MOVE_UP, int(displacementY), int(displacementY));
        SERIAL_PRINTLN("Sensor MOVE_UP");
      }
      else if (displacementY < 0.0f)
      {
        m_bleMouse.moveMouse(MOVE_DOWN, std::abs(int(displacementY)), std::abs(int(displacementY)));
        SERIAL_PRINTLN("Sensor MOVE_DOWN");
      }

      if (app_device->getClickRightDistance() == 1)
      {
        if(!m_bleMouse.isPressed(MOUSE_RIGHT)) {
          m_bleMouse.press(MOUSE_RIGHT);
          SERIAL_PRINTLN("Sensor Press MOUSE_RIGHT");
        }
      } else if (app_device->getClickRightDistance() == 0 && m_bleMouse.isPressed(MOUSE_RIGHT)){
        m_bleMouse.release(MOUSE_RIGHT);
          SERIAL_PRINTLN("Sensor Release MOUSE_RIGHT");
      }

      if (app_device->getClickLeftDistance() == 1)
      {
        if(!m_bleMouse.isPressed(MOUSE_LEFT)) {
          m_bleMouse.press(MOUSE_LEFT);
          SERIAL_PRINTLN("Sensor Press MOUSE_LEFT");
        }
      } else if (app_device->getClickLeftDistance() == 0 && m_bleMouse.isPressed(MOUSE_LEFT)){
        m_bleMouse.release(MOUSE_LEFT);
          SERIAL_PRINTLN("Sensor Release MOUSE_LEFT");
      }
    }
  }

  void Bluetooth::start()
  {
  }
}