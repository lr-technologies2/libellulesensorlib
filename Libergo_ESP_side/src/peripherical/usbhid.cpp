#include "peripherical/usbhid.h"
#include "SerialMacros.h"

#define MANUFACTURER "LR Technologies"
namespace peripherical
{
  UsbHid::UsbHid(std::shared_ptr<ConfigData> configData) : Peripherical(CommunicationType::BLUETOOTH),
#ifdef USING_USBHID 
  Mouse(),
#endif 
  m_configData(configData),
  m_buttonRightPressed(false),
  m_buttonLeftPressed(false),
  m_isBegin(false)
  {
#ifdef USING_USBHID  // initialize mouse control:
  Mouse.begin();
  if(!m_isBegin){
    std::string device = "LRT_" + getDeviceType(deviceType);
    if(!USB.productName(device.c_str())) {
      SERIAL_PRINTLN("product namme error");
    }
    if(!USB.manufacturerName(MANUFACTURER)) {
      SERIAL_PRINTLN("manufacturer namme error");
    }
    m_isBegin = USB.begin();
    SERIAL_BEGIN()
    if(m_isBegin) {
      SERIAL_PRINTLN("mouse init ISB HID succesfully");
      SERIAL_PRINT("Product: ");
      SERIAL_PRINTLN(device.c_str());
      SERIAL_PRINT("Manufacturer: ");
      SERIAL_PRINTLN(MANUFACTURER);
    }
    else {
      SERIAL_PRINTLN("USB.begin error");
    }
  }
#endif
  }

  UsbHid::~UsbHid()
  {
  }

  void UsbHid::begin(const std::shared_ptr<Device> app_device)
  {
  }

  void UsbHid::loop(const std::shared_ptr<Device> app_device)
  {
    serialEventLoop(app_device, m_configData);
#ifdef USING_USBHID
    if (m_isBegin)
    {
      app_device->hidController();
      int sensiblity = m_configData->GetSensiblity();
      float displacementX = float(sensiblity) *float(app_device->getXDistance()) / 100.0f;
      float displacementY = float(sensiblity) *float(app_device->getYDistance()) / 100.0f;
      uint8_t const report_id = 0; // no ID
      if (displacementX != 0.0f || displacementY != 0.0f)
      {
        Mouse.move(int8_t(displacementX), int8_t(displacementY), 0);
      }

      if (app_device->getClickRightDistance() == 1) {
        // if the mouse is not pressed, press it:
        if (!Mouse.isPressed(MOUSE_RIGHT)) {
          Mouse.press(MOUSE_RIGHT);
          SERIAL_PRINTLN("Sensor Press MOUSE_RIGHT");
        }
      }
      // else the mouse button is not pressed:
      else {
        // if the mouse is pressed, release it:
        if (Mouse.isPressed(MOUSE_RIGHT)) {
          Mouse.release(MOUSE_RIGHT);
            SERIAL_PRINTLN("Sensor Release MOUSE_RIGHT");
        }
      }

      if (app_device->getClickLeftDistance() == 1) {
        // if the mouse is not pressed, press it:
        if (!Mouse.isPressed(MOUSE_LEFT)) {
          Mouse.press(MOUSE_LEFT);
          SERIAL_PRINTLN("Sensor Press MOUSE_LEFT");
        }
      }
      // else the mouse button is not pressed:
      else {
        // if the mouse is pressed, release it:
        if (Mouse.isPressed(MOUSE_LEFT)) {
          Mouse.release(MOUSE_LEFT);
            SERIAL_PRINTLN("Sensor Release MOUSE_LEFT");
        }
      }
    }
#endif
  }

  void UsbHid::start()
  {
#ifdef USING_USBHID

#endif
  }
}

