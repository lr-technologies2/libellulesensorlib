#include <Arduino.h>
#include <unity.h>
#include <string>
#include "ConfigData.h"
#include <memory>

void setUp(void) {
}

void tearDown(void) {
}


void test_test(void) {
    std::string hello = "Hello, ";
    std::string world = "world!";
    std::string test = "Hello, world!";
    TEST_ASSERT_EQUAL_STRING(test.c_str(), (hello + world).c_str());
}

void testConfigData() {


//test avec Jerry mouse
{
ConfigData m_configData(available_device::JERRY_MOUSE), m_configData2(available_device::JERRY_MOUSE);
std::string data;
data="m_name=JERRY_MOUSE\nm_sensibility=11\nJOYSTICK0m_minX= 3\nJOYSTICK0m_maxX= 1095\nJOYSTICK0m_minY= 5\nJOYSTICK0m_maxY= 2095\nJOYSTICK0m_neutralX= 111\nJOYSTICK0m_neutralY= 222\nSENSOR0m_neutral= 15\nSENSOR0m_maxDistance= 100\nSENSOR1m_neutral= 35\nSENSOR1m_maxDistance= 300";

bool tLoad = m_configData.Load(data);
TEST_ASSERT_TRUE(tLoad);

TEST_ASSERT_EQUAL(11, m_configData.GetSensiblity());
TEST_ASSERT_EQUAL(1095, m_configData.GetJoystick(0).m_maxX);
TEST_ASSERT_EQUAL(2095, m_configData.GetJoystick(0).m_maxY);
TEST_ASSERT_EQUAL(3, m_configData.GetJoystick(0).m_minX);
TEST_ASSERT_EQUAL(5, m_configData.GetJoystick(0).m_minY);
TEST_ASSERT_EQUAL("JOYSTICK", m_configData.GetJoystick(0).m_name.c_str());
TEST_ASSERT_EQUAL(111, m_configData.GetJoystick(0).m_neutralX);
TEST_ASSERT_EQUAL(222, m_configData.GetJoystick(0).m_neutralY);

TEST_ASSERT_EQUAL("SENSOR", m_configData.GetSensors(0).m_name.c_str());
TEST_ASSERT_EQUAL(15, m_configData.GetSensors(0).m_neutral);
TEST_ASSERT_EQUAL(100, m_configData.GetSensors(0).m_maxDistance);
TEST_ASSERT_EQUAL("SENSOR", m_configData.GetSensors(1).m_name.c_str());
TEST_ASSERT_EQUAL(35, m_configData.GetSensors(1).m_neutral);
TEST_ASSERT_EQUAL(300, m_configData.GetSensors(1).m_maxDistance);

std::string sData = m_configData.Save();

TEST_ASSERT_EQUAL(data.c_str(), sData.c_str());

tLoad = m_configData.Load(data);
TEST_ASSERT_TRUE(tLoad);

TEST_ASSERT_EQUAL(11, m_configData2.GetSensiblity());
TEST_ASSERT_EQUAL(1095, m_configData2.GetJoystick(0).m_maxX);
TEST_ASSERT_EQUAL(2095, m_configData2.GetJoystick(0).m_maxY);
TEST_ASSERT_EQUAL(3, m_configData2.GetJoystick(0).m_minX);
TEST_ASSERT_EQUAL(5, m_configData2.GetJoystick(0).m_minY);
TEST_ASSERT_EQUAL("JOYSTICK", m_configData2.GetJoystick(0).m_name.c_str());
TEST_ASSERT_EQUAL(111, m_configData2.GetJoystick(0).m_neutralX);
TEST_ASSERT_EQUAL(222, m_configData2.GetJoystick(0).m_neutralY);

TEST_ASSERT_EQUAL("SENSOR", m_configData2.GetSensors(0).m_name.c_str());
TEST_ASSERT_EQUAL(15, m_configData2.GetSensors(0).m_neutral);
TEST_ASSERT_EQUAL(100, m_configData2.GetSensors(0).m_maxDistance);
TEST_ASSERT_EQUAL("SENSOR", m_configData2.GetSensors(1).m_name.c_str());
TEST_ASSERT_EQUAL(35, m_configData2.GetSensors(1).m_neutral);
TEST_ASSERT_EQUAL(300, m_configData2.GetSensors(1).m_maxDistance);
}
//test avec air mouse
{
ConfigData m_configData(available_device::AIR_MOUSE), m_configData2(available_device::AIR_MOUSE);
std::string data;
data="m_name=AIR_MOUSE\nm_sensibility=121\nSENSOR0m_neutral= 125\nSENSOR0m_maxDistance= 234\nSENSOR1m_neutral= 225\nSENSOR1m_maxDistance= 444\nSENSOR2m_neutral= 55\nSENSOR2m_maxDistance= 555\nSENSOR3m_neutral= 66\nSENSOR3m_maxDistance= 666";


bool tLoad = m_configData.Load(data);
TEST_ASSERT_TRUE(tLoad);

TEST_ASSERT_EQUAL(121, m_configData.GetSensiblity());

TEST_ASSERT_EQUAL("SENSOR", m_configData.GetSensors(0).m_name.c_str());
TEST_ASSERT_EQUAL(125, m_configData.GetSensors(0).m_neutral);
TEST_ASSERT_EQUAL(234, m_configData.GetSensors(0).m_maxDistance);
TEST_ASSERT_EQUAL("SENSOR", m_configData.GetSensors(1).m_name.c_str());
TEST_ASSERT_EQUAL(225, m_configData.GetSensors(1).m_neutral);
TEST_ASSERT_EQUAL(444, m_configData.GetSensors(1).m_maxDistance);
TEST_ASSERT_EQUAL("SENSOR", m_configData.GetSensors(2).m_name.c_str());
TEST_ASSERT_EQUAL(55, m_configData.GetSensors(2).m_neutral);
TEST_ASSERT_EQUAL(555, m_configData.GetSensors(2).m_maxDistance);
TEST_ASSERT_EQUAL("SENSOR", m_configData.GetSensors(3).m_name.c_str());
TEST_ASSERT_EQUAL(66, m_configData.GetSensors(3).m_neutral);
TEST_ASSERT_EQUAL(666, m_configData.GetSensors(30).m_maxDistance);

std::string sData = m_configData.Save();

TEST_ASSERT_EQUAL(data.c_str(), sData.c_str());

tLoad = m_configData2.Load(data);
TEST_ASSERT_TRUE(tLoad);

TEST_ASSERT_EQUAL(121, m_configData2.GetSensiblity());

TEST_ASSERT_EQUAL("SENSOR", m_configData2.GetSensors(0).m_name.c_str());
TEST_ASSERT_EQUAL(125, m_configData2.GetSensors(0).m_neutral);
TEST_ASSERT_EQUAL(234, m_configData2.GetSensors(0).m_maxDistance);
TEST_ASSERT_EQUAL("SENSOR", m_configData2.GetSensors(1).m_name.c_str());
TEST_ASSERT_EQUAL(225, m_configData2.GetSensors(1).m_neutral);
TEST_ASSERT_EQUAL(444, m_configData2.GetSensors(1).m_maxDistance);
TEST_ASSERT_EQUAL("SENSOR", m_configData2.GetSensors(2).m_name.c_str());
TEST_ASSERT_EQUAL(55, m_configData2.GetSensors(2).m_neutral);
TEST_ASSERT_EQUAL(555, m_configData2.GetSensors(2).m_maxDistance);
TEST_ASSERT_EQUAL("SENSOR", m_configData2.GetSensors(3).m_name.c_str());
TEST_ASSERT_EQUAL(66, m_configData2.GetSensors(3).m_neutral);
TEST_ASSERT_EQUAL(666, m_configData2.GetSensors(30).m_maxDistance);

}
}


void testGetDeviceType() {
  std::string result = getDeviceType(available_device::AIR_MOUSE);
  TEST_ASSERT_EQUAL("AIR_MOUSE", result.c_str());
  result = getDeviceType(available_device::CAPACITIVE_SENSOR);
  TEST_ASSERT_EQUAL("CAPACITIVE_SENSOR", result.c_str());
  result = getDeviceType(available_device::JERRY_MOUSE);
  TEST_ASSERT_EQUAL("JERRY_MOUSE", result.c_str());
  result = getDeviceType(available_device::JOYSTICK);
  TEST_ASSERT_EQUAL("JOYSTICK", result.c_str());
  result = getDeviceType(available_device::LIGHT_MOUSE);
  TEST_ASSERT_EQUAL("LIGHT_MOUSE", result.c_str());
  result = getDeviceType(available_device::RANDOM_DEVICE);
  TEST_ASSERT_EQUAL("RANDOM_DEVICE", result.c_str());
}

int runUnityTests(void) {
  UNITY_BEGIN();
  RUN_TEST(test_test);
  RUN_TEST(testConfigData);
  RUN_TEST(testGetDeviceType);
  return UNITY_END();
}

// WARNING!!! PLEASE REMOVE UNNECESSARY MAIN IMPLEMENTATIONS //

/**
  * For native dev-platform or for some embedded frameworks
  */
//int main(void) {
//  return runUnityTests();
//}

/**
  * For ESP-IDF framework
  * press Test + click boot 15s during "Building & Uploading...
  * when "If you don't see any output for the first 10 secs, please reset board (press reset button)
  * wait 10s and click on reset
  */
void app_main() {
  runUnityTests();
}