/**
 * @file Transition.hpp
 * @author Julien Solbach @ LR Technologies
 * @brief Definition of transitions for all future state machines
 * @version 0.1
 * @date 2023-09-05
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef TRANSITION_H
#define TRANSITION_H

#include <string>
#include <set>
#include <memory>

template<typename EVT, typename CTX>
class IState;

/**
 * @brief This class creates transitions for your state machines (STMs).
 *
 * @tparam EVT type of the events used to trigger the transitions in the state machine
 * @tparam CTX type of the object/struct used to share a common context between all the states in the state machine.
 */
template <typename EVT, typename CTX>
class Transition
{
public:
    /**
     * @brief Construct a new Transition object
     * 
     * @param target target state
     * @param req requested event
     * @param text test to print on UML diagram
     */
    Transition(std::shared_ptr<IState<EVT, CTX>> target, const EVT & req, const std::string &text = "") : mTargetState(target), mRequestedEvent(req), mText(text) {}

    /**
     * @brief getter to the target state of the transition.
     *
     * @return IState<EVT, CTX>* pointer to target State.
     */
    std::shared_ptr<IState<EVT, CTX>> targetState() { return mTargetState; }

    /**
     * @brief this method tests if the provided event is corresponding to the requested one.
     *
     * This method can be overrided to implement a custom eventTest in case the user wants to test another thing than equality.
     * @warning if you want to use a struct or a class for the event. Please override the equality operator in your class/struct.
     *
     * @param event
     * @return true if the event is equal to the requested event
     * @return false if the event is not equal to the requested event
     */
    virtual bool eventTest(const EVT &event) const { return mRequestedEvent == event;}

    /**
     * @brief Function used to print an UML of the STM. This function returns the text put on the transition link in the diagram.
     *
     * @return std::string
     */
    std::string text(){return mText;}

private:
    std::shared_ptr<IState<EVT, CTX>> mTargetState;  /*!< Target state */
    EVT mRequestedEvent;             /*!< Event */
    std::string mText;               /*!< Name of the transition to print on UML diagram */
};

#endif
