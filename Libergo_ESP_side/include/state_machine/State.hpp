/**
 * @file State.hpp
 * @author Julien Solbach @ LR Technologies
 * @brief Definition of State class for all future state machines
 * @version 0.1
 * @date 2023-09-05
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#ifndef STATE_H
#define STATE_H

#include <string>
#include <set>
#include <memory>

#include "Transition.hpp"

/**
 * @brief Abstract class used to instantiate states of a StateMachine (STM) object
 *
 * @tparam EVT type of the events used to trigger the transitions in the state machine
 * @tparam CTX type of the object/struct used to share a common context between all the states in the state machine.
 */
template <typename EVT, typename CTX>
class IState
{
public:
    /** Construct a IState object */
    IState(const std::string & name, const std::string & entry = "", const std::string & exit = "") : mNameStr(name), mEntryStr(entry), mExitStr(exit) {}
    /** Destroys a IState object */
    virtual ~IState() {
    }

    /**
     * @brief Add a transition to the state
     *
     * @param t pointer to the transition object
     */
    void addTransition(const std::shared_ptr<Transition<EVT, CTX>> t) { mTransitions.insert(t); }

    /**
     * @brief Getter for all the transitions the state possess
     *
     * @return const std::set<Transition<EVT, CTX>*>
     */
    const std::set<std::shared_ptr<Transition<EVT, CTX>>> transitions() { return mTransitions; }

    /**
     * @brief Method called by the STM when a transition to this state occured.
     *
     * @param context provide the context in which the state machine is working.
     */
    virtual void onEntry(const CTX& context) = 0;

    /**
     * @brief Method called periodically when this state is the current state of the STM
     *
     * @param context provide the context in which the state machine is working.
     */
    virtual void running(const CTX& context) = 0;

    /**
     * @brief Method called periodically when this state is the current state of the STM and
     * a transition to another state occured.
     *
     * @param context provide the context in which the state machine is working.
     */
    virtual void onExit(const CTX& context) = 0;

    /**
     * @brief getter of the state name
     *
     * @return std::string
     */
    std::string nameStr() {return mNameStr;}

    /**
     * @brief getter of the string describing the onEntry function
     *
     * @return std::string
     */
    std::string entryStr() {return mEntryStr;}

    /**
     * @brief getter of the string describing the onExit function
     *
     * @return std::string
     */
    std::string exitStr() {return mExitStr;}

private:
    std::set<std::shared_ptr<Transition<EVT, CTX>>> mTransitions;  /*!< Set of transitions */

    //Used by the state machine to create a plantuml diagram of the internals.
    std::string mNameStr;  /*!< State name */
    std::string mEntryStr; /*!< OnEntry method description */
    std::string mExitStr;  /*!< OnExit method description */
};

#endif