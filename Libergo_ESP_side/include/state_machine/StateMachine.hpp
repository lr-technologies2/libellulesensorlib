/**
 * @file StateMachine.hpp
 * @author Julien Solbach @ LR Technologies
 * @brief Definiiton of State Machine generic class
 * @version 0.1
 * @date 2023-09-05
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#ifndef STM_H
#define STM_H

#include <string>
#include <set>
#include <memory>

#include "State.hpp"
#include "Transition.hpp"

/**
 * @brief Asbtract class used to create a Custom State Machine (STM) Object
 *
 * @tparam EVT type of the events used to trigger the transitions in the state machine
 * @tparam CTX type of the object/struct used to share a common context between all the states in the state machine.
 */
template<typename EVT, typename CTX>
class IStateMachine
{
public:
    /** Constructs a IStateMachine object*/
    IStateMachine(): mContext() {}
    /** Destroys a IStateMachine object*/
    virtual ~IStateMachine() {
    }

    /**
     * @brief method periodically called by a task
     *
     */
    virtual void run() {
        if (!mCurrentState) return;
        mCurrentState->running(mContext);
    }

    /**
     * @brief This method should be called to start the STM.
     *
     * If this method is not called, the initial state onEntry method won't be called
     */
    void start() {
        if (!mCurrentState) return;

        mCurrentState->onEntry(mContext);
    }

    /**
     * @brief Add a state to the STM
     *
     * @param s pointer to the state object.
     * @param initialState true if this state is the inital state of the STM
     */
    void addState(std::shared_ptr<IState<EVT, CTX>> s, bool initialState = false) {
        mStates.insert(s);
        if (!initialState) return;

        mCurrentState = s;
    }

    /**
     * @brief Notify the STM of the happening of an event.
     * This will trigger a transition to another state if one of the currentState transitions pass the eventTest.
     *
     * @param event notified event.
     */
    bool notify(EVT event) {
        if (mCurrentState == nullptr) return false;

        for (auto transition : mCurrentState->transitions()) {
            if (transition->eventTest(event)) {
                // A TRANSITION OCCURED !
                mCurrentState->onExit(mContext);
                mCurrentState = transition->targetState();
                mCurrentState->onEntry(mContext);
                return true;
            }
        }
        return false;
    }

    /**
     * @brief function used to generate documentation about the state machine.
     * It will print in the Serial a plantuml diagram of the STM.
     *
     * @return std::string
     */
    std::string generateUML() {

        std::string ret = "#######################################\n########## GENERATING STM UML##########\n#######################################\n";

        for (auto state : mStates) {

            ret.append("State " + state->nameStr() + " { \n");
            if (!state->entryStr().empty()) {
                ret.append(state->nameStr() + ": Entry: " + state->entryStr() + "\n");
            }
            if (!state->exitStr().empty()) {
                ret.append(state->nameStr() + ": Exit: " + state->exitStr() + "\n");
            }
            ret.append("}\n");
        }

        ret.append("[*] --> " + mCurrentState->nameStr() + "\n");

        for (auto state : mStates) {
            for (auto transition : state->transitions()) {
                ret.append(state->nameStr() + " --> " +  transition->targetState()->nameStr());
                if (!transition->text().empty()) {
                    ret.append(": " + transition->text());
                }
                ret.append("\n");
            }
        }
        return ret;
    }

protected:
    std::shared_ptr<IState<EVT, CTX>> mCurrentState;    /*!< Current state */
    CTX mContext;                       /*!< Context*/

private:

    std::set<std::shared_ptr<IState<EVT, CTX>>> mStates; /*!< Set of states*/
};

#endif
