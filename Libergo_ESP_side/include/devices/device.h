/**
 * @file device.h
 * @author Julia Cohen, Adrien Dorise ({jcohen, adorise}@lrtechnologies.fr)
 * @brief Declaration of the Device base class.
 * @version 0.1
 * @date 2023-06-28
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef DEVICE_H
#define DEVICE_H

#include <string>
#include <vector>
#include <memory>
#include "devices/sensors/sensor.h"
#include "communication/MessagesManager.h"
#include "ConfigData.h"

/** @class Device
 * @brief Device base class defining the interface for any device (mostly represented as an ESP32)
 * 
 */
class Device
{
    private:
        

    public:
        Device(const int idInit, const std::string name, const CommunicationType commType, std::shared_ptr<ConfigData> confData);
        virtual ~Device();

        virtual bool initSensors(void);
        virtual void hidController();
        virtual int getXDistance() = 0;
        virtual int getYDistance() = 0;
        virtual int getClickLeftDistance() = 0;
        virtual int getClickRightDistance() = 0;
        void calibrate(void);
        bool scan(bool debug=false);
        bool evaluate(void);
        bool get_ready();
        void reset_sensors(void);
        virtual std::string getConfig(void);  // returns the config of all sensors as string
        void updateConfig(std::string new_config);
        std::string send(void);

        void printInfo(void);

        
    protected:
        int _id; /*!< Device ID @note Always 1 device, should this be removed? */
        CommunicationType _commType;
        std::string _name; /*!< Device name (corresponding to class name) */
        bool _ready; /*!< Flag indicating that there is data to send */
        unsigned int _numSensors; /*!< Number of sensors for this device */
        unsigned int _dimension; /*!< Number of dimensions of the device */
        std::vector<std::shared_ptr<Sensor>> _sensorList; /*!< List of sensors of this device @note needs to be a pointer because device is an abstract class*/
        std::shared_ptr<ConfigData> m_confData;
};


#endif