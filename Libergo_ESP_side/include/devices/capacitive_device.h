/**
 * @file capacitive_device.h
 * @author 
 * @brief Defines the class that instantiates a Capacitivedevice
 * @version 0.1
 * @date 2023-10-25
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef CAPACITIVEDEVICE_H
#define CAPACITIVEDEVICE_H

#include "devices/device.h"
#include "devices/sensors/capacitive_sensor.h"

/** @class CapacitiveDevice
 * @brief Class to instantiate a capacitive device
 * 
 */
class CapacitiveDevice : public Device
{
private:

public:
    CapacitiveDevice(const int idInit, const CommunicationType commType, std::shared_ptr<ConfigData> confData);
    int getXDistance() override {return 0;}
    int getYDistance() override {return 0;}
    int getClickLeftDistance()  override {return 0;}
    int getClickRightDistance()  override {return 0;}
protected:
    /* data */
};

#endif

