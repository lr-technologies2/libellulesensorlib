/**
 * @file light_mouse.h
 * @author Adrien Dorise - LR Technologies (adorise@lrtechnologies.fr)
 * @brief Defines the class that instantiates the light mouse device. 
 * The light mouse is composed of one distance VL53L0X sensor.
 * @version 1.0
 * @date 2023-11-2023
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef LIGHTMOUSE_H
#define LIGHTMOUSE_H

#include <string.h>
#include <vector>
#include "devices/device.h"
#include "devices/sensors/VL53L0X.h"

/** @class LightMouse
 * @brief Class to instantiate the LightMouse sensor
 *
 */
class LightMouse : public Device
{
private:

public:
    LightMouse(const int idInit, const CommunicationType commType, std::shared_ptr<ConfigData> confData);

    void formatData();
    int getXDistance() override {return 0;}
    int getYDistance() override {return 0;}
    int getClickLeftDistance()  override {return 0;}
    int getClickRightDistance()  override {return 0;}
protected:
    /* data */

};

#endif