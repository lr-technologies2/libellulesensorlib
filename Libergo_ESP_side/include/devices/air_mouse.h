/**
 * @file air_mouse.h
 * @author Julia Cohen - LR Technologies (jcohen@lrtechnologies.fr)
 * @brief Defines the class that instantiates the AirMouse device
 * @version 0.1
 * @date 2023-06-23
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef AIRMOUSE_H
#define AIRMOUSE_H

#include <string.h>
#include <vector>
#include "devices/device.h"
#include "devices/sensors/VL53L0X.h"
#include "../src/lib_hid/BleMouse.h"

/** @class AirMouse
 * @brief Class to instantiate the AirMouse sensor
 *
 */
class AirMouse : public Device
{
private:
public:
    AirMouse(const int idInit, const CommunicationType commType, std::shared_ptr<ConfigData> confData);

    void hidController() override;
    int getXDistance() override;
    int getYDistance() override;
    int getClickRightDistance() override;
    int getClickLeftDistance() override;

private:
    int s1_distance;
    int s2_distance;
    int s3_distance;
    int s4_distance;

protected:
    /* data */
};

#endif