/**
 * @file random_sensor.h
 * @author Julia Cohen - LR Technologies (jcohen@lrtechnologies.fr)
 * @brief Defies a sensor that returns random values, for test purposes
 * @version 0.1
 * @date 2023-08-22
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef RANDOMSENSOR_H
#define RANDOMSENSOR_H

#include "devices/sensors/sensor.h"


/** @class RandomSensor
 * @brief Class to instantiate a random values sensor
 * 
 */
class RandomSensor: public Sensor
{
private:
    /* data */

public:
    explicit RandomSensor(const int & idInit);

    void readMeas(const bool debug=false) override;

protected:
    /* data */
};

#endif