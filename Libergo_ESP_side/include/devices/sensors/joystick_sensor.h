/**
 * @file Joystick.h
 * @author Adrien Dorise - LR Technologies (adorise@lrtechnologies.fr)
 * @brief Defines the class that instantiates a Joystick sensor
 * @version 1.0
 * @date 2023-10-31
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef JOYSTICKSENSOR_H
#define JOYSTICKSENSOR_H

#include "devices/sensors/distance_sensor.h"
#include <list>
#include "ConfigData.h"

/** @class JoystickSensor
 * @brief Class to instantiate a single Joystick sensor
 *
 */
class JoystickSensor : public Sensor
{
private:
    /* data */

public:
    JoystickSensor(const int idInit, const int _xpin, const int _ypin);

    void readMeas(const bool debug=false) override;
    void formatData() override;
    void reset();
    std::list<int> getXrawDataBuffer();
    std::list<int> getYrawDataBuffer();


    int m_minX, m_maxX, m_minY, m_maxY, m_neutralX, m_neutralY; // TODO calibration



protected:
    /* data */
    int _xpin, _ypin;
    std::list<int> XrawDataBuffer, YrawDataBuffer;
};

#endif