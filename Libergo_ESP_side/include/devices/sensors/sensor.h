/**
 * @file sensor.h
 * @author Adrien Dorise (adorise@lrtechnologies.fr)
 * @brief Declaration of the Sensor base class.
 * @version 0.1
 * @date 2023-07-26
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef SENSOR_H
#define SENSOR_H

#include <string>
#include <list>

/** @class Sensor
 * @brief Abstract class corresponding to a single sensor
 *
 */
class Sensor
{
private:
    /* data */

public:
    Sensor(const int & idInit, const std::string & name);

    virtual bool initSensor();
    int getId();
    std::string getName();
    void run();
    void reset();
    virtual void calibrate();
    virtual void readMeas(const bool debug = false);
    virtual std::string getConfig();
    virtual void formatData();
    void send();
    std::string send(const std::string & header);
    virtual void printInfo();
    bool isQueueEmpty(); // dataBuffer is empty?
    virtual bool getReady();
    virtual std::list<int> getRawDataBuffer();
    virtual void sendMeas();
    virtual int DefaultMaxDistance();

protected:
    int _id;                /*!< Unique ID of the sensor */
    std::string _name;           /*!< Name of the sensor (name of the class) */
    int _dimension;         /*!< Dimension of the sensor */
    float _nullValue;       /*!< Default value when there is no measure or correct value */
    bool _ready;            /*!< Flag to indicate that there is data to send in dataBuffer */
    std::list<int> dataBuffer; /*!< List of measures or values to send */
                            // TODO : add a milisecond counter and use it to prevent reading / sending too fast ?
    
};

#endif