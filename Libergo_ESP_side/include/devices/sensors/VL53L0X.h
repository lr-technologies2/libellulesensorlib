/**
 * @file VL53L0X.h
 * @author Julia Cohen - LR Technologies (jcohen@lrtechnologies.fr)
 * @brief Defines the class that instantiates a VL53L0XSensor sensor
 * @version 0.1
 * @date 2023-06-23
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef VL53L0XSENSOR_H
#define VL53L0XSENSOR_H

#include "Adafruit_VL53L0X.h"
#include "devices/sensors/distance_sensor.h"
#include <list>

/** @def THRESHOLD_LOW_BAD
 * @brief Distance threshold below which the sensor cannot take a measurement
 */
#define THRESHOLD_LOW_BAD 20

/** @def THRESHOLD_LOW_GOOD
 * @brief Distance threshold below which the sensor measurement is not accurate
 */
#define THRESHOLD_LOW_GOOD 40

/** @def THRESHOLD_HIGH_GOOD
 * @brief Distance threshold above which the sensor measurement is not accurate
 */
#define THRESHOLD_HIGH_GOOD 200

/** @def THRESHOLD_HIGH_BAD
 * @brief Distance threshold above which the sensor cannot take a measurement
 */
#define THRESHOLD_HIGH_BAD 250

/** @class VL53L0XSensor
 * @brief Class to instantiate a single VL53L0X sensor
 *
 */
class VL53L0XSensor : public DistanceSensor, Adafruit_VL53L0X
{
private:
    /* data */

public:
    VL53L0XSensor(const int idInit, const int xpin, const int addr);

    bool initSensor(void) override;
    void setLow(void);
    void setHigh(void);
    bool initI2C();
    void readMeas(const bool debug = false) override;
    std::string getConfig() override;
    void formatData() override;
    void reset();
    std::list<int> getRawDataBuffer() override;
    void sendMeas() override;
    int DefaultMaxDistance() override;

private:
    int distance_value;

protected:
    /* data */
    int _xpin;
    int _addressI2C;
    std::list<int> rawDataBuffer;
};

#endif