/**
 * @file distance_sensor.h
 * @author Julia Cohen - LR Technologies (jcohen@lrtechnologies.fr)
 * @brief Defines the abstract class of a 1D Distance sensor
 * @version 0.1
 * @date 2023-06-23
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef DISTANCESENSOR_H
#define DISTANCESENSOR_H

#include "devices/sensors/sensor.h"

/** @class DistanceSensor
 * @brief Abstract class corresponding to a 1D Distance sensor
 *
 */
class DistanceSensor: public Sensor
{
private:
    /* data */

public:
    explicit DistanceSensor(const int & idInit);


protected :
    /* data */
};

#endif