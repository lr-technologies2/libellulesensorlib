/**
 * @file capacitive_sensor.h
 * @author 
 * @brief Defines the abstract class of a capacitive sensor
 * @version 0.1
 * @date 2023-24-10
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef CAPACITIVESENSOR_H
#define CAPACITIVEENSOR_H

#include "devices/sensors/sensor.h"





/** @class CapacitiveSensor
 * @brief Abstract class corresponding to a capacitive sensor
 * 
 */
class CapacitiveSensor: public Sensor
{
public:
    CapacitiveSensor(const int idInit, const int xpin);
    int capacitive_measure();
    int capacitive_threshold();
    std::string getConfig() override;
    void readMeas(const bool debug=false) override;
    void formatData() override;
    

private:
    int _xpin;
    std::list<int> rawDataBuffer;
};

#endif //CAPACITIVEENSOR_H

