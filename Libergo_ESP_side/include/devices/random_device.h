/**
 * @file random_device.h
 * @author Julia Cohen (jcohen@lrtechnologies.fr)
 * @brief Defines the class that instantiates a RandomDevice (random number generator).
 * @version 0.1
 * @date 2023-08-22
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef RANDOMDEVICE_H
#define RANDOMDEVICE_H

#include "devices/device.h"
#include "devices/sensors/random_sensor.h"
#include "communication/MessagesManager.h"

/** @class RandomDevice
 * @brief Class to instantiate a random number generator to simulate a device
 * 
 */
class RandomDevice : public Device
{
private:

public:
    RandomDevice(const int idInit, const CommunicationType commType, std::shared_ptr<ConfigData> confData);
    int getXDistance() override {return randomData;}
    int getYDistance() override {return randomData;}
    int getClickLeftDistance()  override {return randomData;}
    int getClickRightDistance()  override {return randomData;}
    void hidController() override;
protected:
    /* data */
private:
    int randomData;
};

#endif