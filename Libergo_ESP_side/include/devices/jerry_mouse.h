/**
 * @file jerry_mouse.h
 * @author Adrien Dorise - LR Technologies (adorise@lrtechnologies.fr)
 * @brief Defines the class that instantiates the Jerry device
 * @version 1.0
 * @date 2023-11-06
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef JERRYMOUSE_H
#define JERRYMOUSE_H

#include <string.h>
#include <vector>
#include "devices/device.h"
#include "devices/sensors/VL53L0X.h"
#include "devices/sensors/joystick_sensor.h"
#include "../src/lib_hid/BleMouse.h"


/** @def SAMPLING_TIME
 * @brief Time in ms between each querry.
 */
#define SAMPLING_TIME 1

/** @def THRESHOLD_CLICK
 * @brief Distance threshold below which a click is registered on the distance sensor.
 */
#define THRESHOLD_CLICK 20

/** @def MAX_JOYSTICK
 * @brief Maximum input of the joystick sensor for both X-axis and Y-axis
 */
#define MAX_JOYSTICK 4095



/** @class JerryMouse
 * @brief Class to instantiate the JerryMouse sensor
 *
 */
class JerryMouse : public Device
{
private:
    int s1_distance;
    int s2_distance;
    int jx_distance;
    int jy_distance;

public:
    JerryMouse(const int idInit, const CommunicationType commType, std::shared_ptr<ConfigData> confData);

    void formatData();
    bool controller();
    void hidController() override;
    int getXDistance() override;
    int getYDistance() override;
    int getClickLeftDistance() override;
    int getClickRightDistance() override;
protected:
    /* data */

};

#endif