#pragma once 

#include <vector>
#include <string>
#include <map>
#include "files_manager.h"
#include "device_selection.h"
struct JoystickConfig
{
    int m_minX = 0;
    int m_maxX = 4095;
    int m_minY = 0;
    int m_maxY = 4095;
    int m_neutralX = 1000;
    int m_neutralY = 1000;
    const std::string m_name = "JOYSTICK";

    std::string save(size_t i);
    void load(size_t i, const std::map<std::string, int> & data);
};

struct SensorConfig
{
    int m_neutral = 25;
    int m_maxDistance = 200;
    const std::string m_name = "SENSOR";

    std::string save(size_t i);
    void load(size_t i, const std::map<std::string, int> & data);

};

class ConfigData
{
public:
    ConfigData(const available_device dType);
    ~ConfigData();
    void Default(const available_device dType);
    const JoystickConfig GetJoystick(size_t id);
    const SensorConfig GetSensors(size_t id);
    const int GetSensiblity();
    std::string Save();
    bool Load(std::string data);
protected:
    int m_sensibility;
    std::vector<JoystickConfig> m_joysticks;
    std::vector<SensorConfig> m_distanceSensors;
    std::string m_name;
    std::string m_file;
    /* Configuration of the SPIFFS filesystem */
    FilesManager ConfigFiles; /**< Object that handles reading from and writing to the configuration files (Board Id and Sensors) */
};

