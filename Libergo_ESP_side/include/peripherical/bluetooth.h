#pragma once

#include "peripherical.h"

#include "lib_hid/BleMouse.h"
#include "lib_hid/BleMouseTypes.h"
#include <WiFi.h>
#include "communication/MessagesTypes.h"
#include "ConfigData.h"

namespace peripherical
{
    class Bluetooth: public Peripherical
    {
        public:
        Bluetooth(std::shared_ptr<ConfigData> configData);
        ~Bluetooth();

        void begin(const std::shared_ptr<Device> app_device);
        void loop(const std::shared_ptr<Device> app_device);
        void start();
        protected:
        BleMouse m_bleMouse;
        std::shared_ptr<ConfigData> m_configData;
    };
}