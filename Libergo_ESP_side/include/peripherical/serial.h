#pragma once

#include "peripherical.h"


#include "state_machine/sensor_manager.hpp"
#include "state_machine/sensor_comm_client.hpp"
#include <Arduino.h>
#include <ArduinoJson.h>
#include "communication/MessagesManager.h"
#include "communication/MessagesTypes.h"
#include "general_header.hpp"
#include "errors.h"

namespace peripherical
{
    class SerialUSB: public Peripherical
    {
        public:
        SerialUSB();
        ~SerialUSB();

        
        void begin(const std::shared_ptr<Device> app_device);
        void loop(const std::shared_ptr<Device> app_device);
        void start();

        protected:
        bool tempSend(std::string msg);
        //void SensorCommClientTaskCode(void *pvParameters);
        //void SensorManagerTaskCode(void *pvParameters);
        //void stateMachine(std::shared_ptr<Device> app_device);

/** State machine SensorManager */
SensorManager stmSensor;
/** State machine SensorCommClient */
SensorCommClient stmCommClient;

// Variables definition

TaskHandle_t SensorCommClientTask; /**< SensorCommClient task handle*/
TaskHandle_t SensorManagerTask;    /**< SensorManager task handle*/

    };
}