#pragma once

#include <memory>
#include "devices/device.h"
#include "device_selection.h"

namespace peripherical
{

class Peripherical 
{
public:
    Peripherical(const CommunicationType comm_type);
    ~Peripherical();

    virtual void begin(const std::shared_ptr<Device> app_device) = 0;
    virtual void loop(const std::shared_ptr<Device> app_device) = 0;
    virtual void serialEventLoop(const std::shared_ptr<Device> app_device, std::shared_ptr<ConfigData> configData);
    virtual void start() = 0;
    CommunicationType getComType();

protected:
    CommunicationType m_comm_type;
};
}