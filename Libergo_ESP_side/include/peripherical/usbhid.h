#pragma once

#ifdef USING_USBHID
#ifndef ARDUINO_USB_MODE
#error This ESP32 SoC has no Native USB interface
#elif ARDUINO_USB_MODE == 1
#warning This sketch should be used when USB is in OTG mode
#endif /* ARDUINO_USB_MODE */


#include "USB.h"
#include "USBHIDMouse.h"
#endif


#include "peripherical.h"

#include "communication/MessagesTypes.h"
#include "ConfigData.h"

namespace peripherical
{
    class UsbHid: public Peripherical
    {
        public:
        UsbHid(std::shared_ptr<ConfigData> configData);
        ~UsbHid();

        void begin(const std::shared_ptr<Device> app_device);
        void loop(const std::shared_ptr<Device> app_device);
        void start();
        protected:
        std::shared_ptr<ConfigData> m_configData;
        bool m_buttonRightPressed;
        bool m_buttonLeftPressed;
#ifdef USING_USBHID
        // USB HID object
        USBHIDMouse Mouse;
#endif
        bool m_isBegin;
    };
}