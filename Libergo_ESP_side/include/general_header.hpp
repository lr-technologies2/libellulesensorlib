/**
 * @file general_header.hpp
 * 
 * 
 * @author joao Martin--Saquet (jmartinsaquet@lrtechnologies.fr)
 * @brief  this file is the general header file for all file of the project 
 * should include all basic libraries such as arduino.h or string.h
 * @version 0.1
 * @date 2023-06-02
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#ifndef GNH_H
#define GNH_H

#include <string.h>
#include <vector>
#include <stdio.h>
#include <iostream>
#include <list>
#include <map>

#include <Arduino.h>
#include <Wire.h>  // I2C lib

#include "FS.h"  // Arduino file system wrapper
#include "SPIFFS.h"

// lib dependencies
#include "Adafruit_VL53L0X.h"


#endif