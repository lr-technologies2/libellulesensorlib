/**
 * @file files_manager.h
 * @author Julia Cohen - LR Technologies (jcohen@lrtechnologies.fr)
 * @brief File describing the FilesManager class to handle the configuration files stored in the ESP32
 * @version 0.1
 * @date 2023-06-28
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef FILES_MANAGER_H
#define FILES_MANAGER_H

#include <string>

/** @def ID_FILE_NAME
 * @brief Name of the file containing the board ID (for bluetooth identification)
 */
#define ID_FILE_NAME "/Libergo_ID.txt"

/** @def DEVICE_FILE_NAME
 * @brief Name of the file containing the device configuration
 */
#define DEVICE_FILE_NAME "/Libergo_Device.txt"

/** @class FilesManager
 * @brief FilesManager class to read and write a new board ID and Sensor information in text files
 * 
 * @details The files that are being modified are data/Libergo_ID.txt and data/Libergo_Sensor.txt
 * 
 */
class FilesManager
{
    public:
        FilesManager();
        void initID(void);
        std::string readID(void);
        void writeID(const std::string &new_id);
        std::string readSensor(void);
        void writeSensor(const std::string & new_sensor_data);
        unsigned int getErrorStatus();

    private:
        unsigned int _errorStatus;
};

#endif