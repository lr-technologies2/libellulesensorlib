/**
 * @file MessagesManager.h
 * @author Lucas Le Loiret, Adrien Dorise @ LR Technologies
 * @brief Definition of read/write functions to manage messages
 * @version 0.2
 * @date 2023-11-06
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef MESSAGESMANAGER_H
#define MESSAGESMANAGER_H

#include <ArduinoJson.h>

typedef enum CommunicationType
{
    USB_HID,
    SERIAL_USB,
    BLUETOOTH
}CommunicationType;

void initConnection(CommunicationType comm_type= SERIAL_USB);

bool is_connection_available(CommunicationType comm_type= SERIAL_USB);


/********* Send messages. Args to complete.************/
void sendMessage(JsonDocument message, CommunicationType comm_type= SERIAL_USB);

void sendMessageStart();

void sendMessageAck();

void sendMessageReady();

void sendMessageEnd();

void sendMessageDataSensor(const std::string headers[], const int data[], const int size);
void sendMessageConfiguration(const std::string & data);

void sendMessageOK();

void sendMessageNOK();

void sendMessageIsAlivePC();


/********* Read functions. After message's type is knowns, called the corresponding function. ******/
/********* To do : write this function in MessageManager.cpp file.  ************/
String readMessage(CommunicationType comm_type= SERIAL_USB);

int getMessageType(String message);

std::string getConfiguration();

void readStartPC();

void readAck();

void readReady();

void readCalibration();

void readDeactivateSensor();

void readActivateSensor();

void readConfiguration();

void readRequestConf();

void readDeviceInfo();

void readNewID();

void read_shutdown();

void readIsAliveESP();

#endif //MESSAGESMANAGER_H