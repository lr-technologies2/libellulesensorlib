#ifndef MESSAGESTYPES_H_
#define MESSAGESTYPES_H_

//Messages ID have to be the same that in the file messages_types.py (PC side)

//MESSAGE FROM PC
#define START_PC            0 /*!< PC initalisation is done */
#define ACK_PC              1 /*!< Ack from PC */
#define READY_PC            2 /*!< PC is ready to start receiving measurements */
#define CALIBRATION         3 /*!< Request to start calibration procedure */
#define DEACTIVATE_SENSOR   4 /*!< Request to deactivate a sensor */
#define ACTIVATE_SENSOR     5 /*!< Request to activate a sensor */
#define DEVICE_INFO         6 /*!< Request information about device */
#define NEW_ID              7 /*!< Send a new ID for this device */
#define RESTART_ESP         8 /*!< Restart the ESP32 device */
#define IS_ESP_ALIVE        9 /*!< PC wants to check if connection still alive */
#define REQUEST_CONF        10 /*!< PC wants sensors configuration */
#define DEVICE_CONFIG       11 /*!< PC send sensors configuration */

//MESSAGE FROM ESP
#define START_ESP           0 /*!< ESP intialisation is done and will initiate loop */
#define ACK_ESP             1 /*!< Ack from device */
#define READY_ESP           2 /*!< ESP is ready to start sending measurements */
#define END_MSG_ESP         3 /*!< ESP has finished his message */
#define DATA_SENSOR         4 /*!< Sensor data */
#define OK_MESSAGE          5 /*!< Message is OK */
#define NO_OK_MESSAGE       6 /*!< Message is not ok (unknown type, bad structure...) */
#define IS_PC_ALIVE         7 /*!< ESP wants to check if connection still alive */
#define CONFIG_ESP          8 /*!< ESP send sensors configuration */

#endif //MESSAGESTYPES_H_

