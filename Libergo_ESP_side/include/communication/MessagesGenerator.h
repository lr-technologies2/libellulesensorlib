/**
 * @file MessagesGenerator.h
 * @author Lucas Le Loiret @ LR Technologies
 * @brief Definition of the methods to create messages to send
 * @version 0.1
 * @date 2023-09-18
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef MESSAGESGENERATOR_H_
#define MESSAGESGENERATOR_H_

#include <ArduinoJson.h>

// TODO complete arguments for each message


JsonDocument messageStart();

JsonDocument messageAck();

JsonDocument messageReady();

JsonDocument messageEnd();

JsonDocument messageDataSensor(const std::string headers[], const int data[], const int size);
JsonDocument messageConfiguration(const std::string & data);

JsonDocument messageOK();

JsonDocument messageNOK();

JsonDocument messageIsAlivePC();


#endif //MESSAGESGENERATOR_H_