/**
 * @file errors.h
 * @author Julia Cohen (jcohen@lrtechnologies.fr)
 * @brief File defining all error codes to be used in the project.
 * @version 0.1
 * @date 2023-06-28
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef ERRORS
#define ERRORS

/** @def NO_ERROR
 * @brief Absence of error
 */
#define NO_ERROR 0

/** @def UNDEFINED_ERROR
 * @brief Undefined error
 *
 */
#define UNDEFINED_ERROR 1

// SPIFFS status errors
/** @def SPIFFS_INIT_ERROR
 * @brief Initialization error
 */
#define SPIFFS_INIT_ERROR 2

/** @def SPIFFS_OPEN_ERROR
 * @brief error when opening a file
 */
#define SPIFFS_OPEN_ERROR 3

/** @def SPIFFS_WRITE_ERROR
 * @brief error when writing a file
 */
#define SPIFFS_WRITE_ERROR 4

// I2C errors

// Sensors errors

#endif