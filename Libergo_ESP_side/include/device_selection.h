/**
 * @file device_selection.h
 * @author Julia Cohen, Adrien Dorise - LR Technologies ({jcohen,adorise}@lrtechnologies.fr)
 * @brief  Specify the current hardware configuration
 * The desired device mus be selected by setting the <wanted_device>.
 * The <available_device> dress the list of available device to be used.
 * The inner parameters (such as input pin, I2C ID...) are available in further section of this file
 * @version 0.1
 * @date 2023-06-07
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef DEVICE_SELECT
#define DEVICE_SELECT

#include <Arduino.h>

/********************************************************/
/** @enum available_device
 * @brief List of all available devices
 * @warning Not used in v0.1
 *
 */
typedef enum available_device
{
    JERRY_MOUSE,
    LIGHT_MOUSE,
    AIR_MOUSE,
    JOYSTICK,
    CAPACITIVE_SENSOR,
    RANDOM_DEVICE
} available_device;

const std::string getDeviceType(const available_device dType);

/**
 * @brief Device selected.
 * Change this variable to select a different device. The default is `RANDOM_DEVICE`.
 * 
 */
#ifdef IAM_JERRY_MOUSE
const available_device deviceType = available_device::JERRY_MOUSE;
#endif 

#ifdef IAM_AIR_MOUSE
const available_device deviceType = available_device::AIR_MOUSE;
#endif 

#ifdef IAM_RANDOM_DEVICE
const available_device deviceType = available_device::RANDOM_DEVICE;
#endif

/********************************************************/
/** @def LIGHTMOUSE_XPIN
 * @brief XSHUT pin for the VL530X sensor
 */
#define LIGHTMOUSE_XPIN GPIO_NUM_5

/** @def LIGHTMOUSE_ADDRESS
 * @brief I2C address for the VL530X sensor
 */
#define LIGHTMOUSE_ADDRESS 0x32

/********************************************************/
/** @def AIRMOUSE_XPIN1
 * @brief XSHUT pin for sensor 1
 */
/** @def AIRMOUSE_XPIN2
 * @brief XSHUT pin for sensor 2
 */
/** @def AIRMOUSE_XPIN3
 * @brief XSHUT pin for sensor 3
 */
/** @def AIRMOUSE_XPIN4
 * @brief XSHUT pin for sensor 4
 */
#if CONFIG_IDF_TARGET_ESP32
#define AIRMOUSE_XPIN1 GPIO_NUM_5
#define AIRMOUSE_XPIN2 GPIO_NUM_16
#define AIRMOUSE_XPIN3 GPIO_NUM_0
#define AIRMOUSE_XPIN4 GPIO_NUM_19
#elif CONFIG_IDF_TARGET_ESP32S3
#define AIRMOUSE_XPIN1 GPIO_NUM_35
#define AIRMOUSE_XPIN2 GPIO_NUM_36
#define AIRMOUSE_XPIN3 GPIO_NUM_37
#define AIRMOUSE_XPIN4 GPIO_NUM_38
#endif

/** @def AIRMOUSE_ADDRESS1
 * @brief I2C address for sensor 1
 */
/** @def AIRMOUSE_ADDRESS2
 * @brief I2C address for sensor 2
 */
/** @def AIRMOUSE_ADDRESS3
 * @brief I2C address for sensor 3
 */
/** @def AIRMOUSE_ADDRESS4
 * @brief I2C address for sensor 4
 */
#if CONFIG_IDF_TARGET_ESP32
#define AIRMOUSE_ADDRESS1 0x32
#define AIRMOUSE_ADDRESS2 0x34
#define AIRMOUSE_ADDRESS3 0x36
#define AIRMOUSE_ADDRESS4 0x38
#elif CONFIG_IDF_TARGET_ESP32S3
#define AIRMOUSE_ADDRESS1 0x32
#define AIRMOUSE_ADDRESS2 0x34
#define AIRMOUSE_ADDRESS3 0x36
#define AIRMOUSE_ADDRESS4 0x38
#endif

/********************************************************/
/** @def JERRYMOUSE_XPIN
 * @brief X pin for the Joystick sensor
 */
#if CONFIG_IDF_TARGET_ESP32
#define JERRYMOUSE_XPIN GPIO_NUM_34 //39
#elif CONFIG_IDF_TARGET_ESP32S3
#define JERRYMOUSE_XPIN GPIO_NUM_4
#endif
/** @def JERRYMOUSE_YPIN
 * @brief Y pin for the Joystick sensor
 */
#if CONFIG_IDF_TARGET_ESP32
#define JERRYMOUSE_YPIN GPIO_NUM_36
#elif CONFIG_IDF_TARGET_ESP32S3
#define JERRYMOUSE_YPIN GPIO_NUM_5
#endif

/** @def JERRYMOUSE_RCPIN
 * @brief XSHUT pin for right click sensor
 */
#if CONFIG_IDF_TARGET_ESP32
#define JERRYMOUSE_RCPIN GPIO_NUM_17
#elif CONFIG_IDF_TARGET_ESP32S3
#define JERRYMOUSE_RCPIN GPIO_NUM_10
#endif

/** @def JERRYMOUSE_LCPIN
 * @brief XSHUT pin for left click sensor
 */
#if CONFIG_IDF_TARGET_ESP32
#define JERRYMOUSE_LCPIN GPIO_NUM_16
#elif CONFIG_IDF_TARGET_ESP32S3
#define JERRYMOUSE_LCPIN GPIO_NUM_11
#endif

/** @def JERRYMOUSE_ADDRESSRC
 * @brief I2C address for right click sensor
 */
#if CONFIG_IDF_TARGET_ESP32
#define JERRYMOUSE_ADDRESSRC 0x32
#elif CONFIG_IDF_TARGET_ESP32S3
#define JERRYMOUSE_ADDRESSRC 0x32
#endif

/** @def JERRYMOUSE_ADDRESSLC
 * @brief I2C address for left click sensor
 */
#if CONFIG_IDF_TARGET_ESP32
#define JERRYMOUSE_ADDRESSLC 0x34
#elif CONFIG_IDF_TARGET_ESP32S3
#define JERRYMOUSE_ADDRESSLC 0x34
#endif

/********************************************************/
/*
 * A capacitive sensor can be an actual sensor, or a wire connected to specific pin.
 * For more details, see <a href="https://randomnerdtutorials.com/esp32-touch-pins-arduino-ide/">this tutorial</a>.
*/
/** @def CAPACITIVE_XPIN1
 * @brief GPIO pin for capacitive sensor
 */
#define CAPACITIVE_XPIN1 GPIO_NUM_4

#endif