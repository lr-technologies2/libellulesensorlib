#pragma once 

#include <Arduino.h>

template<class T>
class Singleton
{
public:
    static T & getInstance()
    {
        static T instance;
        return instance;
    }
};

class SerialMacro: public Singleton<SerialMacro>
{
    friend class Singleton<SerialMacro>;
private:
    SerialMacro();

    ~SerialMacro();

    bool m_begin;
    u_int32_t m_speed;
public:
    bool getBegin();
    void setBegin();
    void setSpeed(int speed);
};


#define SERIAL_SPEED_BEGIN(speed) \
SerialMacro::getInstance().setSpeed(speed); \

#define SERIAL_BEGIN() \
SerialMacro::getInstance().setBegin(); \

#define SERIAL_PRINT(msg) \
if (SerialMacro::getInstance().getBegin()){Serial.print(msg);} \

#define SERIAL_PRINT2(msg, arg) \
if (SerialMacro::getInstance().getBegin()){Serial.print(msg, arg);} \

#define SERIAL_PRINTLN(msg) \
if (SerialMacro::getInstance().getBegin()){Serial.println(msg);} \

#define SERIAL_PRINTLN2(msg, arg) \
if (SerialMacro::getInstance().getBegin()){Serial.println(msg, arg);} \

