# Libergo - ESP32
## Description

This folder contains the embedded code to deploy to an ESP32 with one or more sensors. The goal is to read the sensors values as they are used as a computer mouse, normalize the measurements and send them to the computer.   
The proposed approach is the following: the computer and ESP32 board communicate through a wireless Bluetooth connection, identified by an ID attributed to a specific board. This ID can be modified using the companion computer app, which sends a specific message from the computer to the ESP32.


[[_TOC_]]

----

## Folder structure

```
📦 libellulesensorlib               ------> Root folder   
┣ 📂 Libergo_ESP_side               ------> C++ Embedded code for ESP32 platform   
┃ ┣ 📂  Capa_sensor                 ------> (To be deleted) Old code to reintegrate as a Sensor/Device   
┃ ┃ ┣ 📜  Capacitive_Sensor.cpp   
┃ ┃ ┗ 📜  Capacitive_Sensor.h   
┃ ┣ 📂  doc                         ------> Documentation   
┃ ┃ ┣ 📂  coverage                  ------> Store doc coverage results (using the script)   
┃ ┃ ┣ 📂  doxygen                   ------> Store doxygen-generated code documentation (using the script)   
┃ ┃ ┣ 📜  device_sensor_diagram.png   
┃ ┃ ┗ 📜  Last_Doc_coverage.png     ------> Screenshot of the last doc coverage evaluation   
┃ ┣ 📂  include                     ------> Include directory containing header files   
┃ ┃ ┣ 📂  devices                   ------> Everything about the Device and Sensor classes   
┃ ┃ ┃ ┣ 📂  sensors   
┃ ┃ ┃ ┃ ┣ 📜  VL53L0X.h              
┃ ┃ ┃ ┃ ┣ 📜  distance_sensor.h              
┃ ┃ ┃ ┃ ┣ 📜  random_sensor.h              
┃ ┃ ┃ ┃ ┗ 📜  sensor.h                 
┃ ┃ ┃ ┣ 📜  air_mouse.h                  
┃ ┃ ┃ ┣ 📜  device.h             
┃ ┃ ┃ ┣ 📜  light_mouse.h             
┃ ┃ ┃ ┗ 📜  random_device.h       
┃ ┃ ┣ 📂  state_machine             ------> Everything needed to build any state machine   
┃ ┃ ┃ ┣ 📜  State.hpp                   
┃ ┃ ┃ ┣ 📜  StateMachine.hpp            
┃ ┃ ┃ ┗ 📜  Transition.hpp              
┃ ┃ ┣ 📜  MessagesGenerator.h       ------> Creates different types of messages   
┃ ┃ ┣ 📜  MessagesManager.h         ------> Receive/Send messages   
┃ ┃ ┣ 📜  MessagesTypes.h           ------> Defines different types of messages   
┃ ┃ ┣ 📜  device_selection.h        ------> **Select your current device (before build)**     
┃ ┃ ┣ 📜  errors.h                  ------> Error codes definition      
┃ ┃ ┣ 📜  files_manager.h           ------> Class to read/write Device information into the local filesystem     
┃ ┃ ┗ 📜  general_header.hpp        ------> Contains common imports to not repeat them across the project     
┃ ┣ 📂  lib                         ------> (Not used) External libraries     
┃ ┣ 📂  src                         ------> Source code    
┃ ┃ ┣ 📂  devices                   ------> Everything about the Device and Sensor classes   
┃ ┃ ┃ ┣ 📂  sensors                     
┃ ┃ ┃ ┃ ┣ 📜  VL53L0X.cpp              
┃ ┃ ┃ ┃ ┣ 📜  distance_sensor.cpp              
┃ ┃ ┃ ┃ ┣ 📜  random_sensor.cpp              
┃ ┃ ┃ ┃ ┗ 📜  sensor.cpp                 
┃ ┃ ┃ ┣ 📜  air_mouse.cpp                  
┃ ┃ ┃ ┣ 📜  device.cpp             
┃ ┃ ┃ ┣ 📜  light_mouse.cpp             
┃ ┃ ┃ ┗ 📜  random_device.cpp       
┃ ┃ ┣ 📂  state_machine             ------> Our state machines: Sensor_Comm_Client, Sensor_Manager   
┃ ┃ ┃ ┣ 📜  sensor_comm_client.cpp                   
┃ ┃ ┃ ┣ 📜  sensor_comm_client.hpp             
┃ ┃ ┃ ┣ 📜  sensor_manager.cpp                   
┃ ┃ ┃ ┗ 📜  sensor_manager.hpp              
┃ ┃ ┣ 📜  MessagesGenerator.cpp     ------> Creates different types of messages   
┃ ┃ ┣ 📜  MessagesManager.cpp       ------> Receive/Send messages    
┃ ┃ ┣ 📜  debug.cpp                 ------> (To be moved/deleted) Code to test specific elements (I2C, etc.)   
┃ ┃ ┣ 📜  files_manager.cpp         ------> Class to read/write Device information into the local filesystem      
┃ ┃ ┗ 📜  main.cpp                  ------> Main script    
┃ ┣ 📂  test                        ------> Folder to develop unit tests     
┃ ┣ 📜  Doxyfile                    ------> Doxygen configuration file   
┃ ┣ 📜  Libergo_ID.txt              ------> Example of config file stored on ESP32   
┃ ┣ 📜  Libergo_Sensor.txt          ------> Example of config file stored on ESP32   
┃ ┣ 📜  README.md                   ------> This file   
┃ ┣ 📜  doc_coverage_generator.sh   ------> Script to gnerate doxygen documentation and doc coverage report   
┃ ┗ 📜  platformio.ini              ------> PlatformIO configuration file   
┣ 📂 Libergo_PC_side                ------> Python backend to exchange messages between the PC and ESP32     
┣ 📂 Libergo_Sensor_App             ------> Python GUI modules (to be moved to LR_Keyboard repository once operational)    
┣ 📂 doc                            ------> Global documentation folder   
┣ 📜 README                         ------> This file   
┗ 📜 .gitignore                     ------> Specify files and folders that must not be pushed to the remote repository (local configs, etc.)   

```

## Architecture

### Sensor_Comm_Client

Here is a detailed view of the **Sensor_Comm_Client** component.

After setting up the Bluetooth, the module waits for messages. When a message arrives, the destination ID is compared with the board BT_ID (stored in the filesystem) and treated only if they correspond.   
Then, the message type is checked:
- if it is a request to change the BT_ID: the file with ID is overwritten and the new ID is used.
- else: store the message for the other core to process. It can be a request for the device parameters, or information to update the device parameters (specific user data for example). 

![Sensor_comm_Client state machine](../doc/SensorCommClient.jpg)


### Sensor_Manager

Here is a detailed view of the **Sensor_Manager** component.

The sensors are initialized during the board setup and configured according to default or a stored configuration. 

The task is sleeping until there is communication from the computer. After a t_sleeping duration, the core is powered off (deep sleep).

When a message is received from the other core (Sensor_Comm_Client), the message is parsed to determine the message type, which defines the type of information contained in the message. The message type can be:
1. changing the sensors parameters (calibration update)
2. starting to send measurements
3. the connection to PC has been lost or no Ack has been received in a while
  
After a certain time without any message received from the PC, the core goes back to Sleep state (from state Measure -> Parse message of type 3. -> Sleep). In the meantime and after receiving the Start message, the sensors are scanned regularly, their values are evaluated against a threshold or condition within the Scan state (are they significant? is that a new measure or an old one?). If the measurement is valid, then it is normalized and sent to the computer. This loop will be done either in the Measure state or in the Device `scan` function, there is no need for another small state machine. **Converting to mouse instructions is done on the computer.**

![Sensor_Manager state machine](../doc/SensorManager.jpg)

## Development status

- [ ] Communication:
  - [x] Serial Comm POC
  - [ ] (WIP) Serial Comm packaged
  - [x] Bluetooth Serial Comm POC
  - [ ] (WIP) Bluetooth Serial Comm packaged
  - [ ] Queue between tasks
  - [ ] (WIP) Exchange protocol
- [ ] Devices and Sensors:
  - [x] Device class
  - [ ] (WIP) Device class improvements
  - [x] Sensor class
  - [ ] (WIP) Sensor class improvements
  - [x] Airmouse Device
  - [x] Lightmouse Device
  - [x] VL53L0X Sensor
  - [x] Random Device
  - [x] Random Sensor
  - [ ] Capacitive Sensor
  - [ ] EMG Sensor
  - [ ] Joystick Sensor
- [ ] State machines:
  - [x] V1: basic implementation
  - [ ] (WIP) V2: advanced implementation
  - [ ] Sensor_Comm_Client
  - [ ] Sensor_Manager
- [ ] Unit tests


## Status
Doc coverage: 91.7%   
Test coverage: 0%

## Troubleshooting

**ESP32 keeps rebooting when I deploy the program, error is "SW_CPU_RESET"**   
There is a memory corruption problem. Using the VS Code PlatformIO scripts (PLATFORMIO > Project Tasks):   
- In `General`: Full Clean
- In `Platform`: Erase Flash, then Build Filesystem Image, then Upload Filesystem Image

Build and deploy again.

**When executing the project on the ESP32 board, I get a "esp_core_dump_flash: Core dump flash config is corrupted!" error message in the Serial terminal, and the board keeps rebooting**
- Solution 1: if the problem is related to the file system   
The memory needs to be correctly initialized.   
Use the "ESP32 File system SPIFFS helpers" code in the snippets section of the project ([here](https://gitlab.com/lr-technologies2/libellulesensorlib/-/snippets/2587627)). Run the main.cpp **twice** and try again the main code.   
Explaination: if the filesystem is not initialized, the SPIFFS.begin(true) command will return false, indicating that the filesystem is not working properly. Since the argument is true, it will initialize the memory. Next time, it will be working as expected.

- Solution 2: the problem is related ot the memory usage
Revise your code, there might be an attempt to access a variable that was wiped out of the memory. Especially, variables defined in constructors and accessd somewhere else in the code.


## Device/Sensor classes architecture

![Diagram of devices and sensors architecture](doc/device_sensor_diagram.png)

## Documentation

Generate the `doxygen` documentation of this code with the following procedure.   
After everything is installed, the steps to generate the documentation and coverage reports can be performed in a single step with shell script: ```./doc_coverage_generator.sh``` .

### Install doxygen and prepare the folder

Install `doxygen`:   
```bash
sudo apt install doxygen
```

Make sure that folder and subfolder `doc/doxygen/` exist, and file `Doxyfile` at the root of the current directory (they should be...).  
If they don't exist:
- create the folders
- init doxygen with command `doxygen -g`

In the `Doxyfile` file, replace or fill in the following tags with their correct values:
- `PROJECT_NAME           = "Libergo ESP Side"`
- `PROJECT_BRIEF          = "Embedded code for ESP32 for project Libergo"`
- `OUTPUT_DIRECTORY       = "doc/doxygen"`
- `INPUT                  = src/ include/`
- `RECURSIVE              = YES`


### Generate the doc

```
doxygen
```

Open the file `doc/doxygen/html/index.html` in a web browser to access the documentation.

### Generate doc coverage

To compute the percentage of the code that is documented, we use [coverxygen](https://github.com/psycofdj/coverxygen). Follwing the indications in the ReadMe:   
```
sudo apt-get install lcov
pip3 install coverxygen
```

Make sure that `GENERATE_XML = YES` in the `Doxyfile`.   
Run doxygen as indicated above: `doxygen`   
A folder named `xml` should have appeared in the `doc/doxygen/` folder.   

Now run the coverxygen command:   
```
python3 -m coverxygen --xml-dir doc/doxygen/xml --src-dir . --output doc-coverage.info
```

To visualize the result in a console:   
```
lcov --summary doc-coverage.info
```

To visualize the results in an html-browsable format (and much more readable):  
```
genhtml --no-function-coverage --no-branch-coverage doc-coverage.info -o doc/coverage
```
Then open the index.html in a web browser. You can then click on individual items to get the report for every folder, subfolder or file. The result is as follow: 
![documentation coverage - last coverage: 91.7%](doc/Last_Doc_Coverage.png)

