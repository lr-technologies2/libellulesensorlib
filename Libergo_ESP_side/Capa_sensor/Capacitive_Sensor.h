/**
 * @file Capacitive_Sensor.h
 * @author 
 * @brief
 * @version 0.1
 * @date 2023-04-05
 *
 * @copyright Copyright (c) 2023
 *
 */
#define SENSOR_CAPACITIVE

long smooth();

const int numReadings  = 10;
long readings [numReadings];
int readIndex  = 0;
long total  = 0;
const int sensitivity  = 1000;
const int thresh  = 200;
const int csStep  = 10000;
CapacitiveSensor cs  = CapacitiveSensor(2, 3);

/*
 * Refaire la librairie pour CapacitiveSensor, actuellement ne fonctionne pas avec l'ESP 32
 */